# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# pylint: disable=import-error
"""Main module, defines the application"""
from contextlib import asynccontextmanager

from apscheduler.schedulers.asyncio import AsyncIOScheduler  # type: ignore
from brotli_asgi import BrotliMiddleware
from fastapi import FastAPI, Request
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException, RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from app.config import settings
from app.logger import configure_logger
from app.routers import drawings, main, warnings
from app.utils.cleanup import cleanup_drafts
from app.utils.client import AsyncHttpxClientWrapper

configure_logger()


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Lifespan specification"""
    # pylint: disable = redefined-outer-name,unused-argument

    # Get async client
    await AsyncHttpxClientWrapper.get_httpx_client()
    # Schedule cleanup job
    if settings.run_draft_cleanup:
        scheduler = AsyncIOScheduler()
        scheduler.add_job(cleanup_drafts,
                          "cron", [settings.draft_cleanup_hours],
                          hour="*",
                          jitter=10,
                          max_instances=1,
                          coalesce=True)
        scheduler.start()

    yield

    # Shutdown scheduler
    if settings.run_draft_cleanup:
        scheduler.shutdown()
    # Clean up async client
    await AsyncHttpxClientWrapper.close_httpx_client()


app = FastAPI(docs_url=settings.application_doc_root,
              root_path=settings.application_root_path,
              lifespan=lifespan)
app.include_router(main.router)
app.include_router(drawings.router)
app.include_router(warnings.router)
app.add_middleware(CORSMiddleware,
                   allow_origins=['*'],
                   allow_methods=['OPTIONS', 'GET', 'POST', 'HEAD'],
                   expose_headers=['Authorization', 'Content-Type', 'Location'])


@app.middleware("http")
async def add_headers(request: Request, call_next):
    """Adds security headers"""
    response = await call_next(request)
    response.headers['X-Content-Type-Options'] = "nosniff"
    response.headers[
        'Strict-Transport-Security'] = "max-age=31536000; includeSubDomains"
    return response


app.add_middleware(BrotliMiddleware, gzip_fallback=True)


@app.exception_handler(HTTPException)
async def http_exception_handler(_: Request, exc: HTTPException):
    """Converts a HTTPException to a HTTP response"""

    return JSONResponse({'message': exc.detail},
                        status_code=exc.status_code,
                        headers=exc.headers)


@app.exception_handler(RequestValidationError)
async def request_validation_error_handler(_: Request,
                                           exc: RequestValidationError):
    """Converts a HTTPException to a HTTP response"""

    return JSONResponse({'message': jsonable_encoder(exc.errors())},
                        status_code=400)
