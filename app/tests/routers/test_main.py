# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module to test db"""

from fastapi.testclient import TestClient

from app.config import settings


def test_root_endpoint(client: TestClient) -> None:
    "Test root endpoint"

    resp = client.get('/')
    assert resp.status_code == 200
    assert resp.text == 'GeoWeb Warnings API'


def test_version_endpoint(client: TestClient) -> None:
    "Test version endpoint"

    resp = client.get('/version')
    assert resp.status_code == 200
    assert resp.json() == {'version': settings.version}


def test_healthcheck_endpoint(client: TestClient) -> None:
    "Test healthcheck endpoint"

    resp = client.get('/healthcheck')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['status'] == 'OK'
    assert resp_json['service'] == 'Warnings'
    assert resp_json['database']['status'] == 'OK'
