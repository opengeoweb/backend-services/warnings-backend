# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# pylint: disable=unused-argument
"""Module to test drawings endpoints in warnings backend"""
import json
import uuid

from fastapi.testclient import TestClient
from freezegun import freeze_time
from sqlmodel import Session

from app.models import DrawingTableRecord, ScopeEnum


def test_post_drawing_without_slash(client: TestClient) -> None:
    """Tests posting a drawing to URL without trailing slash"""

    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    with open('app/tests/testdata/input_drawing2.json', 'rb') as fh:
        response = client.post('/drawings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    # if GET /drawings exist - expand test to check contents


def test_post_drawing_with_slash(client: TestClient) -> None:
    """Tests posting a drawing to URL with trailing slash"""

    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers['Location'] is not None
    returned_uri = response.headers['Location']
    # Check the id has a valid UUID format
    new_id = returned_uri.rstrip('/').rsplit('/', 1)[-1]
    assert uuid.UUID(new_id)


def test_post_drawing_same_user(client: TestClient, session: Session) -> None:
    """Test the post drawing route when title already exists for given user"""

    ## post first time
    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers['Location'] is not None
    returned_uri = response.headers['Location']
    # Check the id has a valid UUID format
    new_id = returned_uri.rstrip('/').rsplit('/', 1)[-1]
    assert uuid.UUID(new_id)

    ## post same drawing a second time
    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert response.json() == {
        'message': 'Object name already exists, please choose a different name'
    }


def test_post_drawing_invalid(client: TestClient) -> None:
    """Test the post drawing route with an invalid drawing body"""

    with open('app/tests/testdata/invalid_input_drawing.json', 'rb') as fh:
        response = client.post('/drawings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400


def test_post_drawing_invalid_scope(client: TestClient) -> None:
    """Test the post drawings route with an invalid scope"""

    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        data = json.load(fh)
        data['scope'] = 'invalid'
        response = client.post('/drawings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400

    ## check that POST is possible with valid scope
    data['scope'] = 'user'
    response = client.post('/drawings',
                           json=data,
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_drawing_nouser(client: TestClient) -> None:
    """Test the post drawings route when no user is authenticated"""

    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': ''})
    assert response.status_code == 401


def test_post_drawing_no_title(client: TestClient) -> None:
    """Test the post drawings route when no title is given"""

    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        data = json.load(fh)
        del data['objectName']
        assert 'objectName' not in data
        response = client.post('/drawings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    # if GET /drawings exist - expand test to check name of drawing


def test_get_drawing_non_existing(client: TestClient) -> None:
    """Tests getting a drawing by a non-existing ID"""
    response = client.get('/drawing/non-existing-id',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 404
    assert {'detail': 'Not Found'} == response.json()


def test_get_drawing_existing_without_user(client: TestClient,
                                           session: Session) -> None:
    """Tests getting a drawing by ID, without providing a username"""

    with open('app/tests/testdata/input_geojson.json', 'rb') as fh:
        data = json.load(fh)
        # add a drawing
        drawing = DrawingTableRecord(objectName="This is a drawing",
                                     scope=ScopeEnum.USER,
                                     username="gwuser",
                                     geoJSON=data)
        drawing_id = str(drawing.id)
        session.add(drawing)
        session.commit()

        response = client.get('/drawings/' + drawing_id)
        assert response.headers.get('content-type') == 'application/json'
        # 400 response for invalid request as no username header is provided
        assert response.status_code == 400


def test_get_drawing_existing_empty_string_user(client: TestClient,
                                                session: Session) -> None:
    """Tests getting a drawing by ID, by providing empty string username"""

    with open('app/tests/testdata/input_geojson.json', 'rb') as fh:
        data = json.load(fh)
        # add a drawing
        drawing = DrawingTableRecord(objectName="This is a drawing",
                                     scope=ScopeEnum.USER,
                                     username="gwuser",
                                     geoJSON=data)
        drawing_id = str(drawing.id)
        session.add(drawing)
        session.commit()

        response = client.get('/drawings/' + drawing_id,
                              headers={'Geoweb-Username': ''})
        assert response.headers.get('content-type') == 'application/json'
        # _check_authenticated has failed and no username has been provided
        assert response.status_code == 401
        assert {'message': 'Not authenticated'} == response.json()


def test_get_drawing_existing_with_username(client: TestClient,
                                            session: Session) -> None:
    """Tests getting a drawing by ID, with providing a username"""

    with open('app/tests/testdata/input_geojson.json', 'rb') as fh:
        data = json.load(fh)
        # add a drawing
        drawing = DrawingTableRecord(objectName="This is a drawing",
                                     scope=ScopeEnum.USER,
                                     username="gwuser",
                                     geoJSON=data)
        drawing_id = str(drawing.id)
        # drawing_updateTime = drawing.lastUpdatedTime
        session.add(drawing)
        session.commit()

        response = client.get('/drawings/' + drawing_id,
                              headers={'Geoweb-Username': 'gwuser'})

        assert response.headers.get('content-type') == 'application/json'
        assert response.status_code == 200
        found_drawing = response.json()
        assert found_drawing['id'] == drawing_id
        assert found_drawing['objectName'] == 'This is a drawing'
        assert found_drawing['scope'] == 'user'
        assert found_drawing['geoJSON'] == data


def test_get_drawing_existing_invalid_user(client: TestClient,
                                           session: Session) -> None:
    """Tests getting a drawing by ID, with providing an invalid username"""

    with open('app/tests/testdata/input_geojson.json', 'rb') as fh:
        data = json.load(fh)
        # add a drawing
        drawing = DrawingTableRecord(objectName="This is a drawing",
                                     scope=ScopeEnum.USER,
                                     username="gwuser",
                                     geoJSON=data)
        drawing_id = str(drawing.id)
        # drawing_updateTime = drawing.lastUpdatedTime
        session.add(drawing)
        session.commit()

        response = client.get('/drawings/' + drawing_id,
                              headers={'Geoweb-Username': 'gwuser2'})

        assert response.headers.get('content-type') == 'application/json'
        assert response.status_code == 404
        assert {'message': 'Drawing not found'} == response.json()


def test_list_drawings_without_username(client: TestClient,
                                        seeded_drawings: None) -> None:
    """Check no drawings are listed when no username provided"""

    response = client.get('/drawings')
    assert response.status_code == 400


def test_list_drawings_empty_string_username(client: TestClient,
                                             seeded_drawings: None) -> None:
    """Check no drawings are listed when no username provided"""

    response = client.get('/drawings', headers={'Geoweb-Username': ''})
    assert response.status_code == 401
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert body['message'] == 'Not authenticated'


def test_list_drawings_with_only_globals(client: TestClient,
                                         seeded_drawings: None) -> None:
    """Tests listing drawings returns only globals and areas when username
    has no personal drawings"""

    response = client.get('/drawings', headers={'Geoweb-Username': 'newgwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 5
    # Check that the scope of the returned drawings is indeed global
    assert body[0]["scope"] == "global"
    assert body[1]["scope"] == "global"
    # Check that objects are returned first
    assert body[0]['keywords'] == 'Objects'
    assert body[1]['keywords'] == 'Objects'
    # Check that areas are returned after that
    assert not 'Objects' in body[2]['keywords']
    assert not 'Objects' in body[3]['keywords']
    assert not 'Objects' in body[4]['keywords']


def test_list_drawings_with_usr_without_slash(client: TestClient,
                                              seeded_drawings: None) -> None:
    """Tests listing drawings, with providing a username"""

    # Try with gwuser
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global, 3 areas

    # Try with gwuser2
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 10  # 5 personal, 2 global, 3 areas


def test_list_drawings_with_usr_with_slash(client: TestClient,
                                           seeded_drawings: None) -> None:
    """Tests listing drawings, with providing a username"""

    # Try with gwuser
    response = client.get('/drawings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global, 3 areas

    # Try with gwuser2
    response = client.get('/drawings/', headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 10  # 5 personal, 2 global, 3 areas


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_update_drawing_without_slash(client: TestClient):
    """Test updating a drawing without slash"""

    # post a drawing
    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # verify drawing is in database
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1

    # get drawing to update
    original_drawing = body[0]
    drawing_id = original_drawing['id']

    # check datetime formatting
    assert original_drawing['lastUpdatedTime'] == "2023-12-06T10:31:14Z"

    # change title
    updated_drawing = original_drawing.copy()
    updated_drawing['objectName'] = "Updated title"

    # POST update - move a bit forward in time
    with freeze_time("2023-12-06T15:31:14", tz_offset=0):
        response = client.post(f'/drawings/{drawing_id}',
                               json=updated_drawing,
                               headers={'Geoweb-Username': 'gwuser'})
        assert response.status_code == 201

        # retrieve list again
        response = client.get('/drawings',
                              headers={'Geoweb-Username': 'gwuser'})
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'application/json'
        body = response.json()
        assert len(body) == 1

        # id should be the same
        assert body[0]['id'] == drawing_id
        # check that title is updated
        assert body[0]['objectName'] == 'Updated title'
        # check that updatedtime is updated
        assert original_drawing['lastUpdatedTime'] != body[0]['lastUpdatedTime']
        assert body[0]['lastUpdatedTime'] == "2023-12-06T15:31:14Z"


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_update_drawing_with_slash(client: TestClient):
    """Test updating a drawing with slash"""

    # post a drawing
    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        response = client.post('/drawings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # verify drawing is in database
    response = client.get('/drawings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1

    # get drawing to update
    original_drawing = body[0]
    drawing_id = original_drawing['id']

    # check datetime formatting
    assert original_drawing['lastUpdatedTime'] == "2023-12-06T10:31:14Z"

    # change title
    updated_drawing = original_drawing.copy()
    updated_drawing['objectName'] = "Updated title"

    # POST update - move a bit forward in time
    with freeze_time("2023-12-06T15:31:14", tz_offset=0):
        response = client.post(f'/drawings/{drawing_id}/',
                               json=updated_drawing,
                               headers={'Geoweb-Username': 'gwuser'})
        assert response.status_code == 201

        # retrieve list again
        response = client.get('/drawings/',
                              headers={'Geoweb-Username': 'gwuser'})
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'application/json'
        body = response.json()
        assert len(body) == 1

        # id should be the same
        assert body[0]['id'] == drawing_id
        # check that title is updated
        assert body[0]['objectName'] == 'Updated title'
        # check that updatedtime is updated
        assert original_drawing['lastUpdatedTime'] != body[0]['lastUpdatedTime']
        assert body[0]['lastUpdatedTime'] == "2023-12-06T15:31:14Z"


def test_update_drawing_with_different_user(client: TestClient,
                                            seeded_drawings: None):
    """Test updating a drawing with different username"""

    # get list of user drawings
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get user drawing to update
    original_drawing = body[3]
    assert original_drawing["scope"] == "user"
    drawing_id = original_drawing['id']

    # change title
    updated_drawing = original_drawing.copy()
    updated_drawing['objectName'] = "Updated title"

    # POST update
    response = client.post(f'/drawings/{drawing_id}',
                           json=updated_drawing,
                           headers={'Geoweb-Username': 'otheruser'})
    assert response.status_code == 400
    assert response.json() == {'message': 'Drawing not found for user'}


def test_update_drawing_without_username(client: TestClient,
                                         seeded_drawings: None):
    """Test updating a drawing without username"""

    # get list of user drawings
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get drawing to update
    original_drawing = body[0]
    drawing_id = original_drawing['id']

    # change title
    updated_drawing = original_drawing.copy()
    updated_drawing['objectName'] = "Updated title"

    # POST update
    response = client.post(f'/drawings/{drawing_id}',
                           json=updated_drawing,
                           headers={'Geoweb-Username': ''})
    assert response.status_code == 401
    assert response.json() == {'message': 'Not authenticated'}


def test_update_nonexisting_drawing(client: TestClient, seeded_drawings: None):
    """Test updating a drawing that does not exist"""

    ## post first time
    with open('app/tests/testdata/input_drawing.json', 'rb') as fh:
        data = json.load(fh)
        data['id'] = 'drawing_id_string'
        data['lastUpdatedTime'] = '2023-09-01 10:32:23.442675'
        response = client.post('/drawings/non-existing-id/',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert response.json() == {'message': 'Drawing not found for user'}


def test_update_invalid_drawing(client: TestClient, seeded_drawings: None):
    """Test updating an invalid drawing"""

    # get list of user drawings
    response = client.get('/drawings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get drawing to update
    original_drawing = body[0]
    drawing_id = original_drawing['id']

    # change title
    updated_drawing = original_drawing.copy()
    updated_drawing['objectName'] = "Updated title"
    updated_drawing['scope'] = 'invalid'

    # POST update
    response = client.post(f'/drawings/{drawing_id}/',
                           json=updated_drawing,
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert "Input should be 'user' or 'global'" in response.json(
    )['message'][0]['msg']


def test_delete_drawing(client: TestClient, seeded_drawings: None):
    """Test deleting a drawing"""

    # get list of user drawings
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get drawing to delete
    original_drawing = body[2]
    drawing_id = original_drawing['id']

    # POST update
    response = client.post(f'/drawings/{drawing_id}?delete=true',
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 201

    # check if the list is now shorter
    assert len(
        client.get('/drawings', headers={
            'Geoweb-Username': 'gwuser'
        }).json()) == 7


def test_delete_drawing_wrong_scope(client: TestClient, seeded_drawings: None):
    """Test deleting a drawing with the wrong scope (in this case GLOBAL)"""

    # get list of user drawings
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get drawing to delete
    original_drawing = body[0]
    drawing_id = original_drawing['id']

    # POST update
    response = client.post(f'/drawings/{drawing_id}?delete=true',
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400

    # check if the list is still the same
    assert len(
        client.get('/drawings', headers={
            'Geoweb-Username': 'gwuser'
        }).json()) == 8


def test_delete_drawing_wrong_user(client: TestClient, seeded_drawings: None):
    """Test deleting with a wrong username"""

    # get list of user drawings
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get drawing to delete
    original_drawing = body[3]
    drawing_id = original_drawing['id']

    # POST update
    response = client.post(f'/drawings/{drawing_id}?delete=true',
                           headers={'Geoweb-Username': 'gwuser-wrong'})
    assert response.status_code == 400

    # check if the list is still the same
    assert len(
        client.get('/drawings', headers={
            'Geoweb-Username': 'gwuser'
        }).json()) == 8


def test_delete_drawing_with_delete_is_false(client: TestClient,
                                             seeded_drawings: None):
    """Test deleting a drawing without setting delete to true"""

    # get list of user drawings
    response = client.get('/drawings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 8  # 3 personal, 2 global and 3 areas

    # get drawing to delete
    original_drawing = body[0]
    drawing_id = original_drawing['id']

    # POST update
    response = client.post(f'/drawings/{drawing_id}?delete=false',
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400

    # check if the list is still the same
    assert len(
        client.get('/drawings', headers={
            'Geoweb-Username': 'gwuser'
        }).json()) == 8


def test_list_age_default(client: TestClient, seeded_timed_drawings: None,
                          session: Session):
    """Test listing drawings"""
    # get list of user drawings for default age
    response = client.get('/drawings/', headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 7


def test_list_age_30_days(client: TestClient, seeded_timed_drawings: None,
                          session: Session):
    """Test listing drawings"""
    # get list of user drawings for last 30 days
    response = client.get(f'/drawings/?age={30*24*3600}',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 7


def test_list_age_2_days(client: TestClient, seeded_timed_drawings: None,
                         session: Session):
    """get list of user drawings for last 2 days"""
    response = client.get(f'/drawings/?age={2*3600*24}',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 4


def test_list_search_one_term(client: TestClient, seeded_drawings: None,
                              session: Session):
    """Get list of drawings with a search term"""
    response = client.get('/drawings/?search=01',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 3
    assert "01" in body[0]['objectName']
    assert "01" in body[1]['objectName']
    assert "01" in body[2]['objectName']


def test_list_search_two_terms(client: TestClient, seeded_drawings: None,
                               session: Session):
    """Get list of drawings two serach terms"""
    response = client.get('/drawings/?search=01,area',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0]['objectName'] == "Area01"


def test_list_keywords(client: TestClient, seeded_drawings: None,
                       session: Session):
    """Test listing of keywords"""
    response = client.get("/area-keywords?language=en",
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 4

    # check that each value in list is unique
    assert len(set(body)) == len(body)


def test_list_keywords_invalid_language(client: TestClient,
                                        seeded_drawings: None,
                                        session: Session):
    """Test listing of keywords"""
    response = client.get("/area-keywords?language=invalid",
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 400
    body = response.json()
    assert body['message'][0][
        'msg'] == "Input should be 'en', 'nl', 'fi' or 'no'"


def test_list_search_capitals(client: TestClient, seeded_drawings: None,
                              session: Session):
    """Get list of drawings with a capitalized search word"""
    response = client.get('/drawings/?search=AREA',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 3
    assert body[0]['objectName'] == "Area01"
    assert body[1]['objectName'] == "Area02"
    assert body[2]['objectName'] == "Area03"


def test_list_search_lowercase(client: TestClient, seeded_drawings: None,
                               session: Session):
    """Get list of drawings with an all lowercase search word"""
    response = client.get('/drawings/?search=area',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 3
    assert body[0]['objectName'] == "Area01"
    assert body[1]['objectName'] == "Area02"
    assert body[2]['objectName'] == "Area03"


def test_list_filter_keyword(client: TestClient, seeded_drawings: None,
                             session: Session):
    """Get list of drawings with an all lowercase search word"""
    response = client.get('/drawings/?keywords=objects',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 7


def test_list_filter_multiple_keywords(client: TestClient,
                                       seeded_drawings: None, session: Session):
    """Get list of drawings with an all lowercase search word"""
    response = client.get('/drawings/?keywords=country_code,aviation',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0]['objectName'] == "Area02"


def test_list_filter_search_and_keywords(client: TestClient,
                                         seeded_drawings: None,
                                         session: Session):
    """Get list of drawings with an all lowercase search word"""
    response = client.get('/drawings/?keywords=country_code,aviation',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0]['objectName'] == "Area02"
