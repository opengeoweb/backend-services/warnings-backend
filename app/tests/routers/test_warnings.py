# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#pylint: disable=unused-argument,line-too-long,too-many-lines
"""Module to test warnings endpoints in warnings backend"""
import json
import logging
from copy import deepcopy
from datetime import datetime, timedelta, timezone

from fastapi.testclient import TestClient
from freezegun import freeze_time
from sqlmodel import Session

from app.config import settings, warnings_config
from app.models import DomainEnum, StatusEnum, WarningTableRecord
from app.utils.cleanup import cleanup_drafts

LOGGER = logging.getLogger(__name__)


def test_post_warning_without_slash(client: TestClient) -> None:
    """Tests posting a warning to URL without trailing slash"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_warning_with_slash(client: TestClient) -> None:
    """Tests posting a warning to URL with trailing slash"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}/',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_warning_with_id(client: TestClient) -> None:
    """Tests posting a warning with ID (EWC case)"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        # add ID
        data['warningDetail']['id'] = 'test_id'
        response = client.post('/warnings/',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]
    assert generated_id == 'test_id'

    # test that warning exists with get
    response = client.get('/warnings/test_id/',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_warning_with_ewc_fields(client: TestClient) -> None:
    """Tests posting a warning with ID and EWC fields"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        # add EWC specific fields
        data['warningDetail']['id'] = 'test_id'
        data['warningDetail']['type'] = 1
        data['warningDetail']['authority'] = 'knmi'

        response = client.post('/warnings/',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]
    assert generated_id == 'test_id'

    # test that warning exists with get
    response = client.get('/warnings/test_id/',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_warning_invalid(client: TestClient) -> None:
    """Test the post warning route with an invalid warning body"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # remove field to create an invalid body
        data = json.load(fh)
        del data['status']

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    message = response.json()['message']
    assert len(message) == 1
    assert "Field required" in message[0]['msg']


def test_post_warning_invalid_warningdetail(client: TestClient) -> None:
    """Test the post warning route with an invalid warningdetail body"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # remove field to create an invalid body
        data = json.load(fh)
        del data['warningDetail']['validFrom']

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert "Input should be a valid datetime" in response.json()['message']


def test_post_warning_invalid_phenomenon(client: TestClient) -> None:
    """Test the post warning route with an invalid phenomenon"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # change phenomenon to create invalid phenomenon
        data = json.load(fh)
        data['warningDetail']['phenomenon'] = 'invalid'

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert "Input should be 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp' or 'coastalEvent'" in response.json(
    )['message'][0]['msg']
    assert ['body', 'warningDetail', 'warningDetailStrict',
            'phenomenon'] == response.json()['message'][0]['loc']


def test_post_warning_missing_level_in_warningdetail(
        client: TestClient) -> None:
    """Test the post warning route with an invalid warningDetail"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # remove level to create invalid warningDetail
        data = json.load(fh)
        del data['warningDetail']['level']

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert "Invalid entry for level: Input should be 'moderate', 'severe' or 'extreme'" in response.json(
    )['message']


def test_post_warning_invalid_level(client: TestClient) -> None:
    """Test the post warning route with an invalid level"""
    data: dict
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # edit level to create invalid level
        data = json.load(fh)
        data['warningDetail']['level'] = 3

    response = client.post('/warnings',
                           json=data,
                           headers={'Geoweb-Username': 'gwuser'})
    resp_msg = response.json()["message"]

    assert response.status_code == 400
    assert ['body', 'warningDetail', 'warningDetailStrict',
            'level'] == resp_msg[0]['loc']
    assert "Input should be 'moderate', 'severe' or 'extreme'" in resp_msg[0][
        'msg']


def test_post_warning_nouser(client: TestClient) -> None:
    """Test the post warning route when no user is authenticated"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': ''})
    assert response.status_code == 401
    assert {'message': "Not authenticated"} == response.json()


@freeze_time("2023-09-19T10:31:14.00000", tz_offset=0)
def test_post_warning_check_datetime_fields(client: TestClient) -> None:
    """Post a warning and retrieve the warning. Check that the formatting
    of datetime fields is correct"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}/',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()
    assert content['lastUpdatedTime'] == "2023-09-19T10:31:14Z"
    assert content['warningDetail']['validFrom'] == "2023-09-20T11:31:14Z"
    assert content['warningDetail']['validUntil'] == "2023-09-20T18:31:15Z"


def test_update_warning_from_draft_to_publish(client: TestClient,
                                              caplog) -> None:
    """
    Test updating the status from DRAFT to PUBLISH of an existing warning
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try to publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        assert data['status'] == StatusEnum.PUBLISHED
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Update warning in database" in caplog.text
    assert "Successfully marked warning as PUBLISHED warning in database" in caplog.text


def test_update_warning_from_draft_to_draft(client: TestClient, caplog) -> None:
    """
    Test creating a draft warning and updating the warning with status DRAFT
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try update warning - status remains draft
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['status'] = StatusEnum.DRAFT
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning in database" in caplog.text
    assert "Publish warning" not in caplog.text


def test_update_warning_from_proposal_to_publish(client: TestClient,
                                                 caplog) -> None:
    """
    Test updating the status from PROPOSAL to PUBLISH of an existing warning
    """

    username = "test-user"
    # post a PROPOSAL warning
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'external_source'})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response['editor'] is None

    # set editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # now try to publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        assert data['status'] == StatusEnum.PUBLISHED
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Update warning in database" in caplog.text
    assert "Successfully marked warning as PUBLISHED warning in database" in caplog.text


def test_update_warning_from_proposal_to_draft(client: TestClient,
                                               caplog) -> None:
    """
    Test updating the status from PROPOSAL to DRAFT of an existing warning
    """

    username = "test-user"
    # post a PROPOSAL warning
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        data = json.load(fh)
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'external_source'})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response['editor'] is None

    # set editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # now try update warning - status remains draft
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['status'] = StatusEnum.DRAFT
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning in database" in caplog.text
    assert "Publish warning" not in caplog.text


def test_update_warning_from_todo_to_publish(client: TestClient,
                                             caplog) -> None:
    """
    Test updating the status from TODO to PUBLISH of an existing warning
    """

    username = "test-user"
    # post a TODO warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = StatusEnum.TODO
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try to publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        assert data['status'] == StatusEnum.PUBLISHED
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning in database" in caplog.text
    assert "Successfully marked warning as PUBLISHED warning in database" in caplog.text


def test_update_warning_from_todo_to_draft(client: TestClient, caplog) -> None:
    """
    Test updating the status from TODO to DRAFT of an existing warning
    """

    username = "test-user"
    # post a TODO warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = StatusEnum.TODO
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try update warning - status remains draft
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['status'] = StatusEnum.DRAFT
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning in database" in caplog.text
    assert "Publish warning" not in caplog.text


def test_update_warning_invalid_status_change_publish_draft(
        client: TestClient) -> None:
    """
    Test the post warning route when warning already exists
    in database but with an invalid status change
      --> try to change a published warning into a draft
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now try to set status to DRAFT
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        assert data['status'] == StatusEnum.DRAFT
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert "Status change not allowed" in response.text
    assert response.status_code == 400


def test_update_warning_invalid_status_change_draft_proposal(
        client: TestClient) -> None:
    """
    Test the post warning route when warning already exists
    in database but with an invalid status change
      --> try to change a draft warning into a proposal
    """

    # create a DRAFT warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try to set status to PROPOSAL
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['status'] = StatusEnum.PROPOSAL
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert "Status change not allowed" in response.text
    assert response.status_code == 400


def test_update_warning_invalid_status_change_todo_draft(
        client: TestClient) -> None:
    """
    Test the post warning route when warning already exists
    in database but with an invalid status change
      --> try to change a draft warning into a todo
    """

    # create a DRAFT warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try to set status to TODO
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['status'] = StatusEnum.TODO
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert "Status change not allowed" in response.text
    assert response.status_code == 400


def test_update_published_warning_to_draft(client: TestClient) -> None:
    """
    Test that a published warning cannot be set as DRAFT
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now try to set status to DRAFT
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['status'] = "DRAFT"
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert "Status change not allowed" in response.text
    assert response.status_code == 400


def test_update_published_warning(client: TestClient, caplog) -> None:
    """
    Test that a published warning can be updated with a new
    PUBLISHED warning
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # check contents from warning
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': "test-user"
                          }).json()
    assert response['warningDetail']['probability'] == 50
    assert response['warningDetail']['phenomenon'] == 'wind'
    assert response['warningDetail']['level'] == 'moderate'

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now update contents of data and update published warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        data['warningDetail']['probability'] = 60
        data['warningDetail']['phenomenon'] = 'rain'
        data['warningDetail']['level'] = 'severe'
        # make sure status = PUBLISHED
        assert data['status'] == 'PUBLISHED'
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # again retrieve warning and make sure contents were updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': "test-user"
                          }).json()
    assert response['warningDetail']['probability'] == 60
    assert response['warningDetail']['phenomenon'] == 'rain'
    assert response['warningDetail']['level'] == 'severe'

    # make sure editor is none
    assert response['editor'] is None

    # check logs for updating logs
    assert "Update warning in database: mark as PUBLISHED" in caplog.text
    assert "Updating warning in database" in caplog.text
    assert "Post to http://mock-publisher/publish" not in caplog.text


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_save_draft_update_published_warning(client: TestClient,
                                             caplog) -> None:
    """
    Test that a draft of an update of a published warning can be saved
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # check contents from warning
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': "test-user"
                          }).json()
    assert response['warningDetail']['probability'] == 50
    assert response['warningDetail']['phenomenon'] == 'wind'
    assert response['warningDetail']['level'] == 'moderate'

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # get the published data
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': "test-user"})
    data = response.json()
    # now update contents of data and save draft of update published warning
    data['status'] = 'DRAFT_PUBLISHED'
    data['warningDetail']['linked_to_id'] = generated_id
    data['warningDetail']['probability'] = 60
    data['warningDetail']['phenomenon'] = 'rain'
    data['warningDetail']['level'] = 'severe'
    response = client.post('/warnings',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    # ensure a location header is set
    assert response.headers.get('Location', None) is not None

    # check that an ID is returned, which is not equal
    # to the generated_id of the published warning
    draft_published_id = response.headers['Location'].split("/")[-2]
    assert generated_id != draft_published_id

    # try retrieving the warning list
    response = client.get('/warnings/',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    warning_list = response.json()
    # two elements should be returned in warning list
    assert len(warning_list) == 2

    # first item should be DRAFT_PUBLISHED
    draft_warning = warning_list[0]
    assert draft_warning['status'] == 'DRAFT_PUBLISHED'
    # make sure editor is set
    assert draft_warning['editor'] == 'test-user'
    # make sure contents are correct
    assert draft_warning['warningDetail']['probability'] == 60
    assert draft_warning['warningDetail']['phenomenon'] == 'rain'
    assert draft_warning['warningDetail']['level'] == 'severe'
    assert draft_warning['warningDetail']['linked_to_id'] == generated_id

    # second item should be the PUBLISHED warning
    published_warning = warning_list[1]
    assert published_warning['status'] == 'PUBLISHED'
    # make sure PUBLISHED warning has no editor
    assert published_warning['editor'] is None
    # make sure contents have not been updated
    assert published_warning['warningDetail']['probability'] == 50
    assert published_warning['warningDetail']['phenomenon'] == 'wind'
    assert published_warning['warningDetail']['level'] == 'moderate'

    assert "Unlock PUBLISHED warning" in caplog.text


def test_draft_published_status_change(client: TestClient, caplog) -> None:
    """
    Test that a DRAFT_PUBLISHED warning can only be changed into
    DRAFT_PUBLISHED or PUBLISHED statuses
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now update contents of data and save draft of update published warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'DRAFT_PUBLISHED'
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        data['warningDetail']['linked_to_id'] = generated_id
        data['warningDetail']['probability'] = 60
        data['warningDetail']['phenomenon'] = 'rain'
        data['warningDetail']['level'] = 'severe'
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    # ensure a location header is set
    assert response.headers.get('Location', None) is not None

    # retrieve the data and verify the status
    draft_published_id = response.headers['Location'].split("/")[-2]

    # retrieve the draft_published item
    response = client.get(f'/warnings/{draft_published_id}',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    data = response.json()

    # try to update the status to DRAFT
    data['status'] = 'DRAFT'
    response = client.post(f'/warnings/{draft_published_id}',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.json()['message'] == 'Status change not allowed'
    assert response.status_code == 400

    # try to update the status to IGNORED
    data['status'] = 'IGNORED'
    response = client.post(f'/warnings/{draft_published_id}',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.json()['message'] == 'Status change not allowed'
    assert response.status_code == 400

    # try to update the status to DRAFT_PUBLISHED
    data['status'] = 'DRAFT_PUBLISHED'
    response = client.post(f'/warnings/{draft_published_id}',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # try to update the status to PUBLISHED
    data['status'] = 'PUBLISHED'
    response = client.post(f'/warnings/{draft_published_id}',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_draft_published_update_as_draft(client: TestClient, caplog) -> None:
    """
    Test that a DRAFT_PUBLISHED warning can be updated with status
    DRAFT_PUBLISHED, just like a normal DRAFT
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now update contents of data and save draft of update published warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'DRAFT_PUBLISHED'
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        data['warningDetail']['linked_to_id'] = generated_id
        data['warningDetail']['probability'] = 60
        data['warningDetail']['phenomenon'] = 'rain'
        data['warningDetail']['level'] = 'severe'
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    # ensure a location header is set
    assert response.headers.get('Location', None) is not None

    # retrieve the data and verify the status
    draft_published_id = response.headers['Location'].split("/")[-2]

    # retrieve the draft_published item
    response = client.get(f'/warnings/{draft_published_id}',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    data = response.json()
    # check contents
    assert data['status'] == 'DRAFT_PUBLISHED'
    assert data['warningDetail']['probability'] == 60
    assert data['warningDetail']['phenomenon'] == 'rain'
    assert data['warningDetail']['level'] == 'severe'
    assert data['warningDetail']['linked_to_id'] == generated_id

    # modify the contents of the draft
    data['warningDetail']['probability'] = 90
    data['warningDetail']['level'] = 'extreme'
    response = client.post(f'/warnings/{draft_published_id}',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # retrieve the warning list to check the contents
    response = client.get('/warnings/',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    warning_list = response.json()
    assert len(warning_list) == 2

    assert warning_list[0]['status'] == 'DRAFT_PUBLISHED'
    assert warning_list[1]['status'] == 'PUBLISHED'
    assert warning_list[0]['warningDetail']['probability'] == 90
    assert warning_list[0]['warningDetail']['level'] == 'extreme'
    assert warning_list[1]['warningDetail']['probability'] == 50
    assert warning_list[1]['warningDetail']['level'] == 'moderate'


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_publish_draft_published_warning(client: TestClient, caplog) -> None:
    """
    Test that a DRAFT_PUBLISHED warning is published. Make sure the
    original PUBLISHED warning is updated and that the DRAFT_PUBLISHED
    item is removed
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now update contents of data and save draft of update published warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'DRAFT_PUBLISHED'
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        data['warningDetail']['linked_to_id'] = generated_id
        data['warningDetail']['probability'] = 60
        data['warningDetail']['phenomenon'] = 'rain'
        data['warningDetail']['level'] = 'severe'
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    # ensure a location header is set
    assert response.headers.get('Location', None) is not None

    # retrieve the data and verify the status
    draft_published_id = response.headers['Location'].split("/")[-2]

    # retrieve the draft_published item
    response = client.get(f'/warnings/{draft_published_id}',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    data = response.json()

    # modify the contents and publish the draft
    data['warningDetail']['probability'] = 90
    data['warningDetail']['level'] = 'extreme'
    data['status'] = 'PUBLISHED'
    response = client.post(f'/warnings/{draft_published_id}',
                           json=data,
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # retrieve the warning list to check the contents
    response = client.get('/warnings/',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200
    warning_list = response.json()
    assert len(warning_list) == 1
    assert warning_list[0]['id'] == generated_id
    assert warning_list[0]['status'] == 'PUBLISHED'
    assert warning_list[0]['warningDetail']['probability'] == 90
    assert warning_list[0]['warningDetail']['level'] == 'extreme'
    assert warning_list[0]['editor'] is None

    # make sure DRAFT_PUBLISHED was deleted
    response = client.get(f'/warnings/{draft_published_id}',
                          headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 404


def test_update_nonexisting_warning(client: TestClient) -> None:
    """
    Try to update a warning that does not exist in the database
    """

    # update warning that is not present in database
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = 'non-existing'
        response = client.post('/warnings/non-existing',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert "Could not update warning: warning not found in database" in response.text
    assert response.status_code == 400


def test_publish_new_warning(client: TestClient, caplog) -> None:
    """
    Test posting a warning that is also immediately published
    """

    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" not in caplog.text
    assert "Create a published warning in database" in caplog.text


def test_publish_new_warning_and_fetch_it(client: TestClient, caplog) -> None:
    """
    Test posting a warning that is also immediately published
    """

    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" not in caplog.text
    assert "Create a published warning in database" in caplog.text

    warning_url = response.headers['location']

    second_response = client.get(warning_url,
                                 headers={'Geoweb-Username': username})
    assert second_response.headers.get('content-type') == 'application/json'
    assert second_response.status_code == 200
    assert (
        second_response.json()['warningDetail']['domain'] == DomainEnum.PUBLIC)


def test_publish_new_warning_and_fetch_it_non_public(client: TestClient,
                                                     caplog) -> None:
    """
    Test posting a warning that is also immediately published
    """

    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        warningjson = json.load(fh)
        warningjson['warningDetail']['domain'] = 'maritime'
        response = client.post('/warnings',
                               json=warningjson,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" not in caplog.text
    assert "Create a published warning in database" in caplog.text

    warning_url = response.headers['location']

    second_response = client.get(warning_url,
                                 headers={'Geoweb-Username': username})
    assert second_response.headers.get('content-type') == 'application/json'
    assert second_response.status_code == 200
    assert (second_response.json()['warningDetail']['domain'] ==
            DomainEnum.MARITIME)


def test_publish_draft_warning(client: TestClient, caplog) -> None:
    """
    Test publishing a warning that was first saved as draft
    """

    username = "test-user"
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now try to publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" not in caplog.text
    assert "Update warning in database: mark as PUBLISHED" in caplog.text


def test_get_warning_non_existing(client: TestClient) -> None:
    """Tests getting a warning by a non-existing ID"""
    response = client.get('/warnings/non-existing-id',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 404
    assert {'message': 'Warning not found'} == response.json()


def test_get_warning_existing_without_user(client: TestClient,
                                           session: Session) -> None:
    """Tests getting a warning by ID, without providing a username"""

    # add a warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)['warningDetail']
        warning = WarningTableRecord(status="PUBLISHED", warningDetail=data)
        warning_id = str(warning.id)
        session.add(warning)
        session.commit()

        response = client.get('/warnings/' + warning_id)
        assert response.headers.get('content-type') == 'application/json'
        # Not found because not authenticated
        assert response.status_code == 400


def test_get_warning_existing_with_username(client: TestClient,
                                            session: Session) -> None:
    """Tests getting a warning by ID, with providing a username"""

    # add a warning - make sure it is valid
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)['warningDetail']
        warning = WarningTableRecord(status="PUBLISHED", warningDetail=data)
        warning_id = str(warning.id)
        session.add(warning)
        session.commit()

    response = client.get('/warnings/' + warning_id,
                          headers={'Geoweb-Username': 'gwuser'})

    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_warning = response.json()
    assert found_warning['id'] == warning_id
    assert found_warning['status'] == 'PUBLISHED'
    assert found_warning['warningDetail']['validFrom'] == "2023-09-20T11:31:14Z"
    assert found_warning['warningDetail'][
        'validUntil'] == "2023-09-20T18:31:15Z"


def test_post_warning_and_get_warning_different_user(
        client: TestClient) -> None:
    """Tests posting a warning with a username and retrieving
    a warning with a different username"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:

        response = client.post('/warnings/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}/',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.status_code == 200


def test_post_warning_draft(client: TestClient) -> None:
    """Test posting a warning with status DRAFT and missing fields"""

    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        # load data and make sure status is DRAFT
        data = json.load(fh)
        assert data['status'] == "DRAFT"
        # verify some fields are missing
        assert data.get('warningDetail').get('level', None) is None
        assert data.get('warningDetail').get('phenomenon', None) is None

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}/',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.json()['status'] == 'DRAFT'
    # verify data is still missing
    assert response.json().get('warningDetail').get('level', None) is None
    assert response.json().get('warningDetail').get('phenomenon', None) is None


def test_post_warning_missing_fields_publish(client: TestClient) -> None:
    """Test posting a warning with status PUBLISHED and missing fields"""

    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        # load data and set status to PUBLISHED
        data = json.load(fh)
        data['status'] = "PUBLISHED"
        # verify some fields are missing
        assert data.get('level', None) is None
        assert data.get('phenomenon', None) is None

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    # resposne should be 400 as warnings with missing fields
    # cannot be posted with status "PUBLISHED"
    assert response.status_code == 400
    assert "Input should be a valid string" in response.json()['message']


def test_post_warning_publish(client: TestClient) -> None:
    """Test posting a warning with status PUBLISHED"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # load data and check that status is published
        data = json.load(fh)
        assert data['status'] == "PUBLISHED"
        # verify all required fields are present
        assert data.get('warningDetail') is not None
        assert data['warningDetail'].get('level', None) is not None
        assert data['warningDetail'].get('phenomenon', None) is not None
        assert data['warningDetail'].get('validFrom', None) is not None
        assert data['warningDetail'].get('validUntil', None) is not None
        assert data['warningDetail'].get('level', None) is not None

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}/',
                          headers={'Geoweb-Username': 'gwuser2'})
    assert response.json()['status'] == 'PUBLISHED'
    # verify data still exists
    assert response.json()['warningDetail'].get('level', None) is not None
    assert response.json()['warningDetail'].get('phenomenon', None) is not None
    assert response.json()['warningDetail'].get('validFrom', None) is not None
    assert response.json()['warningDetail'].get('validUntil', None) is not None
    assert response.json()['warningDetail'].get('level', None) is not None


def test_post_warning_invalid_status(client: TestClient) -> None:
    """Test posting a warning with invalid status"""

    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # change status to an invalid value
        data = json.load(fh)
        data['status'] = "INVALID"

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400

    message = response.json()['message']
    # verify validation error
    assert len(message) == 1
    assert "'PROPOSAL', 'PUBLISHED', 'EXPIRED', 'DRAFT'" in message[0]['msg']


def test_post_warning_invalid_proposal(client: TestClient) -> None:
    """Test posting a warning with proposal status"""

    # try posting an incomplete warning with status proposal
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        # make data invalid
        data = json.load(fh)
        del data['warningDetail']

        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    # should fail because a PROPOSAL should be a complete warning
    # without missing fields
    assert response.status_code == 400

    # try again with a complete warning with status proposal
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    # should succeed because a PROPOSAL should be a complete warning
    # without missing fields
    assert response.status_code == 200


def test_get_warninglist_trailing_slash(client: TestClient,
                                        seeded_warnings) -> None:
    """Test retrieving the warning list"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_get_warninglist_no_trailing_slash(client: TestClient,
                                           seeded_warnings) -> None:
    """Test retrieving the warning list"""
    response = client.get('/warnings', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_get_warninglist_unauthenticated(client: TestClient,
                                         seeded_warnings) -> None:
    """Test retrieving the warning list"""
    response = client.get('/warnings')
    assert response.status_code == 400
    assert 'Field required' in response.json()['message'][0]['msg']
    assert 'Geoweb-Username' in response.json()['message'][0]['loc']


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_status(client: TestClient,
                                         seeded_warnings) -> None:
    """Test retrieving the warning list"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()
    assert len(content) == 13

    # check statuses in list
    assert content[0]['status'] == StatusEnum.TODO
    assert content[1]['status'] == StatusEnum.TODO
    assert content[2]['status'] == StatusEnum.TODO
    assert content[3]['status'] == StatusEnum.TODO
    assert content[4]['status'] == StatusEnum.TODO
    assert content[5]['status'] == StatusEnum.DRAFT
    assert content[6]['status'] == StatusEnum.DRAFT
    assert content[7]['status'] == StatusEnum.DRAFT
    assert content[8]['status'] == StatusEnum.PUBLISHED
    assert content[9]['status'] == StatusEnum.PUBLISHED
    assert content[10]['status'] == StatusEnum.PUBLISHED
    assert content[11]['status'] == StatusEnum.EXPIRED
    assert content[12]['status'] == StatusEnum.EXPIRED


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_todo(client: TestClient,
                                       seeded_warnings) -> None:
    """Test the ordering of warnings in TODO section"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()

    # check ordering of TODO based on lastUpdatedTime
    assert content[0]['lastUpdatedTime'] > content[1]['lastUpdatedTime']
    assert content[1]['lastUpdatedTime'] > content[2]['lastUpdatedTime']
    assert content[2]['lastUpdatedTime'] > content[3]['lastUpdatedTime']
    assert content[3]['lastUpdatedTime'] > content[4]['lastUpdatedTime']


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_draft(client: TestClient,
                                        seeded_warnings) -> None:
    """Test the ordering of warnings in DRAFT section"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()

    # check ordering of DRAFT based on lastUpdatedTime
    assert content[5]['lastUpdatedTime'] > content[6]['lastUpdatedTime']
    assert content[6]['lastUpdatedTime'] > content[7]['lastUpdatedTime']


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_publish(client: TestClient,
                                          seeded_warnings) -> None:
    """Test the ordering of warnings in PUBLISHED section"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()

    # check ordering of PUBLISHED based on lastUpdatedTime
    assert content[8]['lastUpdatedTime'] > content[9]['lastUpdatedTime']
    assert content[9]['lastUpdatedTime'] > content[10]['lastUpdatedTime']


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_expire(client: TestClient,
                                         seeded_warnings) -> None:
    """Test the ordering of warnings in EXPIRED section"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()

    # check ordering of EXPIRED based on lastUpdatedTime
    assert content[11]['lastUpdatedTime'] > content[12]['lastUpdatedTime']


@freeze_time("2023-12-12T10:31:14", tz_offset=0)
def test_get_warninglist_update_expire(client: TestClient,
                                       seeded_warnings) -> None:
    """Move the freeze_time forward in time and observe more warnings
    are marked as expired"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()
    assert len(content) == 9

    # check that ordering of the list has changed
    # less are returned because the expiration
    # is over 24 hours
    # some TODO have been moved to EXPIRED
    assert content[0]['status'] == StatusEnum.TODO
    assert content[1]['status'] == StatusEnum.TODO
    assert content[2]['status'] == StatusEnum.TODO
    assert content[3]['status'] == StatusEnum.DRAFT
    assert content[4]['status'] == StatusEnum.DRAFT
    assert content[5]['status'] == StatusEnum.DRAFT
    assert content[6]['status'] == StatusEnum.PUBLISHED
    assert content[7]['status'] == StatusEnum.EXPIRED
    assert content[8]['status'] == StatusEnum.EXPIRED


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_after_posting_draft_warning(
        client: TestClient, seeded_warnings) -> None:
    """Test the ordering of warnings after posting a DRAFT"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()
    list_length = len(content)

    # check draft order before posting
    assert content[5]['status'] == StatusEnum.DRAFT
    assert content[6]['status'] == StatusEnum.DRAFT
    assert content[7]['status'] == StatusEnum.DRAFT

    # post draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # again make warning list call
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    # check content
    content = response.json()
    assert len(content) == list_length + 1
    assert content[5]['status'] == StatusEnum.DRAFT
    assert content[6]['status'] == StatusEnum.DRAFT
    assert content[7]['status'] == StatusEnum.DRAFT
    assert content[8]['status'] == StatusEnum.DRAFT
    # check that ID matches
    assert content[5]['id'] == generated_id


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_ordering_after_posting_publish_warning(
        client: TestClient, seeded_warnings) -> None:
    """Test the ordering of warnings after posting a PUBLISHED"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()
    list_length = len(content)

    # check published order before posting
    assert content[8]['status'] == StatusEnum.PUBLISHED
    assert content[9]['status'] == StatusEnum.PUBLISHED

    # post published warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = "2023-12-06T12:31:14Z"
        data['warningDetail']['validUntil'] = "2023-12-06T18:31:14Z"
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # again make warning list call
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    # check content
    content = response.json()
    assert len(content) == list_length + 1
    assert content[8]['status'] == StatusEnum.PUBLISHED
    assert content[9]['status'] == StatusEnum.PUBLISHED
    assert content[10]['status'] == StatusEnum.PUBLISHED
    # check that ID matches
    assert content[8]['id'] == generated_id


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_get_warninglist_showing_no_ignored_warnings(
        client: TestClient, seeded_warnings_with_ignored) -> None:
    """Test that the warnings list does not show IGNORED warnings"""
    response = client.get('/warnings/', headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    content = response.json()
    cnt_ignored = 0
    for warning in content:
        if warning['status'] == "IGNORED":
            cnt_ignored = cnt_ignored + 1
    assert cnt_ignored == 0
    assert len(content) == 11


def test_delete_draft_warning(client: TestClient, caplog) -> None:
    """
    Test deleting a DRAFT warning
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure 1 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1

    # now try to delete the warning
    response = client.post(f'/warnings/{generated_id}?delete=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Deleting warning in database" in caplog.text
    assert "Successfully deleted warning in database" in caplog.text

    # make sure 0 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 0


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_delete_publish_warning(client: TestClient, caplog) -> None:
    """
    Test that deleting a PUBLISHED warning is not allowed
    """

    username = "test-user"
    # publish a warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure 1 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1

    # assign editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # now try to delete the warning
    response = client.post(f'/warnings/{generated_id}?delete=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 400
    assert "Deletion of warning only allowed if status is DRAFT" in response.text

    # check logs
    assert "Deleting warning in database" in caplog.text
    assert "Deletion of warning only allowed if status is DRAFT" in caplog.text

    # make sure 1 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': 'gwuser'
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1


def test_delete_warning_if_editor(client: TestClient, caplog) -> None:
    """
    Test deleting a warning if you are the editor
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure 1 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1
    # make sure that editor is set to "test-user"
    assert response[0]['editor'] == username

    # now try to delete the warning
    response = client.post(f'/warnings/{generated_id}?delete=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Deleting warning in database" in caplog.text
    assert "Successfully deleted warning in database" in caplog.text

    # make sure 0 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 0


def test_delete_warning_if_no_editor(client: TestClient, caplog) -> None:
    """
    Test deleting a warning if there is no editor for the warning
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure 1 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1

    # remove the editor
    response = client.post(f'/warnings/{generated_id}?editor=false',
                           headers={'Geoweb-Username': username})

    # make sure that editor is None for warning in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1
    assert response[0]['editor'] is None

    # now try to delete the warning
    response = client.post(f'/warnings/{generated_id}?delete=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Deleting warning in database" in caplog.text
    assert "Successfully deleted warning in database" in caplog.text

    # make sure 0 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 0


def test_delete_warning_if_other_editor(client: TestClient, caplog) -> None:
    """
    Test you can delete a warning if another user is the editor
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure 1 warnings exists in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1

    # take over editor with another user
    other_username = 'other-user'
    response = client.post(f'/warnings/{generated_id}?editor=true&force=true',
                           headers={'Geoweb-Username': other_username})

    # make sure that editor is updated for warning in database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 1
    assert response[0]['editor'] == other_username

    # now try to delete the warning
    response = client.post(f'/warnings/{generated_id}?delete=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # make sure the warning has been deleted from the database
    response = client.get('/warnings/', headers={
        'Geoweb-Username': username
    }).json()
    assert isinstance(response, list)
    assert len(response) == 0


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_update_or_delete_invalid_request(client: TestClient, caplog) -> None:
    """
    Test response of an invalid request where delete is false and no
    payload is sent
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # now make an invalid request
    response = client.post(f'/warnings/{generated_id}?delete=true&editor=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 400
    assert "Invalid request" in response.text


def test_editor_new_draft_warning(client: TestClient) -> None:
    """
    Test the editor value after saving a DRAFT
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == "test-user"


def test_no_editor_new_publish_warning(client: TestClient) -> None:
    """
    Test the editor value after saving a PUBLISH
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    # a published warning is saved with editor: None
    assert response["editor"] is None


def test_assign_and_unassign_warning(client: TestClient, caplog) -> None:
    """
    Test assigning and unassigning a warning
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    # warning editor should be assigned
    assert response["editor"] == "test-user"

    # update warning to unassign
    response = client.post(f'/warnings/{generated_id}?editor=false',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning editor in database" in caplog.text
    assert "Successfully updated warning editor" in caplog.text

    # again retrieve warning and make
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    #  make sure editor is none
    assert response["editor"] is None

    # update warning to assign editor again
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning editor in database" in caplog.text
    assert "Successfully updated warning editor" in caplog.text

    # again retrieve warning and make
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    # make sure warning editor is assigned
    assert response["editor"] == username


def test_unassign_after_publish(client: TestClient, caplog) -> None:
    """
    Test that a warning is unassigned after publishing
    """

    username = "test-user"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that the warning is unassigned
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == "test-user"

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # again retrieve warning
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    #  make sure editor is none
    assert response["editor"] is None


def test_assign_takeover(client: TestClient, caplog) -> None:
    """
    Test taking over an assigned warning
    """

    username1 = "test-user-1"
    username2 = "test-user-2"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username1})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == username1

    # take over warning editor assignment
    response = client.post(f'/warnings/{generated_id}?editor=true&force=true',
                           headers={'Geoweb-Username': username2})
    assert response.status_code == 200

    # check logs
    assert "Updating warning editor in database" in caplog.text
    assert "Successfully updated warning editor" in caplog.text

    # again retrieve warning
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    #  make sure editor is none
    assert response["editor"] == username2


def test_assign_warning_other_editor_without_force(client: TestClient) -> None:
    """
    Test assigning an editor to a warning that has another without using the force
    """

    username1 = "test-user-1"
    username2 = "test-user-2"

    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username1})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == username1

    # try setting test-user-2 as editor without force
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': username2})

    # this should not be possible
    assert response.status_code == 409
    assert "Cannot assign editor of warning that is being edited by another user." in response.text


def test_assign_warning_other_editor_with_force(client: TestClient) -> None:
    """
    Test assigning an editor to a warning that has another using the force
    """

    username1 = "test-user-1"
    username2 = "test-user-2"

    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username1})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == username1

    # try setting test-user-2 as editor with force
    response = client.post(f'/warnings/{generated_id}?editor=true&force=true',
                           headers={'Geoweb-Username': username2})

    # this should be possible
    assert response.status_code == 200


def test_assign_warning_no_editor_no_force(client: TestClient) -> None:
    """
    Test assigning an editor to a warning that has no editor without the force
    """

    username1 = "test-user-1"
    username2 = "test-user-2"

    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username1})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == username1

    # now unset the editor
    response = client.post(f'/warnings/{generated_id}?editor=false',
                           headers={'Geoweb-Username': username1})
    assert response.status_code == 200

    # try setting test-user-2 as editor without force
    response = client.post(f'/warnings/{generated_id}?editor=true&force=false',
                           headers={'Geoweb-Username': username2})

    # this should be possible
    assert response.status_code == 200


def test_cannot_update_if_warning_assigned(client: TestClient) -> None:
    """
    Test that a user cannot update warning if warning is assigned by other user
    """

    username1 = "test-user-1"
    username2 = "test-user-2"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username1})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == username1

    # now other user tries to update warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response['id'] = generated_id
        response['warningDetail']['id'] = generated_id
        response = client.post(f'/warnings/{generated_id}',
                               json=response,
                               headers={'Geoweb-Username': username2})
    assert "Warning is being edited by another user and cannot be updated" in response.text
    assert response.status_code == 400


def test_unassigning_warning(client: TestClient) -> None:
    """
    Test that only the editor can unassign a warning
    """

    username1 = "test-user-1"
    username2 = "test-user-2"
    # post a draft warning
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username1})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure the warning exists
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # a draft is saved with an editor equal to the username header
    assert response["editor"] == username1

    # now other user tries unassign warning
    response = client.post(f'/warnings/{generated_id}?editor=false',
                           headers={'Geoweb-Username': username2})
    assert "Cannot unassign editor of warning that is being edited by another user" in response.text
    assert response.status_code == 400

    # now editor tries unassign warning
    response = client.post(f'/warnings/{generated_id}?editor=false',
                           headers={'Geoweb-Username': username1})
    assert response.status_code == 200

    # verify warning editor is unassigned
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username1
                          }).json()
    # make sure editor is set to None
    assert response["editor"] is None


def test_publish_new_warning_with_publisher_configured(httpx_mock,
                                                       client: TestClient,
                                                       caplog) -> None:
    '''
    Test publishing a new warning when a warning publisher has been set up
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"

    # set mock response
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # proceed with publishing
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        # do mock request
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'
    settings.warning_publisher_url = None


def test_publish_draft_warning_with_publisher_configured(
        httpx_mock, client: TestClient, caplog) -> None:
    '''
    Test publishing a draft warning when a warning
    publisher has been set up
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"

    # save draft
    username = "test-user"
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # mock publish response
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        response_data['id'] = generated_id
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # now try to publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Update warning in database: mark as PUBLISHED" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'
    settings.warning_publisher_url = None


def test_update_published_warning_with_publisher_configured(
        httpx_mock, client: TestClient, caplog) -> None:
    '''
    Test updating of a published warning when a warning publisher has been set up
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    # mock response publish
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'

    # update contents of the published warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['probability'] = 60
    update_warning['warningDetail']['phenomenon'] = 'thunderstorm'
    update_warning['warningDetail']['id'] = generated_id
    update_warning['editor'] = username

    # mock response publish update
    # with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
    response_data = deepcopy(update_warning)['warningDetail']
    response_data['areas'][0]['published_warning_uuid'] = 'mock_updated_uuid'
    httpx_mock.add_response(url="http://mock-publisher/publish",
                            method="POST",
                            status_code=200,
                            json=response_data)

    # unlock warning for editing
    client.post(f'/warnings/{generated_id}?editor=true',
                headers={'Geoweb-Username': username})

    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Update warning in database: mark as PUBLISHED" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text
    assert "Incoming published_warning_uuid equal to DB" in caplog.text

    # check warning was successfully updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response.get('warningDetail').get('probability') == 60
    assert response.get('warningDetail').get('phenomenon') == 'thunderstorm'
    # check published_warning_uuid is updated
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_updated_uuid'

    # clean up environment vars
    settings.warning_publisher_url = None


def test_update_published_warning_with_publisher_configured_missing_published_uuid(
        httpx_mock, client: TestClient, caplog) -> None:
    '''
    Test updating of a published warning when the published_warning_uuid is missing
    from the payload
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    # mock response publish
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'

    # update contents of the published warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['probability'] = 60
    update_warning['warningDetail']['phenomenon'] = 'thunderstorm'
    update_warning['warningDetail']['id'] = generated_id
    update_warning['editor'] = username
    del update_warning['warningDetail']['areas'][0]['published_warning_uuid']

    # mock response publish update
    # with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
    response_data = deepcopy(update_warning)['warningDetail']
    response_data['areas'][0]['published_warning_uuid'] = 'mock_updated_uuid'
    httpx_mock.add_response(url="http://mock-publisher/publish",
                            method="POST",
                            status_code=200,
                            json=response_data)

    # unlock warning for editing
    client.post(f'/warnings/{generated_id}?editor=true',
                headers={'Geoweb-Username': username})

    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Update warning in database: mark as PUBLISHED" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text
    assert "No incoming UUID found - set UUID from DB" in caplog.text

    # check warning was successfully updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response.get('warningDetail').get('probability') == 60
    assert response.get('warningDetail').get('phenomenon') == 'thunderstorm'
    # check published_warning_uuid is updated
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_updated_uuid'

    # clean up environment vars
    settings.warning_publisher_url = None


def test_update_published_warning_with_publisher_configured_missing_areas(
        httpx_mock, client: TestClient, caplog) -> None:
    '''
    Test updating of a published warning when the areas object is missing
    from the payload. Warning can only be updated when it is saved as
    DRAFT - it cannot be published
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    # mock response publish
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'

    # update contents of the published warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['probability'] = 60
    update_warning['warningDetail']['phenomenon'] = 'thunderstorm'
    update_warning['warningDetail']['id'] = generated_id
    update_warning['editor'] = username

    # unlock warning for editing
    client.post(f'/warnings/{generated_id}?editor=true',
                headers={'Geoweb-Username': username})

    # remove "areas" and post update
    del update_warning['warningDetail']['areas']
    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 400

    assert "Areas missing: Could not set published_warning_uuid correctly" in caplog.text

    # now change status to DRAFT_PUBLISHED
    update_warning['status'] = 'DRAFT_PUBLISHED'
    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Updating warning in database" in caplog.text
    assert "No incoming UUID found - set UUID from DB" in caplog.text
    assert "Areas missing: generate empty areaItem" in caplog.text

    # check warning was successfully updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response.get('warningDetail').get('probability') == 60
    assert response.get('warningDetail').get('phenomenon') == 'thunderstorm'
    # check published_warning_uuid has not changed
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'
    assert response.get('warningDetail').get('areas')[0].get(
        'objectName') == '-'

    # clean up environment vars
    settings.warning_publisher_url = None


def test_update_published_warning_with_publisher_configured_different_published_uuid(
        httpx_mock, client: TestClient, caplog) -> None:
    '''
    Test updating of a published warning when the payload contains a different
    published_warning_uuid than in the database. Updating should fail
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    # mock response publish
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'

    # update contents of the published warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['probability'] = 60
    update_warning['warningDetail']['phenomenon'] = 'thunderstorm'
    update_warning['warningDetail']['id'] = generated_id
    update_warning['editor'] = username
    update_warning['warningDetail']['areas'][0][
        'published_warning_uuid'] = 'other-uuid'

    # unlock warning for editing
    client.post(f'/warnings/{generated_id}?editor=true',
                headers={'Geoweb-Username': username})
    # post update
    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 400
    assert "Incoming published_warning_uuid different from DB." in response.text

    # check logs
    assert "Incoming published_warning_uuid different from DB." in caplog.text

    # clean up environment vars
    settings.warning_publisher_url = None


def test_publish_error_with_publisher_configured(httpx_mock, client: TestClient,
                                                 caplog) -> None:
    '''
    Test publishing a draft warning when a warning
    publisher has been set up
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    httpx_mock.add_response(url="http://mock-publisher/publish",
                            method="POST",
                            status_code=400,
                            json={"detail": "mock_error"})

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 400

    assert response.json() == {'message': 'PublishError: mock_error'}

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Non-20x status code returned" in caplog.text
    settings.warning_publisher_url = None


def test_error_update_published_warning_with_publisher_configured(
        httpx_mock, client: TestClient, caplog) -> None:
    '''
    Test error catching for updating of a published warning
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        mock_response = json.load(fh)['warningDetail']
        mock_response['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=mock_response)

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'

    # update contents of the published warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['id'] = generated_id
    update_warning['warningDetail']['probability'] = 60
    update_warning['warningDetail']['phenomenon'] = 'thunderstorm'
    update_warning['editor'] = username

    # mock update error
    httpx_mock.add_response(url="http://mock-publisher/publish",
                            method="POST",
                            status_code=400,
                            json={"detail": "mock update error"})

    # unlock warning for editing
    client.post(f'/warnings/{generated_id}?editor=true',
                headers={'Geoweb-Username': username})

    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert 'mock update error' in response.json()['message']

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Non-20x status code returned" in caplog.text

    # check warning was successfully updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response.get('warningDetail').get('probability') != 60
    assert response.get('warningDetail').get('phenomenon') != 'thunderstorm'

    # if publishing faild, warning remains locked for editor
    assert response.get('editor') == username

    # clean up environment vars
    settings.warning_publisher_url = None


def test_update_warning_from_proposal_to_ignored(client: TestClient,
                                                 caplog) -> None:
    """
    Test updating the status from PROPOSAL to IGNORED of an existing warning
    """

    username = "test-user"
    # post a PROPOSAL warning
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'external_source'})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none
    saved_data = client.get(f'/warnings/{generated_id}',
                            headers={
                                'Geoweb-Username': username
                            }).json()
    assert saved_data['editor'] is None

    # set last proposal to ignored and post
    saved_data['status'] = StatusEnum.IGNORED
    response = client.post(f'/warnings/{generated_id}',
                           json=saved_data,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # retrieve changed warning and assert that it has status=="IGNORED"
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': username})
    assert response.json()["status"] == "IGNORED"

    # check logs
    assert "Updating warning in database" in caplog.text


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_update_warning_from_proposal_to_used_draft(client: TestClient,
                                                    caplog) -> None:
    """
    Test updating the status from PROPOSAL to USED of an proposal
    warning when the proposal is used and saved as draft
    """

    username = "test-user"
    # post a PROPOSAL warning
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'external_source'})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none
    saved_data = client.get(f'/warnings/{generated_id}',
                            headers={
                                'Geoweb-Username': username
                            }).json()
    assert saved_data['editor'] is None

    # a proposal will be marked as used if a new warning is saved
    # with a field "proposal_id" that contains the warning_id of the
    # proposal
    # post a DRAFT warning with a proposal_id field equal to generated ID
    with open('app/tests/testdata/warning_draft.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['proposal_id'] = generated_id
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'external_source'})
    assert response.status_code == 200
    generated_draft_id = response.headers['Location'].split('/')[-2]

    # retrieve the proposal warning and ensure status is USED
    used_data = client.get(f'/warnings/{generated_id}',
                           headers={
                               'Geoweb-Username': username
                           }).json()
    assert used_data['status'] == 'USED'

    # retrieve the warning list
    warning_list = client.get('/warnings',
                              headers={
                                  'Geoweb-Username': username
                              }).json()
    assert len(warning_list) == 1
    # make sure that proposal is not returned in warning
    # list, only the draft should be in the warning list
    assert warning_list[0]['id'] == generated_draft_id
    assert warning_list[0]['status'] == 'DRAFT'
    assert warning_list[0]['id'] != generated_id
    # check logs
    assert "Successfully marked proposal as used" in caplog.text


@freeze_time("2023-09-19T10:31:14", tz_offset=0)
def test_update_warning_from_proposal_to_used_publish(client: TestClient,
                                                      caplog) -> None:
    """
    Test updating the status from PROPOSAL to USED of an proposal
    warning when the proposal is used and published
    """

    username = "test-user"
    # post a PROPOSAL warning
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'external_source'})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none
    saved_data = client.get(f'/warnings/{generated_id}',
                            headers={
                                'Geoweb-Username': username
                            }).json()
    assert saved_data['editor'] is None
    assert saved_data['status'] == 'PROPOSAL'

    # a proposal will be marked as used if a new warning is saved
    # with a field "proposal_id" that contains the warning_id of the
    # proposal
    # post a PUBLISHED warning with a proposal_id field equal to generated ID
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['proposal_id'] = generated_id
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'external_source'})
    assert response.status_code == 200
    generated_publish_id = response.headers['Location'].split('/')[-2]

    # retrieve the proposal warning and ensure status is USED
    used_data = client.get(f'/warnings/{generated_id}',
                           headers={
                               'Geoweb-Username': username
                           }).json()
    assert used_data['status'] == 'USED'
    assert used_data['editor'] is None

    # retrieve the warning list
    warning_list = client.get('/warnings',
                              headers={
                                  'Geoweb-Username': username
                              }).json()
    assert len(warning_list) == 1
    # make sure that proposal is not returned in warning
    # list, only the draft should be in the warning list
    assert warning_list[0]['id'] == generated_publish_id
    assert warning_list[0]['status'] == 'PUBLISHED'
    assert warning_list[0]['id'] != generated_id
    # check logs
    assert "Successfully marked proposal as used" in caplog.text


def test_update_warning_from_proposal_publish_error(client: TestClient,
                                                    httpx_mock, caplog) -> None:
    """
    Test updating the status from PROPOSAL to USED of an proposal
    warning when the proposal is published, but a failure occurs
    while publishing. In this case, the PROPOSAL should NOT be
    marked as used
    """

    # mock response from configured publisher that generates an error
    settings.warning_publisher_url = "http://mock-publisher"
    httpx_mock.add_response(url="http://mock-publisher/publish",
                            method="POST",
                            status_code=400,
                            json={"detail": "mock_error"})

    username = "test-user"
    # post a PROPOSAL warning
    with open('app/tests/testdata/warning_proposal.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'external_source'})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none and status is PROPOSAL
    saved_data = client.get(f'/warnings/{generated_id}',
                            headers={
                                'Geoweb-Username': username
                            }).json()
    assert saved_data['editor'] is None
    assert saved_data['status'] == 'PROPOSAL'

    # try to publish a warning with a proposal_id field equal to generated ID
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['proposal_id'] = generated_id
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'external_source'})
    assert response.status_code == 400

    # retrieve the proposal warning and ensure status is not USED
    used_data = client.get(f'/warnings/{generated_id}',
                           headers={
                               'Geoweb-Username': username
                           }).json()
    assert used_data['status'] == 'PROPOSAL'

    settings.warning_publisher_url = None


def test_update_warning_from_draft_to_ignored(client: TestClient,
                                              caplog) -> None:
    """
    Test updating the status from DRAFT to IGNORED of an existing warning
    This update should fail
    """

    username = "test-user"
    # post a DRAFT warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = StatusEnum.DRAFT
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': username})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # make sure that editor is none
    saved_data = client.get(f'/warnings/{generated_id}',
                            headers={
                                'Geoweb-Username': username
                            }).json()
    assert saved_data['editor'] == username

    # set ignored
    saved_data['status'] = StatusEnum.IGNORED
    response = client.post(f'/warnings/{generated_id}',
                           json=saved_data,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 400

    # check logs
    assert "Status change from DRAFT to StatusEnum.IGNORED not allowed" in caplog.text


def test_warning_probability_invalid(client: TestClient):
    """Tests that invalid probability values raise a ValueError."""
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        invalid_values = [35, 45, 55, 65, 75, 85, 95]
        for value in invalid_values:
            data['warningDetail']['probability'] = value
            response = client.post('/warnings/',
                                   json=data,
                                   headers={'Geoweb-Username': 'gwuser'})
            assert response.status_code == 400
            assert f"Probability should be one of the following values: {list(range(10,101,10))}" in response.text


def test_moderate_warning_probability_invalid(client: TestClient):
    """Tests that invalid probability values raise a ValueError."""
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        value = 20
        data['warningDetail']['probability'] = value
        response = client.post('/warnings/',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
        assert response.status_code == 400
        assert "Probability should be 30 or higher for moderate warning" in response.text


def test_warning_validate_probability(client: TestClient):
    """Tests that valid probability values pass validation."""
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        response = client.post('/warnings/',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
        assert response.status_code == 200


def test_warning_probability_cross_validation(client: TestClient):
    """Tests that probability values are
    cross-validated with the level field."""
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        level_probabilities = {
            "moderate": [40, 50, 60, 70, 80, 90, 100],
            "severe": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
            "extreme": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        }
        for level, probabilities in level_probabilities.items():
            for value in probabilities:
                data['warningDetail']['level'] = level
                data['warningDetail']['probability'] = value
                response = client.post('/warnings/',
                                       json=data,
                                       headers={'Geoweb-Username': 'gwuser'})
                assert response.status_code == 200


def test_publish_warning_multiple_areas(client: TestClient, caplog):
    """Test publishing a warning with multiple areas, without
    a publisher configured"""

    username = "test-user"
    with open('app/tests/testdata/warning_publish_areas.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" not in caplog.text
    assert "Create a published warning in database" in caplog.text

    # retrieve warning and check that it has multiple areas
    response = client.get(f"/warnings/{generated_id}",
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response['status'] == "PUBLISHED"
    assert isinstance(response['warningDetail']['areas'], list)
    assert len(response['warningDetail']['areas']) == 3
    assert not "published_warning_uuid" in response['warningDetail']['areas'][0]
    assert not "published_warning_uuid" in response['warningDetail']['areas'][1]
    assert not "published_warning_uuid" in response['warningDetail']['areas'][2]


def test_publish_warning_multiple_areas_with_publisher(client: TestClient,
                                                       caplog, httpx_mock):
    """Test publishing a warning with multiple areas, with
    a publisher configured"""

    # mock publisher setup
    settings.warning_publisher_url = "http://mock-publisher"
    # mock response publish endpoint
    with open('app/tests/testdata/warning_publish_areas.json', 'rb') as fh:
        mock_response = json.load(fh)['warningDetail']
        mock_response['areas'][0]['published_warning_uuid'] = "mock_uuid_1"
        mock_response['areas'][1]['published_warning_uuid'] = "mock_uuid_2"
        mock_response['areas'][2]['published_warning_uuid'] = "mock_uuid_3"
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=mock_response)

    username = "test-user"
    with open('app/tests/testdata/warning_publish_areas.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # retrieve warning and check that it has multiple areas
    response = client.get(f"/warnings/{generated_id}",
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response['status'] == "PUBLISHED"
    assert isinstance(response['warningDetail']['areas'], list)
    assert len(response['warningDetail']['areas']) == 3
    assert "published_warning_uuid" in response['warningDetail']['areas'][0]
    assert "published_warning_uuid" in response['warningDetail']['areas'][1]
    assert "published_warning_uuid" in response['warningDetail']['areas'][2]
    # verify that correct published_warning_uuid is registered
    assert response['warningDetail']['areas'][0][
        "published_warning_uuid"] == 'mock_uuid_1'
    assert response['warningDetail']['areas'][1][
        "published_warning_uuid"] == 'mock_uuid_2'
    assert response['warningDetail']['areas'][2][
        "published_warning_uuid"] == 'mock_uuid_3'
    settings.warning_publisher_url = None


def test_update_warning_multiple_areas_with_publisher(client: TestClient,
                                                      caplog, httpx_mock):
    """Test updating a published warning with multiple areas, with
    a publisher configured"""
    # pylint: disable=too-many-statements

    # mock publisher setup
    settings.warning_publisher_url = "http://mock-publisher"

    # mock response publish endpoint
    with open('app/tests/testdata/warning_publish_areas.json', 'rb') as fh:
        mock_response = json.load(fh)['warningDetail']
        mock_response['areas'][0]['published_warning_uuid'] = "mock_uuid_1"
        mock_response['areas'][1]['published_warning_uuid'] = "mock_uuid_2"
        mock_response['areas'][2]['published_warning_uuid'] = "mock_uuid_3"
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=mock_response)

    username = "test-user"
    with open('app/tests/testdata/warning_publish_areas.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # retrieve warning and check that it has multiple areas
    response = client.get(f"/warnings/{generated_id}",
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response['status'] == "PUBLISHED"
    assert isinstance(response['warningDetail']['areas'], list)
    assert len(response['warningDetail']['areas']) == 3
    assert "published_warning_uuid" in response['warningDetail']['areas'][0]
    assert "published_warning_uuid" in response['warningDetail']['areas'][1]
    assert "published_warning_uuid" in response['warningDetail']['areas'][2]
    # verify that correct published_warning_uuid is registered
    assert response['warningDetail']['areas'][0][
        "published_warning_uuid"] == 'mock_uuid_1'
    assert response['warningDetail']['areas'][1][
        "published_warning_uuid"] == 'mock_uuid_2'
    assert response['warningDetail']['areas'][2][
        "published_warning_uuid"] == 'mock_uuid_3'

    # now update warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['id'] = generated_id
    update_warning['warningDetail']['probability'] = 60
    update_warning['warningDetail']['phenomenon'] = 'thunderstorm'
    update_warning['editor'] = username

    # mock response updating publish endpoint
    mock_update = deepcopy(update_warning)['warningDetail']
    mock_update['areas'][0]['published_warning_uuid'] = "mock_updated_uuid_1"
    mock_update['areas'][1]['published_warning_uuid'] = "mock_updated_uuid_2"
    mock_update['areas'][2]['published_warning_uuid'] = "mock_updated_uuid_3"
    httpx_mock.add_response(url="http://mock-publisher/publish",
                            method="POST",
                            status_code=200,
                            json=mock_update)

    # unlock warning for editing
    client.post(f'/warnings/{generated_id}?editor=true',
                headers={'Geoweb-Username': username})

    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Update warning in database: mark as PUBLISHED" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # check warning was successfully updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response.get('warningDetail').get('probability') == 60
    assert response.get('warningDetail').get('phenomenon') == 'thunderstorm'
    assert isinstance(response['warningDetail']['areas'], list)
    assert len(response['warningDetail']['areas']) == 3
    assert "published_warning_uuid" in response['warningDetail']['areas'][0]
    assert "published_warning_uuid" in response['warningDetail']['areas'][1]
    assert "published_warning_uuid" in response['warningDetail']['areas'][2]
    # verify that correct published_warning_uuid is registered
    assert response['warningDetail']['areas'][0][
        "published_warning_uuid"] == 'mock_updated_uuid_1'
    assert response['warningDetail']['areas'][1][
        "published_warning_uuid"] == 'mock_updated_uuid_2'
    assert response['warningDetail']['areas'][2][
        "published_warning_uuid"] == 'mock_updated_uuid_3'
    settings.warning_publisher_url = None


def test_expire_published_warning(client: TestClient) -> None:
    """Test marking an active published warning as expired.
    Check that the endtime is updated as is configured"""

    # check that warnings config is set
    assert warnings_config.expiration_delay == 60

    # get current time
    now = datetime.now(tz=timezone.utc)
    time_format = "%Y-%m-%dT%H:%M:%SZ"
    set_endtime = datetime.strftime(now + timedelta(minutes=240), time_format)

    # Publish a warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = datetime.strftime(
            now - timedelta(minutes=120), time_format)
        data['warningDetail']['validUntil'] = set_endtime
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # verify that warning is active
    warning = response.json()
    starttime = datetime.strptime(
        warning.get("warningDetail", {}).get("validFrom", None),
        time_format).replace(tzinfo=timezone.utc)
    endtime = datetime.strptime(
        warning.get("warningDetail", {}).get("validUntil", None),
        time_format).replace(tzinfo=timezone.utc)
    assert now >= starttime
    assert now < endtime

    # Mark warning as expired
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = datetime.strftime(
            now - timedelta(minutes=120), time_format)
        data['warningDetail']['validUntil'] = datetime.strftime(
            now + timedelta(minutes=120), time_format)
        data['status'] = 'EXPIRED'
        data['id'] = generated_id
        data['warningDetail']['id'] = generated_id

        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # Get warning list
    response = client.get("/warnings", headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    warnings_list = response.json()
    assert len(warnings_list) == 1
    assert warnings_list[0]['id'] == generated_id
    assert warnings_list[0]['status'] == "PUBLISHED"

    # verify endtime was changed
    assert warnings_list[0]['warningDetail']['validUntil'] != set_endtime

    # verify endtime was updated as configured
    expired_endtime = datetime.strptime(
        warnings_list[0]['warningDetail']['validUntil'],
        time_format).replace(tzinfo=timezone.utc)
    time_difference_minutes = round((expired_endtime - now).seconds / 60)
    assert time_difference_minutes == warnings_config.expiration_delay


def test_expire_proposal(client: TestClient) -> None:
    """Test that you cannot mark a PROPOSAL as expired"""

    # check that warnings config is set
    assert warnings_config.expiration_delay == 60

    # Post a PROPOSAL
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'PROPOSAL'
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # Try to mark PROPOSAL as EXPIRED
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'EXPIRED'
        data['id'] = generated_id
        data['warningDetail']['id'] = generated_id

        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert response.json()['message'] == "Status change not allowed"


def test_expire_draft(client: TestClient) -> None:
    """Test that you cannot mark a DRAFT as expired"""

    # check that warnings config is set
    assert warnings_config.expiration_delay == 60

    # Post a DRAFT
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'DRAFT'
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # Try to mark DRAFT as EXPIRED
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['status'] = 'EXPIRED'
        data['id'] = generated_id
        data['warningDetail']['id'] = generated_id

        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert response.json()['message'] == "Status change not allowed"


def test_expire_inactive_published_warning(client: TestClient) -> None:
    """Test that you cannot mark an inactive published warning as expired"""

    # check that warnings config is set
    assert warnings_config.expiration_delay == 60

    # get current time
    now = datetime.now(tz=timezone.utc)
    time_format = "%Y-%m-%dT%H:%M:%SZ"
    set_endtime = datetime.strftime(now + timedelta(minutes=240), time_format)

    # Publish a warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = datetime.strftime(
            now + timedelta(minutes=120), time_format)
        data['warningDetail']['validUntil'] = set_endtime
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # verify that warning is active
    warning = response.json()
    starttime = datetime.strptime(
        warning.get("warningDetail", {}).get("validFrom", None),
        time_format).replace(tzinfo=timezone.utc)
    endtime = datetime.strptime(
        warning.get("warningDetail", {}).get("validUntil", None),
        time_format).replace(tzinfo=timezone.utc)
    assert now < starttime
    assert now < endtime

    # Mark warning as expired
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = datetime.strftime(
            now + timedelta(minutes=120), time_format)
        data['warningDetail']['validUntil'] = datetime.strftime(
            now + timedelta(minutes=120), time_format)
        data['status'] = 'EXPIRED'
        data['id'] = generated_id
        data['warningDetail']['id'] = generated_id

        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400
    assert response.json()["message"] == 'Cannot expire warning as warning is '+\
        'not active (starttime <= now < endtime)'


def test_expire_published_warning_other_editor(client: TestClient) -> None:
    """Test marking an active published warning as expired when the published
    warning is edited by another user"""

    # get current time
    now = datetime.now(tz=timezone.utc)
    time_format = "%Y-%m-%dT%H:%M:%SZ"
    set_endtime = datetime.strftime(now + timedelta(minutes=240), time_format)

    # Publish a warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = datetime.strftime(
            now - timedelta(minutes=120), time_format)
        data['warningDetail']['validUntil'] = set_endtime
        response = client.post('/warnings',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # test that warning exists with get
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200

    # lock warning with another user
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': 'other-gwuser'})
    assert response.status_code == 200

    # verify editor was set
    response = client.get(f'/warnings/{generated_id}',
                          headers={'Geoweb-Username': 'gwuser'})
    assert response.json()["editor"] == 'other-gwuser'

    # Mark warning as expired
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['validFrom'] = datetime.strftime(
            now - timedelta(minutes=120), time_format)
        data['warningDetail']['validUntil'] = datetime.strftime(
            now + timedelta(minutes=120), time_format)
        data['status'] = 'EXPIRED'
        data['id'] = generated_id
        data['warningDetail']['id'] = generated_id

        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    # verify you get a 200 response
    assert response.status_code == 200


def test_withdraw_published_warning(client: TestClient, caplog) -> None:
    """
    Test that a published warning can be withdrawn and that the publisher
    is triggered
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # verify warning has no editor
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': "test-user"
                          }).json()
    assert response['editor'] is None

    # now mark published warning as WITHDRAWN
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        data['status'] = 'WITHDRAWN'
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # retrieve warning list and make sure warning is returned
    # with status "EXPIRED"
    response = client.get('/warnings', headers={
        'Geoweb-Username': "test-user"
    }).json()
    assert len(response) > 0
    assert response[0]['status'] == "WITHDRAWN"

    # check logs for updating logs
    assert "Update warning in database: mark as WITHDRAWN" in caplog.text
    assert "Updating warning in database" in caplog.text
    assert "Post to http://mock-publisher/publish" not in caplog.text


def test_withdraw_published_warning_other_editor(client: TestClient,
                                                 caplog) -> None:
    """
    Test that a published warning can be withdrawn if the warning is being edited
    by another user
    """

    # publish warning
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': "test-user"})

    # retrieve ID from headers
    generated_id = response.headers.get('location').split('/')[-2]

    # assign other user as editor
    response = client.post(f'/warnings/{generated_id}?editor=true',
                           headers={'Geoweb-Username': "other-user"})
    assert response.status_code == 200

    # now mark published warning as WITHDRAWN by 'test-user'
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        data = json.load(fh)
        data['warningDetail']['id'] = generated_id
        data['id'] = generated_id
        data['status'] = 'WITHDRAWN'
        response = client.post(f'/warnings/{generated_id}',
                               json=data,
                               headers={'Geoweb-Username': "test-user"})
    assert response.status_code == 200

    # retrieve warning list and make sure warning is returned
    # with status "EXPIRED"
    response = client.get('/warnings', headers={
        'Geoweb-Username': "test-user"
    }).json()
    assert len(response) > 0
    assert response[0]['status'] == "WITHDRAWN"

    # check logs for updating logs
    assert "Update warning in database: mark as WITHDRAWN" in caplog.text
    assert "Updating warning in database" in caplog.text
    assert "Post to http://mock-publisher/publish" not in caplog.text


def test_withdraw_warning_with_publisher_configured(httpx_mock,
                                                    client: TestClient,
                                                    caplog) -> None:
    '''
    Test updating of a published warning when a warning publisher has been set up
    '''

    # mock response from configured publisher
    settings.warning_publisher_url = "http://mock-publisher"
    # mock response publish
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response_data = json.load(fh)['warningDetail']
        response_data['areas'][0]['published_warning_uuid'] = 'mock_uuid'
        httpx_mock.add_response(url="http://mock-publisher/publish",
                                method="POST",
                                status_code=200,
                                json=response_data)

    # proceed with publishing
    username = "test-user"
    with open('app/tests/testdata/warning_publish.json', 'rb') as fh:
        response = client.post('/warnings',
                               json=json.load(fh),
                               headers={'Geoweb-Username': username})
    assert response.status_code == 200
    generated_id = response.headers['Location'].split('/')[-2]

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Create a published warning in database" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # verify that "published_warning_uuid" is present and equal to mock_uuid
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    test_published_warning_uuid = response.get('warningDetail').get(
        'areas')[0].get('published_warning_uuid')
    assert test_published_warning_uuid == 'mock_uuid'

    # update contents of the published warning
    update_warning = deepcopy(response)
    update_warning['warningDetail']['id'] = generated_id
    update_warning['status'] = 'WITHDRAWN'

    # mock response publish update
    response_data = deepcopy(update_warning)['warningDetail']
    httpx_mock.add_response(url="http://mock-publisher/withdraw",
                            method="POST",
                            status_code=200,
                            json=response_data)

    response = client.post(f'/warnings/{generated_id}',
                           json=update_warning,
                           headers={'Geoweb-Username': username})
    assert response.status_code == 200

    # check logs
    assert "Post to http://mock-publisher/publish" in caplog.text
    assert "Withdraw warning" in caplog.text
    assert "Update warning in database: mark as WITHDRAWN" in caplog.text
    assert "Successfully finished publishing warning" in caplog.text

    # check warning was successfully updated
    response = client.get(f'/warnings/{generated_id}',
                          headers={
                              'Geoweb-Username': username
                          }).json()
    assert response.get('status') == 'WITHDRAWN'

    # clean up environment vars
    settings.warning_publisher_url = None


def test_update_draft_with_empty_fields(client: TestClient) -> None:
    """Test saving draft warnings with empty fields and no area"""

    mock_data = {
        "warningDetail": {
            "validFrom": "2025-02-10T10:05:32Z",
            "validUntil": "2025-02-10T16:05:32Z",
            "probability": 30
        },
        "status": "DRAFT",
        "editor": "user-1"
    }

    response = client.post('/warnings',
                           json=mock_data,
                           headers={'Geoweb-Username': "user-1"})
    assert response.status_code == 200
    warning_id = response.headers['Location'].split('/')[-2]

    # add one more field, still leaving the area field empty
    mock_data['warningDetail']['id'] = warning_id
    mock_data['warningDetail']['phenomenon'] = 'fog'

    response = client.post(f"/warnings/{warning_id}",
                           json=mock_data,
                           headers={'Geoweb-Username': "user-1"})
    # make sure update is done successfully
    assert response.status_code == 200


@freeze_time("2023-12-06T10:31:14", tz_offset=0)
def test_algorithm_cleanup(client: TestClient, session, seeded_warnings,
                           caplog) -> None:
    """Test the cleanup algorithm for drafts"""

    # check warnings in database
    response = client.get('/warnings', headers={'Geoweb-Username': "user-1"})
    assert response.status_code == 200
    content = response.json()
    assert len(content) == 13
    n_drafts = len([
        warning for warning in content if warning['status'] == StatusEnum.DRAFT
    ])
    assert n_drafts == 3

    # jump forward in time
    with freeze_time("2023-12-07T11:31:19", tz_offset=0):

        # run cleanup
        cleanup_drafts(age=24, session=session)

        response = client.get('/warnings',
                              headers={'Geoweb-Username': "user-1"})
        assert response.status_code == 200
        content = response.json()
        n_drafts = len([
            warning for warning in content
            if warning['status'] == StatusEnum.DRAFT
        ])
        assert n_drafts == 0
        assert "Delete drafts lastUpdatedTime > 24 hours" in caplog.text
        assert "Collected 3 drafts to delete" in caplog.text
