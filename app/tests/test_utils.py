# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module tests functions in utils.py"""

from app.models import StatusEnum
from app.utils.utils import _status_change_allowed


#######################
### PROPOSAL STATUS ###
#######################
def test_status_proposal_to_ignored():
    """Verify status change from proposal to ignored is allowed"""
    assert _status_change_allowed(StatusEnum.PROPOSAL, StatusEnum.IGNORED)


def test_status_proposal_to_draft():
    """Verify status change from proposal to draft is allowed"""
    assert _status_change_allowed(StatusEnum.PROPOSAL, StatusEnum.DRAFT)


def test_status_proposal_to_published():
    """Verify status change from proposal to published is allowed"""
    assert _status_change_allowed(StatusEnum.PROPOSAL, StatusEnum.PUBLISHED)


def test_status_proposal_to_expired():
    """Verify status change from proposal to expired is not allowed"""
    assert not _status_change_allowed(StatusEnum.PROPOSAL, StatusEnum.EXPIRED)


def test_status_proposal_to_draft_published():
    """Verify status change from proposal to draft_published is not allowed"""
    assert not _status_change_allowed(StatusEnum.PROPOSAL,
                                      StatusEnum.DRAFT_PUBLISHED)


def test_status_proposal_to_withdrawn():
    """Verify status change from proposal to withdrawn is not allowed"""
    assert not _status_change_allowed(StatusEnum.PROPOSAL, StatusEnum.WITHDRAWN)


#######################
##### DRAFT STATUS ####
#######################
def test_status_draft_to_draft():
    """Verify status change from draft to draft is allowed"""
    assert _status_change_allowed(StatusEnum.DRAFT, StatusEnum.DRAFT)


def test_status_draft_to_published():
    """Verify status change from draft to published is allowed"""
    assert _status_change_allowed(StatusEnum.DRAFT, StatusEnum.PUBLISHED)


def test_status_draft_to_draft_published():
    """Verify status change from draft to draft_published is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT,
                                      StatusEnum.DRAFT_PUBLISHED)


def test_status_draft_to_proposal():
    """Verify status change from draft to proposal is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT, StatusEnum.PROPOSAL)


def test_status_draft_to_expired():
    """Verify status change from draft to expired is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT, StatusEnum.EXPIRED)


def test_status_draft_to_ignored():
    """Verify status change from draft to ignored is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT, StatusEnum.IGNORED)


def test_status_draft_to_withdrawn():
    """Verify status change from draft to withdrawn is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT, StatusEnum.WITHDRAWN)


#######################
## PUBLISHED STATUS ###
#######################
def test_status_published_to_draft():
    """Verify status change from published to draft is not allowed"""
    assert not _status_change_allowed(StatusEnum.PUBLISHED, StatusEnum.DRAFT)


def test_status_published_to_published():
    """Verify status change from published to published is allowed"""
    assert _status_change_allowed(StatusEnum.PUBLISHED, StatusEnum.PUBLISHED)


def test_status_published_to_draft_published():
    """Verify status change from published to draft_published is allowed"""
    assert _status_change_allowed(StatusEnum.PUBLISHED,
                                  StatusEnum.DRAFT_PUBLISHED)


def test_status_published_to_proposal():
    """Verify status change from published to proposal is not allowed"""
    assert not _status_change_allowed(StatusEnum.PUBLISHED, StatusEnum.PROPOSAL)


def test_status_published_to_expired():
    """Verify status change from published to expired is allowed"""
    assert _status_change_allowed(StatusEnum.PUBLISHED, StatusEnum.EXPIRED)


def test_status_published_to_ignored():
    """Verify status change from draft to ignored is not allowed"""
    assert not _status_change_allowed(StatusEnum.PUBLISHED, StatusEnum.IGNORED)


def test_status_published_to_withdrawn():
    """Verify status change from published to withdrawn is allowed"""
    assert _status_change_allowed(StatusEnum.PUBLISHED, StatusEnum.WITHDRAWN)


##########################
# DRAFT_PUBLISHED STATUS #
##########################
def test_status_draft_published_to_draft():
    """Verify status change from draft_published to draft is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                      StatusEnum.DRAFT)


def test_status_draft_published_to_published():
    """Verify status change from published to published is allowed"""
    assert _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                  StatusEnum.PUBLISHED)


def test_status_draft_published_to_draft_published():
    """Verify status change from draft_published to draft_published is allowed"""
    assert _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                  StatusEnum.DRAFT_PUBLISHED)


def test_status_draft_published_to_proposal():
    """Verify status change from published to proposal is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                      StatusEnum.PROPOSAL)


def test_status_draft_published_to_expired():
    """Verify status change from draft_published to expired is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                      StatusEnum.EXPIRED)


def test_status_draft_published_to_ignored():
    """Verify status change from draft_published to ignored is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                      StatusEnum.IGNORED)


def test_status_draft_published_to_withdrawn():
    """Verify status change from draft_published to withdrawn is not allowed"""
    assert not _status_change_allowed(StatusEnum.DRAFT_PUBLISHED,
                                      StatusEnum.WITHDRAWN)


#######################
#### EXPIRED STATUS ###
#######################
def test_status_expired_to_draft():
    """Verify status change from expired to draft is not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED, StatusEnum.DRAFT)


def test_status_expired_to_published():
    """Verify status change from expired to published is not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED, StatusEnum.PUBLISHED)


def test_status_expired_to_draft_published():
    """Verify status change from expired to draft_published is not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED,
                                      StatusEnum.DRAFT_PUBLISHED)


def test_status_expired_to_proposal():
    """Verify status change from expired to proposal is not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED, StatusEnum.PROPOSAL)


def test_status_expired_to_expired():
    """Verify status change from expired to expired not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED, StatusEnum.EXPIRED)


def test_status_expired_to_ignored():
    """Verify status change from expired to ignored is not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED, StatusEnum.IGNORED)


def test_status_expired_to_withdrawn():
    """Verify status change from expired to withdrawn is not allowed"""
    assert not _status_change_allowed(StatusEnum.EXPIRED, StatusEnum.WITHDRAWN)
