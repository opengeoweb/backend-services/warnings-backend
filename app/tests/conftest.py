# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# pylint: disable=redefined-outer-name,unused-argument
"""Fixtures for tests"""
import json
from collections.abc import Generator
from datetime import datetime, timedelta, timezone

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from freezegun import freeze_time
from sqlalchemy.event import listens_for
from sqlalchemy.orm import sessionmaker
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from app.config import settings
from app.db import get_session
from app.main import app as test_app
from app.models import DrawingTableRecord, WarningTableRecord
from app.utils.utils import _pydantic_json_serializer


@pytest.fixture(scope="session")
def db_engine():
    """Fixture for creating the SQLAlchemy engine"""
    engine = create_engine(settings.warnings_backend_db,
                           poolclass=StaticPool,
                           json_serializer=_pydantic_json_serializer,
                           connect_args={"options": "-c timezone=utc"})

    SQLModel.metadata.create_all(bind=engine)
    session_local = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    yield engine, session_local


@pytest.fixture(scope="function")
def session(db_engine):
    """Yields a database session fixture"""
    # start connection
    engine, session_local = db_engine
    connection = engine.connect()
    transaction = connection.begin()
    session = session_local(bind=connection)
    nested = connection.begin_nested()

    @listens_for(session, "after_transaction_end")
    def end_savepoint(_session, _transaction):
        nonlocal nested
        if not nested.is_active:
            nested = connection.begin_nested()

    yield session
    # clean up after test has finished
    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture
def app() -> Generator[FastAPI]:
    """Yields the application configured in testing mode"""
    yield test_app


@pytest.fixture
def client(app: FastAPI, session: Session) -> Generator[TestClient]:
    """Yields a test client"""

    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()


@pytest.fixture
def drawings():
    """Returns a list of drawings"""
    return [
        DrawingTableRecord(
            objectName="Drawing 10",
            scope="user",
            username="gwuser2",
            geoJSON={
                "type":
                    "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "coordinates": [[
                            [6.498868031624255, 53.24486399696136],
                            [6.498868031624255, 53.17046674568226],
                            [6.646091481479658, 53.17046674568226],
                            [6.646091481479658, 53.24486399696136],
                            [6.498868031624255, 53.24486399696136],
                        ]],
                        "type": "Polygon",
                    },
                }],
            },
        ),
        DrawingTableRecord(
            objectName="Drawing 11",
            scope="user",
            username="gwuser",
            geoJSON={
                "type":
                    "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "coordinates": [[
                            [4.704981798766198, 52.997930932407115],
                            [4.835383787121572, 53.02918963772527],
                            [4.903641077901142, 53.11365853049995],
                            [4.872059346346418, 53.179649090341655],
                            [4.818064773043517, 53.16743626602016],
                            [4.741657357991073, 53.097145044005885],
                            [4.704981798766198, 52.997930932407115],
                        ]],
                        "type": "Polygon",
                    },
                }],
            },
        ),
        DrawingTableRecord(
            objectName="Drawing 12",
            scope="user",
            username="gwuser2",
            geoJSON={
                "type":
                    "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "coordinates": [[
                            [4.2494096935421055, 51.505457438023484],
                            [4.28094639378844, 51.48075530824198],
                            [4.3231648796027, 51.48772393090391],
                            [4.295188774545238, 51.51495469320011],
                            [4.2494096935421055, 51.505457438023484],
                        ]],
                        "type": "Polygon",
                    },
                }],
            },
        ),
        DrawingTableRecord(
            objectName="Drawing 13",
            scope="user",
            username="gwuser2",
            geoJSON={
                "type":
                    "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "coordinates": [[
                            [5.954513790246665, 52.23332362289051],
                            [5.936711882090435, 52.186333158344524],
                            [6.009288892266596, 52.195147656460165],
                            [5.954513790246665, 52.23332362289051],
                        ]],
                        "type": "Polygon",
                    },
                }],
            },
        ),
    ]


@pytest.fixture
def seeded_drawings(session: Session,
                    drawings: list[DrawingTableRecord]) -> None:
    """Seeds a table with drawings"""

    filename = "./app/tests/testdata/drawings.json"
    with open(filename, "rb") as fh:
        for drawing in json.load(fh):
            session.add(DrawingTableRecord(**drawing))
        session.commit()

    # seed table
    for drawing in drawings:
        session.add(drawing)
    session.commit()


@pytest.fixture
def seeded_timed_drawings(session: Session,
                          drawings: list[DrawingTableRecord]) -> None:
    """Seeds a table with drawings, only adding for username==gwuser2
    and with timestamps 2 days apart"""

    filename = "./app/tests/testdata/drawings.json"
    faketime = datetime.now(tz=timezone.utc)
    delta = timedelta(days=2)
    with open(filename, "rb") as fh:
        for drawing in json.load(fh):
            if drawing["username"] == "gwuser2" or drawing["scope"] == "global":
                with freeze_time(faketime):
                    session.add(DrawingTableRecord(**drawing))
                    session.commit()
                faketime = faketime - delta
        # session.commit()


@pytest.fixture
def seeded_warnings(session: Session) -> None:
    """Seeds a table with warnings with fake fixed time"""

    filename = "./app/tests/testdata/warnings.json"

    faketime = datetime.strptime("2023-12-06T10:30:14Z", "%Y-%m-%dT%H:%M:%SZ")
    delta = timedelta(seconds=1)
    with open(filename, "rb") as fh:
        for warning in json.load(fh):
            with freeze_time(faketime):
                session.add(WarningTableRecord(**warning))
                faketime += delta
        session.commit()


@pytest.fixture
def seeded_warnings_with_ignored(session: Session) -> None:
    """Seeds a table with warnings with fake fixed time
    warnings 3 and 5 are set to IGNORED
    """

    filename = "./app/tests/testdata/warnings.json"

    faketime = datetime.strptime("2023-12-06T10:30:14Z", "%Y-%m-%dT%H:%M:%SZ")
    delta = timedelta(seconds=1)
    with open(filename, "rb") as fh:
        cnt = 0
        for warning in json.load(fh):
            if cnt in {3, 5}:
                if warning["status"] == "PROPOSAL":
                    warning["status"] = "IGNORED"
            with freeze_time(faketime):
                session.add(WarningTableRecord(**warning))
                faketime += delta
            cnt = cnt + 1
        session.commit()
