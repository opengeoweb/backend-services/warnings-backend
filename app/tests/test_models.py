# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"Models mapping to the database"

from datetime import datetime, timezone

from awaredatetime import AwareDatetime

from app.models import (DomainEnum, DrawingTableRecord, ScopeEnum,
                        WarningTableRecord)

empty_geojson = {"type": "FeatureCollection", "features": []}


def test_init_drawing() -> None:
    """Tests that derived attributes are set."""
    drawing = DrawingTableRecord(objectName="Area 51",
                                 scope=ScopeEnum.USER,
                                 username="E.T.",
                                 geoJSON=empty_geojson,
                                 keywords="test")

    assert drawing.objectName == "Area 51"
    assert drawing.scope == "user"
    assert drawing.username == "E.T."
    assert drawing.geoJSON == empty_geojson
    assert drawing.keywords == "test"

    assert isinstance(drawing.id, str)
    assert isinstance(drawing.lastUpdatedTime, datetime)


def test_init_drawing_no_name() -> None:
    """Tests that derived attributes are set."""

    drawing = DrawingTableRecord(scope=ScopeEnum.USER,
                                 username="E.T.",
                                 geoJSON=empty_geojson,
                                 keywords="test")

    assert isinstance(drawing.objectName, str)
    assert drawing.scope == "user"
    assert drawing.username == "E.T."
    assert drawing.geoJSON == empty_geojson
    assert drawing.keywords == "test"
    assert isinstance(drawing.id, str)
    assert isinstance(drawing.lastUpdatedTime, datetime)


def test_init_drawing_no_keywords() -> None:
    """Tests that derived attributes are set."""

    drawing = DrawingTableRecord(scope=ScopeEnum.USER,
                                 username="E.T.",
                                 geoJSON=empty_geojson)

    assert isinstance(drawing.objectName, str)
    assert drawing.scope == "user"
    assert drawing.username == "E.T."
    assert drawing.geoJSON == empty_geojson
    assert drawing.keywords == "Objects"
    assert isinstance(drawing.id, str)
    assert isinstance(drawing.lastUpdatedTime, datetime)


def test_init_drawing_global() -> None:
    """Test that global scope is valid."""

    drawing = DrawingTableRecord(scope=ScopeEnum.GLOBAL,
                                 username="E.T.",
                                 geoJSON=empty_geojson)

    assert isinstance(drawing.objectName, str)
    assert drawing.scope == "global"
    assert drawing.username == "E.T."
    assert drawing.geoJSON == empty_geojson
    assert isinstance(drawing.id, str)
    assert isinstance(drawing.lastUpdatedTime, datetime)


def test_init_warning() -> None:
    """Tests that derived attributes are set"""
    warning = WarningTableRecord(status="PUBLISHED",
                                 warningDetail={
                                     "id":
                                         "ewc_uuid",
                                     "areas": [{
                                         "geoJSON": empty_geojson,
                                         "objectName": "Empty warning"
                                     }],
                                     "validFrom":
                                         AwareDatetime(2023,
                                                       11,
                                                       30,
                                                       14,
                                                       4,
                                                       23,
                                                       tzinfo=timezone.utc),
                                     "validUntil":
                                         AwareDatetime(2023,
                                                       12,
                                                       1,
                                                       9,
                                                       55,
                                                       1,
                                                       tzinfo=timezone.utc)
                                 })

    assert isinstance(warning.id, str)
    assert isinstance(warning.lastUpdatedTime, datetime)
    assert warning.warningDetail == {
        "id":
            "ewc_uuid",
        "areas": [{
            "geoJSON": empty_geojson,
            "objectName": "Empty warning"
        }],
        "validFrom":
            AwareDatetime(2023, 11, 30, 14, 4, 23, tzinfo=timezone.utc),
        "validUntil":
            AwareDatetime(2023, 12, 1, 9, 55, 1, tzinfo=timezone.utc),
        "domain":
            DomainEnum.PUBLIC
    }
    assert warning.status == "PUBLISHED"


def test_init_warning_ewc() -> None:
    """Tests that derived attributes are set"""
    warning = WarningTableRecord(status="PROPOSAL",
                                 warningDetail={
                                     "id":
                                         "ewc_uuid",
                                     "areas": [{
                                         "geoJSON": empty_geojson,
                                         "objectName": "EWC Object"
                                     }]
                                 })

    assert isinstance(warning.id, str)
    assert warning.id == "ewc_uuid"
    assert isinstance(warning.lastUpdatedTime, datetime)
    assert warning.warningDetail == {
        "id": "ewc_uuid",
        "areas": [{
            "geoJSON": empty_geojson,
            "objectName": "EWC Object"
        }],
        "domain": DomainEnum.PUBLIC
    }
    assert warning.status == "PROPOSAL"
