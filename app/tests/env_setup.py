# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Variables for test setup database"""
import os

from pydantic_settings import BaseSettings


## Set up everything to start database
class TestSettings(BaseSettings):
    """Test settings class"""
    docker_host: str = os.getenv('DOCKER_HOST') if os.getenv(
        'GITLAB_CI') else 'localhost'
    tst_db_port: str = "5433"
    tst_db_name: str = "postgres"
    tst_db_url: str = f"postgresql://geoweb:geoweb@{docker_host}:{tst_db_port}/{tst_db_name}"


test_settings = TestSettings()
