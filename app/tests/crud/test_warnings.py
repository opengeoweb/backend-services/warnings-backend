# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for warnings CRUD actions"""

import json
import warnings
from datetime import UTC, datetime, timedelta

import pytest
from pydantic import ValidationError
from sqlalchemy.exc import SAWarning
from sqlmodel import Session

from app.crud.errors import CreateError
from app.crud.warnings import CRUD
from app.models import (DomainEnum, StatusEnum, WarningInterface,
                        WarningTableRecord, warningDetailStrict)

empty_warningDetail = {
    "id": "test_id",
    "areas": [{
        "geoJSON": {
            "type": "FeatureCollection",
            "features": []
        },
        "objectName": "Empty object"
    }],
    "domain": DomainEnum.PUBLIC
}


def test_create(session: Session) -> None:
    """Tests create"""

    data: WarningTableRecord = WarningTableRecord(
        status="PUBLISHED", warningDetail=empty_warningDetail)

    crud = CRUD(session)
    warning: WarningTableRecord = crud.create(data)

    assert warning is not None
    assert warning.id is not None
    assert warning.status == "PUBLISHED"
    assert warning.warningDetail == empty_warningDetail


def test_create_duplicate(session: Session) -> None:
    """Tests create"""

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=SAWarning)

        data: WarningTableRecord = WarningTableRecord(
            status="PUBLISHED", warningDetail=empty_warningDetail)

        crud = CRUD(session)
        warning: WarningTableRecord = crud.create(data)
        session.flush()

        assert warning is not None
        assert warning.id == "test_id"
        assert warning.status == "PUBLISHED"
        assert warning.warningDetail == empty_warningDetail

        ## add data with same id
        data2: WarningTableRecord = WarningTableRecord(
            status="PUBLISHED",
            warningDetail={
                "id":
                    "test_id",
                "level":
                    "moderate",
                "areas": [{
                    "geoJSON": {
                        "type": "FeatureCollection",
                        "features": []
                    },
                    "objectName": "Emtpy object"
                }]
            },
        )
        with pytest.raises(CreateError):
            crud.create(data2)


def test_read_one(session: Session) -> None:
    """Tests read one"""

    data: WarningTableRecord = WarningTableRecord(
        status="PUBLISHED", warningDetail=empty_warningDetail)
    test_id = str(data.id)

    # add to table
    crud = CRUD(session)
    warning: WarningTableRecord = crud.create(data)

    # retrieve warning
    read_warning = crud.read_one(test_id)
    assert read_warning is not None
    assert warning.id == test_id
    assert warning.status == "PUBLISHED"
    assert warning.warningDetail == empty_warningDetail


def test_invalid_warningdetail() -> None:
    """Tests exceptions when WarningDetail contains invalid geoJSON"""
    # Read GeoJSON data from file
    with open("app/tests/testdata/geojson_unclosed.json", "rb") as fh:
        geojson_data = json.load(fh)
        try:
            warningDetailStrict(areas=[{
                "geoJSON": geojson_data,
                "objectName": "Test object"
            }])
        except ValidationError as err:
            assert "All linear rings have the same start and end coordinates" in str(
                err)
        else:
            assert False


def test_invalid_warninginterface() -> None:
    """Tests exceptions when WarningInterface contains invalid geoJSON"""
    # Read GeoJSON data from file
    with open("app/tests/testdata/geojson_unclosed.json", "rb") as fh:
        geojson_data = json.load(fh)
        try:
            WarningInterface(
                status=StatusEnum.PUBLISHED,
                warningDetail=warningDetailStrict(areas=[{
                    "geoJSON": geojson_data,
                    "objectName": "Test object"
                }]),
            )
        except ValidationError as err:
            assert "All linear rings have the same start and end coordinates" in str(
                err)
        else:
            assert False


def test_invalid_warningtablerecord() -> None:
    """Tests exceptions when WarningTableRecord contains invalid warningDetail"""
    # Read GeoJSON data from file
    with open("app/tests/testdata/warningjson_unclosed.json", "rb") as fh:
        warningdetail_data = json.load(fh)
        try:
            WarningTableRecord(**warningdetail_data)
        except ValidationError as err:
            assert "All linear rings have the same start and end coordinates" in str(
                err)
        else:
            assert False


def test_read_many(session: Session) -> None:
    """Tests read many"""

    data: WarningTableRecord = WarningTableRecord(
        status="DRAFT", warningDetail=empty_warningDetail)
    test_id = data.id

    # add to table
    crud = CRUD(session)
    crud.create(data)

    # retrieve warning
    list_warning = crud.read_many()
    assert list_warning is not None
    assert isinstance(list_warning, list)
    assert list_warning[0].id == test_id
    assert list_warning[0].status == "DRAFT"
    assert list_warning[0].warningDetail == empty_warningDetail


def test_read_many_no_ignored(session: Session) -> None:
    """Tests read many skipping ignored warnings"""

    def get_warning(warning_id, status):
        """Generate a skeleton warning"""
        warning_detail = empty_warningDetail

        # Generate a time in the future for validUntil
        until_time = datetime.strftime(
            datetime.now(UTC) + timedelta(hours=1), "%Y-%m-%dT%H:%M:%SZ")
        warning_detail["validUntil"] = until_time
        warning: WarningTableRecord = WarningTableRecord(
            status=status, warningDetail=warning_detail)
        warning.id = warning_id
        return warning

    draft_id = "draft_1"
    draft = get_warning(draft_id, "DRAFT")

    proposal_id = "proposal_1"
    proposal = get_warning(proposal_id, "PROPOSAL")

    ignored_id = "ignored_1"
    ignored = get_warning(ignored_id, "IGNORED")

    # add to table

    crud = CRUD(session)
    crud.create(draft)
    crud.create(proposal)
    crud.create(ignored)

    # retrieve warning
    # this should return a PROPOSAL and a DRAFT, but not the IGNORED warning
    list_warning = crud.read_many()
    assert list_warning is not None
    assert isinstance(list_warning, list)
    assert len(list_warning) == 2

    assert list_warning[0].id == proposal_id
    assert list_warning[0].status == "TODO"  # PROPOSAL -> TODO by read_many()
    assert list_warning[0].warningDetail == proposal.warningDetail

    assert list_warning[1].id == draft_id
    assert list_warning[1].status == "DRAFT"
    assert list_warning[1].warningDetail == draft.warningDetail
