# Copyright 2022 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for drawings CRUD actions"""

import json

import pytest
from freezegun import freeze_time
from pydantic import ValidationError
from sqlmodel import Session

from app.crud.drawings import CRUD
from app.crud.errors import CreateError, UpdateError
from app.models import DrawingInterface, DrawingTableRecord, ScopeEnum

empty_geojson = {"type": "FeatureCollection", "features": []}


def test_create(session: Session) -> None:
    """Tests create"""

    data: DrawingTableRecord = DrawingTableRecord(objectName="Area 51",
                                                  scope=ScopeEnum.USER,
                                                  username="E.T.",
                                                  geoJSON=empty_geojson)

    crud = CRUD(session)
    drawing: DrawingTableRecord = crud.create(data, "E.T.")

    assert drawing is not None
    assert drawing.id is not None
    assert drawing.username == 'E.T.'

    # insert view-preset with same title and username
    with pytest.raises(
            CreateError,
            match="Object name already exists, please choose a different name"):
        crud.create(data, 'E.T.')


def test_update(session: Session) -> None:
    """Tests create"""

    # first create a drawing
    data: DrawingTableRecord = DrawingTableRecord(objectName="Area 51",
                                                  scope=ScopeEnum.USER,
                                                  username="E.T.",
                                                  geoJSON=empty_geojson)

    crud = CRUD(session)
    drawing: DrawingTableRecord = crud.create(data, "E.T.")

    assert drawing is not None
    assert drawing.id is not None
    assert drawing.username == 'E.T.'

    # now try to update the title of the drawing
    update: DrawingInterface = DrawingInterface(objectName="Area 52",
                                                scope=ScopeEnum.USER,
                                                geoJSON=empty_geojson)
    crud.update(drawing, update, "E.T.")

    # check if update action was successfull
    updated_drawing: DrawingTableRecord = crud.read_one(drawing.id,
                                                        "E.T.")  # type: ignore
    assert updated_drawing.objectName == "Area 52"
    assert updated_drawing.geoJSON == empty_geojson


def test_update_error(session: Session) -> None:
    """Tests create UpdateError"""

    # create two drawings a drawing
    data: DrawingTableRecord = DrawingTableRecord(objectName="Area 51",
                                                  scope=ScopeEnum.USER,
                                                  username="E.T.",
                                                  geoJSON=empty_geojson)

    crud = CRUD(session)
    drawing1: DrawingTableRecord = crud.create(data, "E.T.")

    assert drawing1 is not None
    assert drawing1.id is not None
    assert drawing1.username == 'E.T.'

    data2: DrawingTableRecord = DrawingTableRecord(objectName="Another Area",
                                                   scope=ScopeEnum.USER,
                                                   username="E.T.",
                                                   geoJSON=empty_geojson)
    drawing2: DrawingTableRecord = crud.create(data2, "E.T.")
    assert drawing2 is not None

    # now update the title of the drawing1
    update: DrawingInterface = DrawingInterface(objectName="Another Area",
                                                scope=ScopeEnum.USER,
                                                geoJSON=empty_geojson)

    with pytest.raises(
            UpdateError,
            match="Object name already exists, please choose a different name"):
        crud.update(drawing1, update, "E.T.")


def test_read(session: Session) -> None:
    """Tests read with and without age parameter"""
    # create two drawings a
    crud = CRUD(session)

    with freeze_time("2023-09-16 12:00:00", tz_offset=0):
        data: DrawingTableRecord = DrawingTableRecord(objectName="Area 51",
                                                      scope=ScopeEnum.USER,
                                                      username="E.T.",
                                                      geoJSON=empty_geojson)
        drawing: DrawingTableRecord = crud.create(data, "E.T.")
        assert drawing is not None

    with freeze_time("2023-09-26 12:00:00", tz_offset=0):
        data2: DrawingTableRecord = DrawingTableRecord(objectName="Area 52",
                                                       scope=ScopeEnum.USER,
                                                       username="E.T.",
                                                       geoJSON=empty_geojson)
        drawing2: DrawingTableRecord = crud.create(data2, "E.T.")
        assert drawing2 is not None

    drawings = crud.read_many(username='E.T.')
    assert drawings is not None
    assert len(drawings) == 2

    with freeze_time("2023-09-26 12:00:00", tz_offset=0):
        drawings = crud.read_many(username='E.T.', age=20 * 24 * 3600)
        assert drawings is not None
        assert len(drawings) == 2

        drawings = crud.read_many(username='E.T.', age=5 * 24 * 3600)
        assert drawings is not None
        assert len(drawings) == 1

        drawings = crud.read_many(username='E.T.')
        assert drawings is not None
        assert len(drawings) == 2


def test_invalid_drawing() -> None:
    """Tests exceptions when Drawing contains invalid geoJSON"""
    # Read GeoJSON data from file
    with open('app/tests/testdata/geojson_unclosed.json', 'rb') as fh:
        geojson_data = json.load(fh)
        try:
            DrawingTableRecord(username="gwuser",
                               scope=ScopeEnum.USER,
                               geoJSON=geojson_data)
        except ValidationError as err:
            assert "All linear rings have the same start and end coordinates" in str(
                err)
        else:
            assert False
