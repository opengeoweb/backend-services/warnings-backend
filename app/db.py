# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module handles the interaction with the database"""

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from app.config import settings
from app.utils.utils import _pydantic_json_serializer

engine = create_engine(settings.warnings_backend_db,
                       pool_recycle=120,
                       pool_pre_ping=True,
                       json_serializer=_pydantic_json_serializer)


def get_session():
    """Yields a database session"""
    with Session(engine) as session:
        yield session
