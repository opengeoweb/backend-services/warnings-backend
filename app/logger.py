# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module for application-wide logging configuration"""
import logging
import logging.config
from typing import Any

from pythonjsonlogger import json

from app.config import settings


class JsonFormatter(json.JsonFormatter):
    "JSON formatter with additional logging fields"

    def add_fields(self, log_record: dict[str, Any], record: logging.LogRecord,
                   message_dict: dict[str, Any]) -> None:
        "Adds some extra fields to each log entry"
        super().add_fields(log_record, record, message_dict)
        log_record['log_level'] = record.levelname


def configure_logger() -> None:
    "Configures the application loggers"

    # Use INFO if level not valid
    log_level = logging.getLevelName(settings.log_level)

    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'json': {
                'class': 'app.logger.JsonFormatter'
            }
        },
        'handlers': {
            'stream': {
                'class': 'logging.StreamHandler',
                'formatter': 'json',
                'stream': 'ext://sys.stderr',
            }
        },
        'root': {
            'handlers': ['stream'],
            'level': log_level,
        },
        'loggers': {
            'uvicorn.error': {
                'level': log_level,
            },
            'uvicorn.access': {
                'handlers': [],
                'propagate': False
            },
            'sqlalchemy': {
                'level': "ERROR",
            },
            'uvicorn.asgi': {
                'level': log_level,
            }
        },
    })
