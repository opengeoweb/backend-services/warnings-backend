# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module for application-wide settings"""
import json
from abc import ABC
from pathlib import Path
from typing import Any

from pydantic.fields import FieldInfo
from pydantic_settings import (BaseSettings, PydanticBaseSettingsSource,
                               SettingsConfigDict)


class Settings(BaseSettings):
    """BaseSettings class"""
    version: str = "0.0.0"
    warnings_backend_db: str = 'postgresql://geoweb:geoweb@localhost:5432/warningsbackenddb'
    log_level: str = "INFO"
    default_timeout: str = "15"
    warning_publisher_url: str | None = None
    model_config = SettingsConfigDict(env_file='.env',
                                      env_file_encoding='utf-8',
                                      extra='allow')
    warnings_config_file: str = 'configuration_files/warningsConfig.json'
    application_root_path: str = ""
    application_doc_root: str = "/api"
    run_draft_cleanup: bool = False
    draft_cleanup_hours: int = 24


class JsonConfigSettingsSource(PydanticBaseSettingsSource):
    """
    JSON file settings source
    """

    def __init__(self, settings_cls: type[BaseSettings], json_file_path: str):
        super().__init__(settings_cls)
        self.json_file_path = json_file_path
        self.file_content_json = json.loads(
            Path(self.json_file_path).read_text('utf-8'))

    def __call__(self) -> dict[str, Any]:
        settings_dict: dict[str, Any] = {
            field_name: self.file_content_json.get(field_name)
            for field_name in self.settings_cls.model_fields.keys()
            if self.file_content_json.get(field_name) is not None
        }
        return settings_dict

    def get_field_value(self, _field: FieldInfo, field_name: str) -> Any:
        return self.file_content_json.get(field_name)


class JsonConfigBaseSettings(BaseSettings, ABC):
    """General class to load configuration from JSON file"""

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            init_settings,
            JsonConfigSettingsSource(settings_cls,
                                     settings.warnings_config_file),
            env_settings,
            dotenv_settings,
            file_secret_settings,
        )


class WarningsConfig(JsonConfigBaseSettings):
    """Basemodel for the warnings configuration model"""

    model_config = SettingsConfigDict(title="Warnings Config", extra='allow')
    expiration_delay: int = 60


settings = Settings()
warnings_config = WarningsConfig()
