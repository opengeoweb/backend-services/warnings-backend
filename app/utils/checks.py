# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Healtcheck utilities"""

import logging

import httpx
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import Session

from app.config import settings
from app.crud.drawings import CRUD as DrawingCRUD
from app.crud.warnings import CRUD as WarningsCRUD
from app.utils.client import AsyncHttpxClientWrapper


async def check_service(message: dict, url: str, service_name: str) -> bool:
    """Checks if the service is available by sending GET requests to specified url

    The service health is determined by http status code.

    Args:
        message (dict): status-message to put the availability information
        url (str): healthcheck url to send the request to
        service_name (str): service name to be used in the status message

    Returns:
        bool: If service is healthy
    """
    healthy: bool
    client = await AsyncHttpxClientWrapper.get_httpx_client()
    try:
        response = await client.get(url)
        if response.status_code == httpx.codes.OK:
            message[service_name] = {"status": "OK"}
            healthy = True
        else:
            message[service_name] = {"status": "FAILED"}
            message["status"] = "FAILED"
            healthy = False
    except httpx.HTTPError as err:
        logging.error(err)
        message[service_name] = {"status": "FAILED"}
        message["status"] = "FAILED"
        healthy = False
    return healthy


def database(session: Session, message: dict) -> bool:
    """Checks the database connection

    Args:
        session: database session
        message: status-message to update the database status

    Returns:
        bool: True if database check was successful
    """
    # pylint: disable=consider-using-assignment-expr
    healthy: bool = False
    try:
        tables: dict[str, type[DrawingCRUD | WarningsCRUD]] = {
            "warnings": WarningsCRUD,
            "drawings": DrawingCRUD
        }
        for table, crud_class in tables.items():
            crud = crud_class(session)
            row_count = crud.count_rows()
            if row_count >= 0:
                message["database"][table] = {"status": "OK"}
                message["database"].update({"status": "OK"})
                healthy = True
            else:
                message["database"][table] = {"status": "FAILED"}
                message["status"] = "FAILED"
                message["database"].update({"status": "FAILED"})
                healthy = False

    except OperationalError as err:
        logging.error(err)
        message["database"].update({"status": "FAILED"})
        message["status"] = "FAILED"
        healthy = False
    return healthy


async def publisher(message) -> bool:
    """Check optional publisher service"""
    if publisher_url := settings.warning_publisher_url:
        return await check_service(message, publisher_url, "publisher")
    return True
