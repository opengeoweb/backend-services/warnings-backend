# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Scheduler to clean up drafts every hour"""

import logging

from sqlalchemy.orm import Session

from app.crud.errors import DeleteError
from app.crud.warnings import CRUD
from app.db import engine
from app.models import WarningTableRecord


def cleanup_drafts(age: int = 24, session: Session | None = None) -> None:
    """Cleanup drafts with lastUpdatedTime > 24 hours"""

    logging.info("Delete drafts lastUpdatedTime > %s hours", age)

    if not session:
        session = Session(engine)

    with session:

        drafts_to_delete: list[WarningTableRecord] = CRUD(
            session).collect_drafts_for_cleanup(age)

        logging.info("Collected %s drafts to delete", len(drafts_to_delete))

        for draft in drafts_to_delete:
            try:
                CRUD(session).delete(draft, "cleanup-user")
            except DeleteError as err:
                logging.error("Failed cleaning up draft warning: %s",
                              str(err),
                              extra={"warning_id": draft.id})

    logging.info("Finished draft cleanup")
