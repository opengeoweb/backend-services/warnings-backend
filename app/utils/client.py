# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module provides an async httpx client for the app"""

import httpx

from app.config import settings


class AsyncHttpxClientWrapper:
    """Async HTTPX client"""
    httpx_client: httpx.AsyncClient | None = None

    @classmethod
    async def get_httpx_client(cls) -> httpx.AsyncClient:
        """Get HTTPX async client"""
        if cls.httpx_client is None:
            cls.httpx_client = httpx.AsyncClient(timeout=httpx.Timeout(
                timeout=float(settings.default_timeout)))
        return cls.httpx_client

    @classmethod
    async def close_httpx_client(cls) -> None:
        """Close HTTPX async client"""
        if cls.httpx_client:
            await cls.httpx_client.aclose()
            cls.httpx_client = None
