# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module provide utils for the app"""

import json
import logging
from datetime import datetime, timedelta, timezone

from fastapi import HTTPException, Response
from fastapi.encoders import jsonable_encoder
from pydantic import ValidationError

from app.models import (AreaItem, FeatureCollection, StatusEnum,
                        WarningInterface)


def _check_authenticated(response: Response,
                         username: str | None = None) -> None:
    """Checks if user is authenticated"""
    if not username:
        raise HTTPException(status_code=401, detail="Not authenticated")
    response.headers["username"] = username


def _status_change_allowed(old_status: StatusEnum, new_status: StatusEnum):
    """Checks if warning status change is allowed"""
    # Allow PROPOSAL -> IGNORED
    # or PUBLISHED -> EXPIRED/WITHDRAWN
    # or PUBLISHED/DRAFT_PUBLISHED -> PUBLISHED/DRAFT_PUBLISHED
    # pylint: disable=too-many-boolean-expressions
    if (old_status == StatusEnum.PROPOSAL and new_status == StatusEnum.IGNORED
       ) or (old_status == StatusEnum.PUBLISHED and new_status in [
           StatusEnum.EXPIRED, StatusEnum.WITHDRAWN
       ]) or (old_status in [StatusEnum.PUBLISHED, StatusEnum.DRAFT_PUBLISHED
                            ] and
              new_status in [StatusEnum.PUBLISHED, StatusEnum.DRAFT_PUBLISHED]):
        return True
    return old_status in [
        StatusEnum.DRAFT,
        StatusEnum.PROPOSAL,
        StatusEnum.TODO,
    ] and new_status in [StatusEnum.DRAFT, StatusEnum.PUBLISHED]


def _check_editor(assigned_editor: str | None, username: str):
    """Checks if warning editor and username match"""
    if not assigned_editor == username:
        msg = "Warning is being edited by another user and cannot be updated"
        logging.info("%s", msg)
        raise HTTPException(status_code=400, detail=msg)


def _check_editor_or_none(assigned_editor: str | None, username: str):
    """Checks if warning editor and username match OR editor is None"""
    if assigned_editor is not None:
        _check_editor(assigned_editor, username)


def _pydantic_json_serializer(*args, **kwargs) -> str:
    """Encodes json in the same way as pydantic."""
    return json.dumps(jsonable_encoder(*args, **kwargs))


def _is_warning_active(warning: WarningInterface):
    """Verifies that warning is 'active', so that
    - current time > start time AND
    - current time < end time
    """
    now = datetime.now(tz=timezone.utc)
    return (warning.warningDetail.validFrom and
            warning.warningDetail.validUntil and warning.warningDetail.validFrom
            <= now < warning.warningDetail.validUntil)


def calc_expire_endtime(expiration_delay: int):
    """Calculates the new endtime which is the current time
    increased with number of minutes configured in
    warnings_config.expiration_delay
    """
    return (datetime.now(tz=timezone.utc) +
            timedelta(minutes=expiration_delay)).replace(microsecond=0)


def log_validation_errors(warning_id: str | None, err: ValidationError) -> str:
    """Log the validation errors"""
    if err.errors():
        msg = ""
        for _err in err.errors():
            err_loc = _err.get("loc", ("unknown location"))
            err_msg = _err.get("msg",
                               "unknown error").replace("Value error,", "")
            msg += f"Invalid entry for {err_loc[0]}: {err_msg}; \n"
    logging.error(msg, extra={"info": str(err), "warning_id": warning_id})
    return msg if msg else "No validation error information found."


def _check_published_warning_uuid(
        published_warning_uuid: str,
        incoming_warning: WarningInterface) -> WarningInterface:
    """Make sure the published_warning_uuid is set correct.

    If the existing has an "areas" object AND if the "areas" object
    has a "published_warning_uuid" ==> the warning has been published
    to a downstream publisher ==> make sure that the
    "published_warning_uuid" value matches with that of the incoming
    payload to avoid conflicts with the downstream publisher

    Note: assuming one area in the areas object for now so get the
    first item in the 'areas' list
    """

    # If incoming warning has a published_warning_uuid, compare with
    # the published_warning_uuid in the database
    if incoming_warning.warningDetail.areas and (
            incoming_published_uuid :=
            incoming_warning.warningDetail.areas[0].published_warning_uuid):

        # If different UUIDs: raise an error
        if incoming_published_uuid != published_warning_uuid:
            msg = "Incoming published_warning_uuid different from DB."
            logging.error(
                msg,
                extra={
                    'DB published_warning_uuid': published_warning_uuid,
                    'incoming published_warning_uuid': incoming_published_uuid,
                    'warning_id': incoming_warning.id
                })
            raise HTTPException(status_code=400, detail=msg)
        # Otherwise no need to change anything
        logging.info("Incoming published_warning_uuid equal to DB")

    # If missing in incoming data, add it to the warning
    else:
        logging.warning("No incoming UUID found - set UUID from DB",
                        extra={
                            "published_warning_uuid": published_warning_uuid,
                            'warning_id': incoming_warning.id
                        })
        # Add value to areas object if it exists
        if incoming_warning.warningDetail.areas:
            incoming_warning.warningDetail.areas[
                0].published_warning_uuid = published_warning_uuid
        # If it does not exist & status is draft, add an empty object
        # Add empty geoJSON and objectName as this is required by the
        # AreaItem model
        elif (incoming_warning.status in [
                StatusEnum.DRAFT, StatusEnum.DRAFT_PUBLISHED
        ]) and (not incoming_warning.warningDetail.areas):
            logging.warning(
                "Areas missing: generate empty areaItem for DRAFT warning")
            incoming_warning.warningDetail.areas = [
                AreaItem(published_warning_uuid=published_warning_uuid,
                         geoJSON=FeatureCollection(type="FeatureCollection",
                                                   features=[]),
                         objectName="-")
            ]
        else:
            msg = "Areas missing: Could not set published_warning_uuid correctly"
            logging.warning(
                msg,
                extra={
                    "published_warning_uuid": published_warning_uuid,
                    "warning_id": incoming_warning.id
                })
            raise HTTPException(status_code=400, detail=msg)
    return incoming_warning
