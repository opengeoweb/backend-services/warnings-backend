# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module provide publishing utils for the app"""
import json
import logging

import httpx
from fastapi import HTTPException
from sqlmodel import Session

from app.config import settings
from app.crud.errors import CreateError, UpdateError
from app.crud.warnings import CRUD
from app.models import StatusEnum, WarningTableRecord, warningDetailStrict
from app.utils.client import AsyncHttpxClientWrapper


class PublishError(BaseException):
    """Represents a publishing error"""


class WarningPublisher:
    """Warnings publisher"""

    def __init__(self):
        """Init"""
        self.warning_url = settings.warning_publisher_url

    def save_warning(
        self,
        session: Session,
        username: str,
        warning: WarningTableRecord,
        existing_warning: WarningTableRecord | None = None
    ) -> WarningTableRecord:
        """Save or update a warning in the database"""
        try:
            if not existing_warning:
                logging.info("Create a published warning in database")
                published_warning = CRUD(session).create(warning)
            else:
                logging.info("Update warning in database: mark as %s",
                             warning.status)
                CRUD(session).update(existing_warning, warning, username)
                published_warning = existing_warning
        except (CreateError, UpdateError) as err:
            err_str = "Publishing warning failed: failed updating database"
            logging.error(err_str,
                          extra={
                              'username': username,
                              'err': str(err)
                          })
            raise HTTPException(status_code=400, detail=err_str) from err
        logging.info("Successfully marked warning as %s warning in database",
                     warning.status)
        return published_warning

    async def publish_warning(self, warning: warningDetailStrict,
                              username: str) -> warningDetailStrict:
        """Publish a warning to the configured publisher"""
        # pylint: disable=invalid-name
        logging.info("Publish warning", extra={"username": username})

        # create payload with metadata
        payload = {
            "warning": json.loads(warning.model_dump_json(exclude_none=True)),
            "metadata": {
                "author": username
            }
        }
        logging.info("Payload: %s", payload)
        client = await AsyncHttpxClientWrapper.get_httpx_client()
        try:
            logging.info("Post to %s/publish", self.warning_url)
            r = await client.post(f"{self.warning_url}/publish", json=payload)
        except httpx.HTTPError as exc:
            logging.error("Publishing failed", extra={"exc": str(exc)})
            raise PublishError(
                "Exception occured while trying to publish warning") from exc
        if not r.status_code == httpx.codes.OK:
            logging.error("Non-20x status code returned",
                          extra={
                              "status code": str(r.status_code),
                              "detail": r.json()['detail']
                          })
            raise PublishError(f"{r.json()['detail']}")
        logging.info("Published successfully")
        return warningDetailStrict(**r.json())

    async def withdraw_warning(self, warning: warningDetailStrict,
                               username: str) -> warningDetailStrict:
        """Withdraw a warning to the configured publisher"""
        # pylint: disable=invalid-name
        logging.info("Withdraw warning", extra={"username": username})

        # create payload with metadata
        payload = {
            "warning": json.loads(warning.model_dump_json(exclude_none=True)),
            "metadata": {
                "author": username
            }
        }
        logging.info("Payload: %s", payload)
        client = await AsyncHttpxClientWrapper.get_httpx_client()
        try:
            logging.info("Post to %s/withdraw", self.warning_url)
            r = await client.post(f"{self.warning_url}/withdraw", json=payload)
        except httpx.HTTPError as exc:
            logging.error("Withdrawing failed", extra={"exc": str(exc)})
            raise PublishError(
                "Exception occured while trying to withdraw warning") from exc
        if not r.status_code == httpx.codes.OK:
            logging.error("Non-20x status code returned",
                          extra={
                              "status code": str(r.status_code),
                              "detail": r.json()['detail']
                          })
            raise PublishError(f"{r.json()['detail']}")
        logging.info("Withdrawn successfully")
        return warningDetailStrict(**r.json())

    async def publisher(
        self,
        warning: WarningTableRecord,
        username: str,
        session: Session,
        existing_warning: WarningTableRecord | None = None
    ) -> WarningTableRecord:
        """Main publisher function"""

        logging.info("Start publishing warning", extra={"username": username})

        ##########################################
        ## if a warning publisher is configured ##
        ##########################################
        if self.warning_url:
            logging.debug("Configured publisher: %s", self.warning_url)

            match (warning.status):
                case (StatusEnum.PUBLISHED):
                    try:
                        # post warning to publisher
                        published_warning_detail = await self.publish_warning(
                            warningDetailStrict(**warning.warningDetail),
                            username)
                    except PublishError as err:
                        msg = f"PublishError: {str(err)}"
                        logging.error(msg,
                                      extra={
                                          "warning": warning.warningDetail,
                                          "username": username
                                      })
                        raise HTTPException(status_code=400,
                                            detail=msg) from err
                case (StatusEnum.WITHDRAWN):
                    published_warning_detail = await self.withdraw_warning(
                        warningDetailStrict(**warning.warningDetail), username)
                case _:
                    raise HTTPException(
                        status_code=400,
                        detail=str("Error occurred while publishing"))

            # update contents of warningDetail
            warning.warningDetail = published_warning_detail.model_dump(
                exclude_none=True, exclude_unset=True)

        ########################################
        ## if NO warning publisher configured ##
        ########################################
        else:
            logging.debug("No publisher configured")

        # update/create warning and
        # mark warning as published in database
        published_warning = self.save_warning(session, username, warning,
                                              existing_warning)

        # make sure editor is set to None
        CRUD(session).assign_editor(warning=published_warning,
                                    editor=None,
                                    username=username)

        logging.info("Successfully finished publishing warning",
                     extra={"username": username})
        return published_warning
