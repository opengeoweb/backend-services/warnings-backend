# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# pylint: disable=invalid-name
"""Models mapping to the database"""
from __future__ import annotations

import uuid
from datetime import datetime, timezone
from enum import Enum

from geojson_pydantic import FeatureCollection
from pydantic import (AwareDatetime, BaseModel, ConfigDict, ValidationError,
                      ValidationInfo, field_validator)
from pydantic.functional_serializers import PlainSerializer
from sqlmodel import JSON, Column, Field, SQLModel
from typing_extensions import Annotated

## to serialize dates for datetime objects
JsonDate = Annotated[
    datetime,
    PlainSerializer(lambda x: datetime.strftime(x, '%Y-%m-%dT%H:%M:%SZ'),
                    return_type=str,
                    when_used='json-unless-none')]


class Language(str, Enum):
    """Enumeration of supported languages"""
    EN = "en"
    NL = "nl"
    FI = "fi"
    NO = "no"


class ScopeEnum(str, Enum):
    '''Enumeration of possible scope values'''
    USER = "user"
    GLOBAL = "global"


class DomainEnum(str, Enum):
    """Domain options"""
    AVIATION = "aviation"
    MARITIME = "maritime"
    PUBLIC = "public"


class DrawingInterface(BaseModel):
    """Drawing model for internal use, responses and updating"""
    # Already existing drawings cannot work with Drawing model
    # because init() is overridden.
    id: str | None = None
    objectName: str
    lastUpdatedTime: JsonDate | None = None
    scope: ScopeEnum
    geoJSON: dict = {}
    keywords: str | None = None
    # WIP - not in table, so not official
    # But probably needed!
    domain: DomainEnum | None = None  # under further discussion
    warningProposalSource: str | None = None  # under further discussion


class DrawingTableRecord(SQLModel, table=True):
    '''Class representing drawings stored in db table drawings'''
    # pylint: disable=too-many-instance-attributes

    __tablename__ = 'drawings'

    id: str | None = Field(primary_key=True)
    objectName: str | None = None
    username: str
    lastUpdatedTime: datetime | None = None
    scope: str
    geoJSON: dict = Field(sa_column=Column(JSON))
    keywords: str | None = None

    # WIP - not in table, so not official
    # But probably needed!
    domain: str | None = None  # under further discussion
    warningProposalSource: str | None = None  # under further discussion

    __pydantic_extra__ = None

    # re-validate model if data is changed:
    model_config = ConfigDict(validate_assignment=True)  # type: ignore

    @field_validator("geoJSON")
    @classmethod
    def validate_geojson(cls, value):
        """Validate that the geoJSON field is a FeatureCollection"""
        try:
            FeatureCollection(**value)
            return value
        except ValidationError as err:
            raise ValueError("Invalid GeoJSON: " + str(err)) from err

    def __init__(self,
                 username: str,
                 scope: ScopeEnum,
                 geoJSON: dict,
                 objectName: str | None = None,
                 domain: DomainEnum | None = None,
                 keywords: str | None = "Objects",
                 warningProposalSource: str | None = None) -> None:
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        super().__init__()

        # always generate unique ID
        self.id = str(uuid.uuid1())
        # always update lastUpdatedTime
        self.lastUpdatedTime = datetime.now(tz=timezone.utc)
        # default value is timestamp
        if not objectName:
            objectName = str(self.lastUpdatedTime)
        self.objectName = objectName
        # set remaining fields
        self.username = username
        self.scope = scope
        self.geoJSON = geoJSON
        self.keywords = keywords

        # WIP - not in table, so not official
        # But probably needed!
        self.domain = domain
        self.warningProposalSource = warningProposalSource

    def update(self, data: DrawingInterface):
        """Update the contents of a drawing with provided data"""
        self.objectName = data.objectName
        self.geoJSON = data.geoJSON
        self.scope = data.scope
        # always update lastUpdatedTime
        self.lastUpdatedTime = datetime.now(tz=timezone.utc)

        # WIP - not in table, so not official
        # But probably needed!
        self.domain = data.domain
        self.warningProposalSource = data.warningProposalSource


class PhenomenonEnum(str, Enum):
    """Phenomeon options"""
    WIND = "wind"
    FOG = "fog"
    THUNDERSTORM = "thunderstorm"
    SNOW_ICE = "snowIce"
    RAIN = "rain"
    HIGH_TEMP = "highTemp"
    FUNNEL_CLD = "funnelCloud"
    LOW_TEMP = "lowTemp"
    COASTAL_EVENT = "coastalEvent"


class LevelEnum(str, Enum):
    """Level options"""
    MODERATE = "moderate"
    SEVERE = "severe"
    EXTREME = "extreme"


class AreaItem(BaseModel):
    """Model for areas items"""
    geoJSON: FeatureCollection
    objectName: str
    # optional field for when a publisher is configured
    published_warning_uuid: str | None = None


class warningDetailDraft(BaseModel):
    '''
    DRAFT Warning model for warnings database

    Optional:
        phenomenon
        areas
        valid from
        valid until
        level
        probability (under construction)
        description original (under construction)
        description translation (under construction)
        domain (str, under construction)
        warningProposalSource (str, under construction)
        proposal_id (str)

    EWC fields, not required:
        authority - str, default is KNMI for now
        type - int, 0=current, 1=early
    '''

    id: str | None = None
    phenomenon: PhenomenonEnum | None = None
    areas: list[AreaItem] | None = None
    validFrom: AwareDatetime | None = None
    validUntil: AwareDatetime | None = None
    level: LevelEnum | None = None
    probability: int | str | None = None
    descriptionOriginal: str | None = None
    descriptionTranslation: str | None = None
    linked_to_id: str | None = None

    # optional field for when proposals are discarded
    proposal_id: str | None = None

    domain: DomainEnum = DomainEnum.PUBLIC
    warningProposalSource: str | None = None

    # EWC fields - not required
    authority: str | None = None
    type: int | None = None


class warningDetailStrict(warningDetailDraft):
    '''
    PUBLISHING Warning model for warnings database

    Required:
        phenomenon
        areas
        valid from
        valid until
        level
        probability
        description
        description NL
    '''

    phenomenon: PhenomenonEnum
    areas: list[AreaItem]
    validFrom: AwareDatetime
    validUntil: AwareDatetime
    level: LevelEnum
    probability: int
    descriptionOriginal: str
    descriptionTranslation: str
    domain: DomainEnum = DomainEnum.PUBLIC

    @field_validator("probability")
    @classmethod
    def validate_probability(cls, value: int, info: ValidationInfo):
        """Ensure that probability is equal to one of the following values:
            [10, 20, 30, 40, 50, 60, 70, 80, 90, 100].
            Also ensure a cross validation with the level field.
        """
        level = info.data.get("level")
        if value not in range(10, 101, 10):
            raise ValueError(
                f"Probability should be one of the following values: {list(range(10,101,10))}"
            )
        if level == LevelEnum.MODERATE and value < 30:
            raise ValueError(
                "Probability should be 30 or higher for moderate warning")
        return value


class StatusEnum(str, Enum):
    """Strict status"""
    PROPOSAL = "PROPOSAL"
    PUBLISHED = "PUBLISHED"
    EXPIRED = "EXPIRED"
    DRAFT = "DRAFT"
    DRAFT_PUBLISHED = "DRAFT_PUBLISHED"
    TODO = "TODO"
    IGNORED = "IGNORED"
    USED = "USED"
    WITHDRAWN = "WITHDRAWN"

    model_config = ConfigDict(validate_assignment=True)


class WarningInterface(BaseModel):
    '''Warning class for internal use'''
    id: str | None = None
    lastUpdatedTime: JsonDate | None = None
    status: StatusEnum
    warningDetail: warningDetailStrict | warningDetailDraft
    editor: str | None = None


class WarningTableRecord(SQLModel, table=True):
    '''Class representing warnings stored in db table warnings'''

    __tablename__ = 'warnings'

    id: str | None = Field(primary_key=True, default=None)
    lastUpdatedTime: datetime | None = None
    editor: str | None = None
    status: str
    warningDetail: dict = Field(sa_column=Column(JSON))

    __pydantic_extra__ = None

    # re-validate model if data is changed:
    model_config = ConfigDict(validate_assignment=True)  # type: ignore

    @field_validator("warningDetail")
    @classmethod
    def validate_warningDetail(cls, value: dict):
        """Validate that the warningdetail field matches the warningDetail model"""
        try:
            warningDetailDraft(**value)
            return value
        except ValidationError as err:
            raise ValueError(f"Invalid warningDetail: {err}") from err

    def __init__(self,
                 status: str,
                 warningDetail: dict,
                 editor: str | None = None) -> None:
        super().__init__()

        # if warning comes from EWC, take over the UUID
        # if not from EWC, generate UUID
        self.id = warningDetail.get('id', str(uuid.uuid1()))
        self.lastUpdatedTime = datetime.now(tz=timezone.utc)
        self.status = status
        self.editor = editor

        # TEMP FIX
        # pylint: disable=fixme
        # always making sure that the fields `id` and
        # warningDetail.id are filled in and are equal
        # to each other for consistency
        # TODO: in the future, the field warningDetail.id
        # could be renamed to warningDetail.external_id
        # as it should only be set when data has come from
        # external sources
        # this name is better to specify the purpose
        # of the field warningDetail.id field and explains
        # why it sometimes is set, and sometimes not
        self.warningDetail = warningDetail
        self.warningDetail['id'] = warningDetail.get('id', self.id)
        self.warningDetail['domain'] = warningDetail.get(
            'domain', DomainEnum.PUBLIC)

    def update(self, data: WarningTableRecord) -> None:
        """Update the contents of a warning with provided data"""

        self.status = data.status
        self.warningDetail = data.warningDetail
        # always update lastUpdatedTime
        self.lastUpdatedTime = datetime.now(tz=timezone.utc)

    def update_editor(self, editor: str | None = None) -> None:
        """Assign or unassign a warning editor"""
        self.editor = editor
        # always update lastUpdatedTime?
        self.lastUpdatedTime = datetime.now(tz=timezone.utc)
