# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This module defines the drawings endpoints for this application.
"""
from __future__ import annotations

import logging

from fastapi import (APIRouter, Body, Depends, Header, HTTPException, Path,
                     Request, Response)
from pydantic import ValidationError
from sqlmodel import Session

from app.crud.drawings import CRUD
from app.crud.errors import CreateError, DeleteError, ReadError, UpdateError
from app.db import get_session
from app.models import DrawingInterface, DrawingTableRecord, Language
from app.utils.utils import _check_authenticated

router = APIRouter()


@router.post("/drawings")
@router.post("/drawings/")
async def post_drawing(
    drawing: DrawingInterface,
    request: Request,
    response: Response,
    username: str = Header(alias='Geoweb-Username'),
    session: Session = Depends(get_session)
) -> None:
    """Saves a drawing with a new ID"""

    _check_authenticated(response, username)

    ## store drawing in database
    try:
        db_drawing = DrawingTableRecord(
            username=username,
            scope=drawing.scope,
            geoJSON=drawing.geoJSON,
            objectName=drawing.objectName,
            domain=drawing.domain,
            warningProposalSource=drawing.warningProposalSource)
        created_drawing = CRUD(session).create(db_drawing, username)
    except (CreateError, ValueError) as err:
        raise HTTPException(status_code=400, detail=str(err)) from err

    ## Set location header
    response.headers['Location'] = str(
        request.url_for('get_drawing', drawing_id=created_drawing.id))


@router.get("/drawings/{drawing_id}",
            response_model=DrawingInterface,
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
@router.get("/drawings/{drawing_id}/",
            response_model=DrawingInterface,
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
async def get_drawing(
    response: Response,
    drawing_id: str = Path(),
    username: str = Header(alias='Geoweb-Username'),
    session: Session = Depends(get_session)
) -> DrawingInterface:
    """Returns the drawing with the given drawing_id, if allowed for the current user."""

    _check_authenticated(response, username)

    response.headers['username'] = username

    try:
        drawing = CRUD(session).read_one(drawing_id, username)
    except ReadError as err:
        raise HTTPException(status_code=400, detail=err) from err

    if drawing is None:
        raise HTTPException(status_code=404, detail='Drawing not found')

    try:
        return DrawingInterface.model_validate(drawing.model_dump())
    except ValidationError as err:
        msg = "Invalid drawing retrieved from database"
        logging.error(msg, extra={"info": str(err), "drawing_id": drawing.id})
        raise HTTPException(status_code=400, detail=msg) from err


@router.get("/drawings",
            response_model=list[DrawingInterface],
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
@router.get("/drawings/",
            response_model=list[DrawingInterface],
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
async def list_drawings(
    response: Response,
    username: str = Header(alias='Geoweb-Username'),
    age: int | None = 7 * 24 * 3600,
    keywords: str | None = None,
    search: str | None = None,
    session: Session = Depends(get_session)
) -> list[DrawingInterface]:
    """Handler for getting a drawing list."""
    # pylint: disable=too-many-arguments,too-many-positional-arguments

    _check_authenticated(response, username)

    response.headers['username'] = username

    # prepare keywords for query -> from comma separated string to list of
    # keywords
    stripped_keywords: list[str] | None = [
        v.lower() for v in keywords.split(',')
    ] if keywords else None

    # perpare serach parameter for query -> from comma separated string to
    # list of search terms
    stripped_search: list[str] | None = [v.lower() for v in search.split(',')
                                        ] if search else None

    # get drawings for user
    drawings: list[DrawingTableRecord] | None = CRUD(session).read_many(
        username, age, stripped_keywords, stripped_search)
    # process result
    # database returns DrawingInterface
    # try transformation into DrawingInterface format
    result = []
    if drawings:
        for drawing in drawings:
            try:
                result.append(
                    DrawingInterface.model_validate(drawing.model_dump()))
            # log a warning and continue if invalid drawing
            # is processed
            except ValidationError as err:
                msg = "Invalid drawing retrieved from database"
                logging.warning(msg,
                                extra={
                                    "info": str(err),
                                    "drawing_id": drawing.id
                                })
    return result


@router.post("/drawings/{drawing_id}", status_code=201)
@router.post("/drawings/{drawing_id}/", status_code=201)
async def update_delete_drawing(
    response: Response,
    drawing_id: str = Path(),
    username: str = Header(alias='Geoweb-Username'),
    session: Session = Depends(get_session),
    delete: bool = False,
    drawing: DrawingInterface | None = Body(default=None)
) -> None:
    """Update or delete a drawing specified with drawing_id"""
    # pylint: disable=too-many-arguments,too-many-positional-arguments
    _check_authenticated(response, username)

    ## retrieve existing drawing
    existing_drawing: DrawingTableRecord | None = CRUD(session).read_one(
        drawing_id, username)
    if not existing_drawing:
        raise HTTPException(status_code=400,
                            detail="Drawing not found for user")

    try:
        if delete is True:
            ## delete drawing in database
            CRUD(session).delete(existing_drawing, username)
        elif delete is False and drawing is not None:
            ## update drawing in database
            CRUD(session).update(existing_drawing, drawing, username)
        elif delete is False and drawing is None:
            raise HTTPException(status_code=400,
                                detail=str("Body is missing in POST message"))
    except (DeleteError, UpdateError, ValueError) as err:
        raise HTTPException(status_code=400, detail=str(err)) from err


@router.get("/area-keywords", response_model=list[str])
@router.get("/area-keywords/", response_model=list[str])
async def list_keywords(
    response: Response,
    username: str = Header(alias='Geoweb-Username'),
    language: Language = Language.EN,
    session: Session = Depends(get_session)
) -> list[str]:
    """Handler for retrieving a list of keywords"""

    _check_authenticated(response, username)
    response.headers['username'] = username

    # language options will be implemented later
    # for now will always be "en"
    return CRUD(session).list_keywords(language)
