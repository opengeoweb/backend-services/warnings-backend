# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This modules defines the base endpoints for this application.
"""
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import PlainTextResponse
from sqlmodel import Session

from app.config import settings
from app.db import get_session
from app.utils import checks

router = APIRouter()


@router.get("/")
async def get_root():
    "Returns a welcome message"
    return PlainTextResponse('GeoWeb Warnings API')


@router.get("/version")
async def get_version():
    "Returns the application's version"
    return {'version': settings.version}


@router.get('/healthcheck')
async def get_healthcheck(session: Session = Depends(get_session)):
    """Health check that can be called to see if service is accessible.

    Returns:
        JSON containing the healthcheck status

    Example:
        Successful response::

            {
                "status": "OK",
                "service": "Warnings",
                "database": {
                    "status": "OK",
                    "warnings": {
                        "status": "OK"
                    },
                    "drawings": {
                        "status": "OK"
                    }
                }
            }

    """
    status: dict = {
        "database": {
            "status": "UNKNOWN"
        },
        "status": "UNKNOWN",
        "service": "Warnings"
    }

    database_healthy = checks.database(session, status)
    publisher_healthy = (True if not settings.warning_publisher_url else await
                         checks.publisher(status))
    service_healthy = database_healthy and publisher_healthy

    status["status"] = "OK" if service_healthy else "FAILED"
    if not service_healthy:
        raise HTTPException(status_code=503, detail=status)
    return status
