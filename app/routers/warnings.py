# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This module defines the warnings endpoints for this application.
"""
from __future__ import annotations

import logging

from fastapi import (APIRouter, Body, Depends, Header, HTTPException, Path,
                     Request, Response)
from pydantic import ValidationError
from sqlmodel import Session

from app.config import warnings_config
from app.crud.errors import (CreateError, DeleteError, EditorError, ReadError,
                             UpdateError)
from app.crud.warnings import CRUD
from app.db import get_session
from app.models import (StatusEnum, WarningInterface, WarningTableRecord,
                        warningDetailStrict)
from app.utils.utils import (_check_authenticated, _check_editor,
                             _check_editor_or_none,
                             _check_published_warning_uuid, _is_warning_active,
                             _status_change_allowed, calc_expire_endtime,
                             log_validation_errors)
from app.utils.warnings_publisher import PublishError, WarningPublisher

router = APIRouter()


@router.post("/warnings")
@router.post("/warnings/")
async def post_warning(
        warning: WarningInterface,
        request: Request,
        response: Response,
        username: str = Header(alias="Geoweb-Username"),
        session: Session = Depends(get_session),
) -> None:
    """Saves a warning with a new ID"""
    # pylint: disable=too-many-branches

    _check_authenticated(response, username)

    # if status is PUBLISHED, only then enforce strict validation
    if warning.status == StatusEnum.PUBLISHED:
        try:
            warningDetailStrict(**warning.warningDetail.model_dump())
        except ValidationError as err:
            msg = log_validation_errors(warning.id, err)
            raise HTTPException(status_code=400, detail=msg) from err

    # if status is DRAFT_PUBLISHED:
    #   - check that:
    #         - the payload contains a field "linked_to_id"
    #         - the "linked_to_id" refers to a warning in the database
    #         - the warning in the database has a status "PUBLISHED"
    #   - then remove the "id" values, so that a new warning with
    #     status "DRAFT_PUBLISHED" will be created in the database
    if (warning.status == StatusEnum.DRAFT_PUBLISHED) and (
            warning.warningDetail.linked_to_id) and (
                published_warning := CRUD(session).read_one(
                    warning.warningDetail.linked_to_id)
            ) and published_warning.status == StatusEnum.PUBLISHED:
        logging.debug("Creating a DRAFT_PUBLISHED warning")
        # remove original ID values
        warning.id = None
        warning.warningDetail.id = None

    ## generate warning for database
    db_warning = WarningTableRecord(
        status=warning.status,
        editor=username,
        warningDetail=warning.warningDetail.model_dump(exclude_none=True,
                                                       exclude_unset=True),
    )

    # if a third-party proposal comes in, set editor to None
    if warning.status == StatusEnum.PROPOSAL:
        db_warning.editor = None

    # if status is publish, call the publisher
    # the publisher will try to publish a warning (if a publisher is configured)
    # then it will save a warning marked as published in the database
    if db_warning.status == StatusEnum.PUBLISHED:
        db_warning.editor = None
        try:
            created_warning = await WarningPublisher().publisher(
                db_warning, username, session)
        except PublishError as err:
            raise HTTPException(status_code=400, detail=str(err)) from err

    ## if status is not PUBLISHED, store warning in database
    else:
        try:
            created_warning = CRUD(session).create(db_warning)
        except CreateError as err:
            raise HTTPException(status_code=400, detail=str(err)) from err

    # If status is DRAFT_PUBLISHED, unlock the related PUBLISHED warning
    if (warning.status == StatusEnum.DRAFT_PUBLISHED) and published_warning:
        logging.info("Unlock PUBLISHED warning")
        try:
            CRUD(session).assign_editor(warning=published_warning,
                                        editor=None,
                                        username=username)
        except EditorError as err:
            raise HTTPException(status_code=400, detail=str(err)) from err

    # if "proposal_id" is part if the warning payload, the related
    # proposal should be marked as "USED" after a new
    # DRAFT or PUBLISHED warning has been created in the database
    if (_proposal_id := warning.warningDetail.proposal_id) and (
            proposal_warning := CRUD(session).read_one(_proposal_id)
    ) is not None and proposal_warning.status != StatusEnum.USED:
        if proposal_warning.status == StatusEnum.PROPOSAL:
            # copy and update the contents
            copy_warning = proposal_warning.model_copy()
            copy_warning.status = "USED"
            try:
                logging.debug("Mark proposal warning as USED")
                CRUD(session).update(proposal_warning, copy_warning, username)
            except UpdateError as err:
                msg = "Failed to remove the warning from the Reviews section. " + \
                    "Please try to delete the warning proposal manually in " + \
                        "the Reviews section"
                logging.error("Failed to mark proposal as used: %s", str(err))
                raise HTTPException(status_code=400, detail=msg) from err
            logging.info("Successfully marked proposal as used")
        else:
            msg = "Failed to remove the warning from the Reviews section. " + \
                    "Please try to delete the warning proposal manually in " + \
                        "the Reviews section"
            logging.error("Failed to mark proposal as used: %s", msg)
            raise HTTPException(status_code=400, detail=msg)

    ## Set location header
    response.headers["Location"] = str(
        request.url_for("get_warning", warning_id=created_warning.id))


@router.get(
    "/warnings",
    response_model=list[WarningInterface],
    response_model_exclude_unset=True,
)
@router.get(
    "/warnings/",
    response_model=list[WarningInterface],
    response_model_exclude_unset=True,
)
async def list_warnings(
        response: Response,
        username: str = Header(alias="Geoweb-Username"),
        session: Session = Depends(get_session),
) -> list[WarningInterface]:
    """Handler for getting a warnings list"""

    _check_authenticated(response, username)

    response.headers["username"] = username

    warnings: list[WarningTableRecord] = CRUD(session).read_many()
    result = []

    for warning in warnings:
        try:
            result.append(WarningInterface.model_validate(warning.model_dump()))
        except ValidationError as err:
            msg = "Invalid warning retrieved from database"
            logging.warning(msg,
                            extra={
                                "info": str(err),
                                "warning_id": warning.id
                            })
    return result


@router.head("/warnings/{warning_id}")
@router.head("/warnings/{warning_id}/")
@router.get(
    "/warnings/{warning_id}",
    response_model=WarningInterface,
    response_model_exclude_unset=True,
)
@router.get(
    "/warnings/{warning_id}/",
    response_model=WarningInterface,
    response_model_exclude_unset=True,
)
async def get_warning(
        response: Response,
        warning_id: str = Path(),
        username: str = Header(alias="Geoweb-Username"),
        session: Session = Depends(get_session),
) -> WarningInterface:
    """Returns the warning with the given warning_id, if allowed for the current user."""

    _check_authenticated(response, username)

    try:
        warning = CRUD(session).read_one(warning_id)
    except ReadError as err:
        raise HTTPException(status_code=400, detail=err) from err

    if warning is None:
        raise HTTPException(status_code=404, detail="Warning not found")

    try:
        return WarningInterface.model_validate(warning.model_dump())
    except ValidationError as err:
        msg = "Invalid warning retrieved from database"
        logging.error(msg, extra={"info": str(err), "warning_id": warning.id})
        raise HTTPException(status_code=400, detail=msg) from err


@router.post("/warnings/{warning_id}")
@router.post("/warnings/{warning_id}/")
async def update_warning(
        response: Response,
        warning_id: str = Path(),
        warning: WarningInterface | None = Body(default=None),
        delete: bool = False,
        editor: bool = False,
        force: bool = False,
        username: str = Header(alias="Geoweb-Username"),
        session: Session = Depends(get_session),
) -> None:
    """Updates the contents of an existing warning, deletes a warning
    or assigns an editor of a warning"""
    # pylint: disable=too-many-statements,too-many-branches,too-many-arguments,too-many-positional-arguments,too-many-locals,possibly-used-before-assignment

    _check_authenticated(response, username)

    # make sure that id in payload matches warning id
    if warning and warning.warningDetail.id != warning_id:
        msg = "Warning IDs in payload and request do not match"
        logging.error(msg)
        raise HTTPException(status_code=400, detail=msg)

    # check if warning exists
    if not (existing_warning := CRUD(session).read_one(warning_id)):
        msg = "Could not update warning: warning not found in database"
        logging.info(msg,
                     extra={
                         "warning_id": warning_id,
                         "username": username
                     })
        raise HTTPException(status_code=400, detail=msg)

    # Go over options by checking the input for
    # delete, warning and editor
    match (delete, warning, editor):
    #######################################################
    ## Assign editor to warning
    ## if no warning is sent and update is False
    ## try to assign or unassign an editor for a warning
    #######################################################
        case (False, None, _):
            # if editor = True
            # editor will get value username
            if editor:
                # If force=False and existing_warning has an editor assigned, and that editor
                # is different from the username => raise a 409 response
                if not force and existing_warning.editor and existing_warning.editor != username:
                    msg = "Cannot assign editor of warning that is being edited " +\
                        "by another user."
                    raise HTTPException(status_code=409, detail=msg)

                # proceed if force=true and/or existing_warning has no editor set (or
                # set editor is equal to username)
                new_editor = username

            # if editor = False
            # editor will be set to None
            else:
                if existing_warning.editor != username:
                    msg = "Cannot unassign editor of warning that is being edited by another user"
                    logging.warning(
                        msg,
                        extra={
                            "warning_id": existing_warning.id,
                            "username": username,
                            "old editor": existing_warning.editor,
                        },
                    )
                    raise HTTPException(status_code=400, detail=msg)
                new_editor = None
            # update the editor in the database
            try:
                CRUD(session).assign_editor(warning=existing_warning,
                                            editor=new_editor,
                                            username=username)
            except EditorError as err:
                raise HTTPException(status_code=400, detail=str(err)) from err
            logging.info("Successfully updated warning editor")

    #######################################################
    ## Delete warning
    ## if delete is true, editor is false and no warning
    ## has been sent
    #######################################################
        case (True, None, False):
            # A warning can be deleted by any user, even if the
            # editor is None, or another username
            # Only condition is that the status is DRAFT or
            # DRAFT_PUBLISHED
            try:
                CRUD(session).delete(existing_warning, username)
            except DeleteError as err:
                raise HTTPException(status_code=400, detail=str(err)) from err
            logging.info("Successfully deleted warning in database")

    #######################################################
    ## Update warning
    ## if a warning body has been sent, and delete and
    ## editor are both false
    #######################################################
        case (False, warning, False) if warning is not None:
            # Check editor based on status
            match warning.status:
                case StatusEnum.EXPIRED | StatusEnum.WITHDRAWN:
                    # Skip editor check
                    pass
                case StatusEnum.IGNORED:
                    _check_editor_or_none(existing_warning.editor, username)
                case _:
                    _check_editor(existing_warning.editor, username)

            # check if status change is allowed
            logging.info("%s=>%s", existing_warning.status, warning.status)
            if not _status_change_allowed(StatusEnum(existing_warning.status),
                                          StatusEnum(warning.status)):
                logging.error(
                    "Status change from %s to %s not allowed",
                    existing_warning.status,
                    warning.status,
                )
                raise HTTPException(status_code=400,
                                    detail="Status change not allowed")

            # if the warning in the DB (existing_warning) has an areas field,
            # check the value of the published_warning_uuid key
            # published_warning_uuid if present in DB warning
            if (areas := existing_warning.warningDetail.get('areas')
               ) and len(areas) > 0:
                warning = _check_published_warning_uuid(
                    areas[0].get('published_warning_uuid'), warning)

            # if status is EXPIRED, set end time of warning to
            # current time + configured delay (in minutes)
            if warning.status == StatusEnum.EXPIRED:
                # check if warning is currently active:
                if _is_warning_active(warning):
                    logging.debug(
                        "Update endtime from %s to + %s minutes from now",
                        warning.warningDetail.validUntil,
                        warnings_config.expiration_delay)
                    # Update validUntil with current time increased with number of
                    # minutes configured in warnings_config.expiration_delay
                    warning.warningDetail.validUntil = calc_expire_endtime(
                        warnings_config.expiration_delay)
                    logging.debug("New endtime: %s",
                                  warning.warningDetail.validUntil)
                    # set status of warning back to published
                    warning.status = StatusEnum.PUBLISHED
                else:
                    msg = "Cannot expire warning as warning is not active " +\
                        "(starttime <= now < endtime)"
                    logging.error(msg)
                    raise HTTPException(status_code=400, detail=msg)

            # if status is PUBLISHED, only then enforce strict validation
            if warning.status == StatusEnum.PUBLISHED:
                try:
                    warningDetailStrict(**warning.warningDetail.model_dump())
                except ValidationError as err:
                    msg = log_validation_errors(warning.id, err)
                    raise HTTPException(status_code=400, detail=msg) from err

                # check if status change is DRAFT_PUBLISHED -> PUBLISHED:
                #     - if so, a check is done that the warning related to
                #       the "linked_to_id" exists, and that the status is
                #       "PUBLISHED"
                #     - then "existing_warning" parameter is updated with
                #       the warning based on the "linked_to_id" parameter
                #     - and IDs in the warning and warningDetail body are updated
                is_draft_published = existing_warning.status == StatusEnum.DRAFT_PUBLISHED
                if is_draft_published and warning.warningDetail.linked_to_id and (
                        existing_warning := CRUD(session).read_one(
                            warning.warningDetail.linked_to_id)
                ) and (existing_warning.status == StatusEnum.PUBLISHED):

                    # modify the IDs to make sure that the PUBLISHED warning will be updated
                    warning.id = warning.warningDetail.linked_to_id
                    warning.warningDetail.id = warning.warningDetail.linked_to_id
                    # reset the "linked_to_id" field
                    warning.warningDetail.linked_to_id = None

            # cast warning into WarningTableRecord
            db_warning = WarningTableRecord(
                status=warning.status,
                editor=username,
                warningDetail=warning.warningDetail.model_dump(
                    exclude_none=True, exclude_unset=True),
            )

            # if status is PUBLISHED or WITHDRAWN, call the publisher
            # the publisher will try to publish a warning (if a publisher is configured)
            # then it will save a warning marked as published or withdrawn in the database
            if db_warning.status in [
                    StatusEnum.PUBLISHED, StatusEnum.WITHDRAWN
            ]:
                db_warning.editor = None
                try:
                    await WarningPublisher().publisher(db_warning, username,
                                                       session,
                                                       existing_warning)
                except PublishError as err:
                    raise HTTPException(status_code=400,
                                        detail=str(err)) from err

                # if is_draft_published: delete the DRAFT_PUBLISHED warning
                # after the published warning has been updated
                if db_warning.status == StatusEnum.PUBLISHED and is_draft_published:
                    logging.debug(
                        "Delete DRAFT_PUBLISHED after publishing update")
                    try:
                        if (warning_to_delete :=
                                CRUD(session).read_one(warning_id)):
                            CRUD(session).delete(warning_to_delete, username)
                    except (ReadError, DeleteError) as err:
                        raise HTTPException(status_code=400,
                                            detail=str(err)) from err

            # if status is not PUBLISHED, update the warning in the database
            else:
                try:
                    CRUD(session).update(existing_warning, db_warning, username)
                except UpdateError as err:
                    raise HTTPException(status_code=400,
                                        detail=str(err)) from err

    #######################################################
    ## Catch an invalid request
    #######################################################
        case _:
            raise HTTPException(status_code=400, detail=str("Invalid request"))
