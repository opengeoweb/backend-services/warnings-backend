# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module handles the interaction with the database"""
#pylint: disable=fixme,invalid-name,redefined-variable-type
from __future__ import annotations

import logging
from datetime import datetime, timedelta, timezone

from sqlalchemy import and_, func, or_, select
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from sqlalchemy.orm import Session
from sqlmodel import col

from app.crud.errors import (CreateError, DeleteError, EditorError, ReadError,
                             UpdateError)
from app.models import StatusEnum, WarningTableRecord


class CRUD:
    """CRUD operations for warnings"""

    _session: Session

    def __init__(self, session: Session) -> None:
        self._session = session

    def read_one(self, warning_id: str) -> WarningTableRecord | None:
        """Reads one warning.
        :param warning_id: ID of the warning
        :returns Warning|None:
        :raises ReadError:
        """

        logging.debug("loading warning from database",
                      extra={"warning_id": warning_id})
        try:
            statement = select(WarningTableRecord).filter(
                WarningTableRecord.id == warning_id)
            return self._session.scalars(statement).first()
        except SQLAlchemyError as err:
            logging.error("error while loading warning from database",
                          extra={
                              "warning_id": warning_id,
                              "err": err,
                          })
            raise ReadError('Error while reading warning') from err

    def create(self, data: WarningTableRecord) -> WarningTableRecord:
        """_summary_

        Args:
            data (WarningTableRecord): Warning to be stored in database

        Raises:
            CreateError: upon error storing warning in database

        Returns:
            WarningTableRecord: Warning that was stored in database
        """

        logging.info("inserting warning into database")
        try:
            self._session.add(data)
            self._session.commit()
        except IntegrityError as err:
            logging.error("error inserting warning into database",
                          extra={"err": err})
            raise CreateError(
                "Error while creating warning: ID already in database. Check input and retry."
            ) from err
        return data

    def read_many(self) -> list[WarningTableRecord]:
        """_summary_

        Returns:
            list[WarningTableRecord]: a list of warnings
                containing four sections:
                - TODO warnings -> exact definition will be defined later on
                - DRAFT warnings -> warnings with status DRAFT
                - PUBLISHED warnings -> warnings with status DRAFT and
                                     validUntil in the FUTURE
                - EXPIRED warnings -> warnings with status != DRAFT
                                     and validUntil between the last
                                     24 hours and NOW
                Within each section, warnings are sorted based on
                lastUpdatedTime (latest first)
        """

        # Fetch times and set date_format constant
        DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
        now: datetime = datetime.now(tz=timezone.utc)
        cutoff: datetime = now - timedelta(hours=24)

        # create empty list to append query results
        result = []

        # TODO query
        # conditions:
        #  - status != draft
        #  - status != draft_published
        #  - status != published
        #  - status != ignored
        #  - status != used
        #  - validUntil >= now
        # status is set to TODO
        try:
            logging.debug("loading TODO warnings from database")
            statement_todo = select(WarningTableRecord).filter(
                WarningTableRecord.warningDetail['validUntil'].as_string()
                >= datetime.strftime(now, DATE_FORMAT),
                WarningTableRecord.status != StatusEnum.DRAFT,
                WarningTableRecord.status != StatusEnum.DRAFT_PUBLISHED,
                WarningTableRecord.status != StatusEnum.PUBLISHED,
                WarningTableRecord.status != StatusEnum.IGNORED,
                WarningTableRecord.status != StatusEnum.WITHDRAWN,
                WarningTableRecord.status != StatusEnum.USED).order_by(
                    col(WarningTableRecord.lastUpdatedTime).desc())
            query_todo = self._session.scalars(statement_todo).all()
            # set status to TODO
            for warning in query_todo:
                warning.status = StatusEnum.TODO
                result.append(warning)
        except SQLAlchemyError as err:
            logging.error("error while loading TODO warnings from database",
                          exc_info=True,
                          extra={
                              "err": err,
                          })

        # DRAFT query
        # conditions:
        #  - status == draft OR draft_published
        try:
            logging.debug(
                "loading DRAFT and DRAFT_PUBLISHED warnings from database")
            statement_draft = select(WarningTableRecord).filter(
                or_(WarningTableRecord.status == StatusEnum.DRAFT,
                    WarningTableRecord.status ==
                    StatusEnum.DRAFT_PUBLISHED)).order_by(
                        col(WarningTableRecord.lastUpdatedTime).desc())
            result += self._session.scalars(statement_draft).all()
        except SQLAlchemyError as err:
            logging.error("error while loading DRAFT warnings from database",
                          exc_info=True,
                          extra={
                              "err": err,
                          })

        # PUBLISHED query
        # conditions:
        #  - status == published
        #  - validUntil >= now
        try:
            logging.debug("loading PUBLISHED warnings from database")
            statement_published = select(WarningTableRecord).filter(
                WarningTableRecord.warningDetail['validUntil'].as_string()
                >= datetime.strftime(now, DATE_FORMAT),
                WarningTableRecord.status == StatusEnum.PUBLISHED).order_by(
                    col(WarningTableRecord.lastUpdatedTime).desc())
            result += self._session.scalars(statement_published).all()
        except SQLAlchemyError as err:
            logging.error(
                "error while loading PUBLISHED warnings from database",
                exc_info=True,
                extra={
                    "err": err,
                })

        # EXPIRED query
        # conditions:
        #  - status != draft &&
        #  - status != ignored &&
        #  - status != draft_published &&
        #  - status != used &&
        #  - status != withdrawn &&
        #  - validUntil < now &&
        #  - validUntil > cutoff
        #  - OR
        #  - status == withdrawn &&
        #  - lastUpdatedTime >= cutoff
        try:
            logging.debug(
                "loading EXPIRED and WITHDRAWN warnings from database")
            statement_expired = select(WarningTableRecord).filter(
                or_(
                    and_(
                        WarningTableRecord.status != StatusEnum.DRAFT,
                        WarningTableRecord.status != StatusEnum.DRAFT_PUBLISHED,
                        WarningTableRecord.status != StatusEnum.IGNORED,
                        WarningTableRecord.status != StatusEnum.USED,
                        WarningTableRecord.status != StatusEnum.WITHDRAWN,
                        WarningTableRecord.warningDetail['validUntil'].
                        as_string() < datetime.strftime(now, DATE_FORMAT),
                        WarningTableRecord.warningDetail['validUntil'].
                        as_string() >= datetime.strftime(cutoff, DATE_FORMAT)),
                    and_(
                        WarningTableRecord.status == StatusEnum.WITHDRAWN,
                        WarningTableRecord.lastUpdatedTime  # type: ignore
                        >= cutoff))).order_by(
                            col(WarningTableRecord.lastUpdatedTime).desc())
            query_expired = self._session.scalars(statement_expired).all()
            for warning in query_expired:
                if warning.status != StatusEnum.WITHDRAWN:
                    warning.status = StatusEnum.EXPIRED
                result.append(warning)
        except SQLAlchemyError as err:
            logging.error(
                "error while loading EXPIRED and WITHDRAWN warnings from database",
                exc_info=True,
                extra={
                    "err": err,
                })

        logging.info("returning %s warnings", len(result))
        return result

    def collect_drafts_for_cleanup(self,
                                   age: int = 24) -> list[WarningTableRecord]:
        """Collect drafts with lastUpdatedTime older than 'age' hours to delete"""

        # Time constancts
        now: datetime = datetime.now(tz=timezone.utc)
        cutoff: datetime = now - timedelta(hours=age)
        result: list = []

        try:
            logging.info(
                "loading DRAFT and DRAFT_PUBLISHED warnings where lastUpdatedTime > now-%shrs",
                age)
            statement_draft = select(WarningTableRecord).filter(
                and_(
                    or_(WarningTableRecord.status == StatusEnum.DRAFT,
                        WarningTableRecord.status ==
                        StatusEnum.DRAFT_PUBLISHED)),
                WarningTableRecord.lastUpdatedTime  # type: ignore
                <= cutoff)
            result += self._session.scalars(statement_draft).all()
        except SQLAlchemyError as err:
            logging.error("error while loading DRAFT warnings from database",
                          exc_info=True,
                          extra={
                              "err": err,
                          })
        return result

    def update(self, warning: WarningTableRecord, data: WarningTableRecord,
               username: str):
        """Update a warning

        Args:
            warning (WarningTableRecord): warning in table
            data (WarningInterface): warning data for update
            username (str): user who updated the warning

        Raises:
            UpdateError: error upon updating the warning in the database
        """

        logging.info("Updating warning in database",
                     extra={
                         "warning_id": warning.id,
                         "username": username
                     })
        try:
            warning.update(data)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error("error while updating warning in database",
                          extra={
                              "warning_id": warning.id,
                              "username": username,
                              "err": err
                          })
            raise UpdateError('Error updating warning') from err

    def delete(self, warning: WarningTableRecord, username: str):
        """deletes a warning

        Args:
            warning (WarningTableRecord): warning to be deleted
            username (str): username

        Raises:
            DeleteError: upon error deleting the warning in the database
        """

        logging.info("Deleting warning in database",
                     extra={
                         "warning_id": warning.id,
                         "username": username
                     })

        ## only warnings with status DRAFT or DRAFT_PUBLISHED can be deleted
        if not warning.status in [StatusEnum.DRAFT, StatusEnum.DRAFT_PUBLISHED]:
            msg = "Deletion of warning only allowed if status is DRAFT"
            logging.error(msg)
            raise DeleteError(msg)
        self._session.delete(warning)
        self._session.commit()

    def assign_editor(self,
                      warning: WarningTableRecord,
                      username: str,
                      editor: str | None = None) -> None:
        """Update the editor in the database

        Args:
            warning (WarningTableRecord): warning in database
            username (str): username
            editor (str | None, optional): editor of the warning. Defaults to None.

        Raises:
            EditorError: raised upon error during database interaction
        """

        logging.info("Updating warning editor in database",
                     extra={
                         "warning_id": warning.id,
                         "new editor": editor,
                         "old editor": warning.editor,
                         "username": username
                     })

        try:
            warning.update_editor(editor)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error("error while updating editor in database",
                          extra={
                              "warning_id": warning.id,
                              "username": username,
                              "err": err
                          })
            raise EditorError('Error updating editor') from err

    def count_rows(self) -> int:
        """Counts all objects in the warnings database table

        Returns:
            int: number of objects in the warnings database table
        """
        # pylint: disable=not-callable
        query = self._session.scalars(
            select(func.count()).select_from(WarningTableRecord)).all()
        return int(query.pop())
