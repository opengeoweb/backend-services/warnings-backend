# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module handles the interaction with the database"""
from __future__ import annotations

import logging
from datetime import datetime, timedelta, timezone
from operator import or_

from sqlalchemy import and_, delete, func, not_, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session
from sqlalchemy.sql.elements import BooleanClauseList
from sqlalchemy.sql.selectable import Select
from sqlmodel import col

from app.crud.errors import CreateError, DeleteError, ReadError, UpdateError
from app.models import DrawingInterface, DrawingTableRecord, ScopeEnum


class CRUD:
    """CRUD operations for drawings"""

    _session: Session

    def __init__(self, session: Session) -> None:
        self._session = session

    def read_many(
            self,
            username: str | None = None,
            age: int | None = None,
            keywords: list[str] | None = None,
            search: list[str] | None = None) -> list[DrawingTableRecord] | None:
        """returns: a list of global drawings and drawings for the given user
        :param username: current user
        :param age: age of drawings in seconds
        :param keywords: list of keywords
        :param search: list of search terms
        :raises SQLAlchemy error:
        """
        # pylint: disable=too-many-locals

        ## TEMPORARY SOLUTION ##
        ## Preferably, the original/previous way of querying is executed.
        ## However, due to performance issues it was desired to split the
        ## queries and set a limit for GLOBAL objects to be returned.
        ## This prevents timeouts in the response.
        ## It was tried to execute one query with UNION but that did not
        ## work out. Therefore this temporary workaround below was implemented
        ## After implementing more advanced filtering options or a different
        ## handling of global objects, attempt to revert this change in
        ## executing the queries

        ## Update 07/2024: got union working in DEV version. However, since
        ## tests use SQLite, tests fail as an invalid SQL is created, see:
        ## https://github.com/sqlalchemy/sqlalchemy/issues/8094

        ##### AREA AND GLOBAL OBJECT QUERY #####
        # areas DONT have the keyword "objects"
        areas_statement = select(DrawingTableRecord).filter(
            not_(func.lower(DrawingTableRecord.keywords).contains("objects")))

        # always query for global objects with keyword "objects"
        global_statement = select(DrawingTableRecord).filter(
            and_(
                func.lower(DrawingTableRecord.keywords).contains("objects"),
                DrawingTableRecord.scope == ScopeEnum.GLOBAL)).order_by(
                    col(DrawingTableRecord.lastUpdatedTime).desc()).limit(50)
        if (age_condition := self._get_age_condition(age)) is not None:
            global_statement = global_statement.filter(age_condition)

        ##### FILTER RESULTS #####
        if isinstance(keywords, list):
            areas_statement = self._do_keywords_query(areas_statement, keywords)
            global_statement = self._do_keywords_query(global_statement,
                                                       keywords)
        if isinstance(search, list):
            global_statement = self._do_search_query(global_statement, search)
            areas_statement = self._do_search_query(areas_statement, search)

        ##### GET RESULTS #####
        areas_result = self._session.scalars(
            areas_statement.order_by(DrawingTableRecord.objectName)).all()
        object_result = self._session.scalars(global_statement).all()

        ##### USER OBJECT QUERY #####
        # if username provided, add query for user name
        # user objects with keyword "objects"
        if (username_condition :=
                self._get_username_condition(username)) is not None:
            user_statement = select(DrawingTableRecord).filter(
                and_(
                    func.lower(DrawingTableRecord.keywords).contains("objects"),
                    username_condition))

            ### FILTER RESULTS ###
            # age
            if age_condition is not None:
                user_statement = user_statement.filter(age_condition)
            # keywords
            if isinstance(keywords, list):
                user_statement = self._do_keywords_query(
                    user_statement, keywords)
            # search
            if isinstance(search, list):
                user_statement = self._do_search_query(user_statement, search)

            ### GET RESULTS ###
            user_result = self._session.scalars(user_statement).all()
            object_result += user_result

        # re-order OBJECT result based on last-updated time
        def _sort(item):
            return item.lastUpdatedTime

        object_result.sort(key=_sort, reverse=True)

        # add area results at the end of the list
        result = object_result + areas_result
        ## END OF TEMPORARY SOLUTION ##

        try:
            logging.info("loading drawings from database",
                         extra={"username": username or ''})
            return result
        except SQLAlchemyError as err:
            logging.error("error while loading drawings from database",
                          extra={
                              "username": username or '',
                              "err": err,
                          })
        return None

    def read_one(self,
                 drawing_id: str,
                 username: str | None = None) -> DrawingTableRecord | None:
        """Reads one drawing.
        :param drawing_id: ID of the drawing
        :param username: current user
        :returns DrawingTableRecord|None:
        :raises ReadError:
        """

        logging.info("loading drawing from database",
                     extra={
                         "drawing_id": drawing_id,
                         "username": username or '',
                     })
        try:
            statement = select(DrawingTableRecord).filter(
                and_(
                    DrawingTableRecord.id == drawing_id,
                    or_(DrawingTableRecord.scope == ScopeEnum.GLOBAL,
                        self._get_username_condition(username))))
            return self._session.scalars(statement).first()
        except SQLAlchemyError as err:
            logging.error("error while loading drawing from database",
                          extra={
                              "drawing_id": drawing_id,
                              "username": username or '',
                              "err": err,
                          })
            raise ReadError('Error while reading drawing') from err

    def create(self, data: DrawingTableRecord,
               username: str) -> DrawingTableRecord:
        """Creates a drawing

    Args:
        data (DrawingInterface): Drawing to be stored in database
        username (str): current user

    Returns:
        DrawingTableRecord: Drawing that was stored in database

    Raises:
        CreateError: upon error storing drawing in database
    """

        ## make sure title does not already exist
        if self._title_exists(data.objectName, username):
            raise CreateError(
                "Object name already exists, please choose a different name")

        logging.info("inserting drawing into database",
                     extra={"username": username})

        try:
            self._session.add(data)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error("error inserting drawing into database",
                          extra={
                              "username": username,
                              "err": err
                          })
            raise CreateError("Error while creating drawing") from err
        return data

    def update(self, drawing: DrawingTableRecord, data: DrawingInterface,
               username: str) -> None:
        """Updates a drawing

        Args:
            drawing (DrawingTableRecord): Drawing stored in database
            data (DrawingTableRecord): Updated drawing
            username (str): username

        Raises:
            UpdateError: upon error updating the database
        """

        if data.objectName != drawing.objectName and self._title_exists(
                data.objectName, username):
            raise UpdateError(
                'Object name already exists, please choose a different name')

        # update drawing
        logging.info("updating drawing in database",
                     extra={
                         "drawing_id": drawing.id,
                         "username": username
                     })
        try:
            drawing.update(data)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error("error while updating drawing in database",
                          extra={
                              "drawing_id": drawing.id,
                              "username": username,
                              "err": err
                          })
            raise UpdateError('Error updating drawing') from err

    def delete(self, drawing: DrawingTableRecord, username: str) -> None:
        """deletes a drawing

        Args:
            drawing (DrawingTableRecord): Drawing stored in database
            username (str): username

        Raises:
            DeleteError: upon error deleting the database
        """
        # deletes drawing
        logging.info("deleting drawing in database",
                     extra={
                         "drawing_id": drawing.id,
                         "username": username
                     })
        if drawing.scope != ScopeEnum.USER:
            logging.info(
                "Can not delete this drawing as it does not have the USER scope"
            )
            raise DeleteError("Can not delete, not USER scope")
        self._session.delete(drawing)
        self._session.commit()

    def _title_exists(self, title: str | None, username: str) -> bool:
        """_summary_

        Args:
            title (str | None): title of warning
            username (str): current user

        Returns:
            bool: True or False if title exists

        Raises:
          SQLAlchemyError: upon database interaction error
        """

        # in case title=None
        if not title:
            return False

        # otherwise title is string
        same_user_title = and_(
            DrawingTableRecord.scope == ScopeEnum.USER,  # type: ignore
            DrawingTableRecord.username == username,
            DrawingTableRecord.objectName == title)

        try:
            existing = self._session.scalars(
                select(DrawingTableRecord).where(same_user_title)).all()

        except SQLAlchemyError as err:
            logging.error("Error while checking if object name exists",
                          extra={
                              "title": title,
                              "username": username,
                              "err": err
                          })
            return True

        if len(existing) == 0:
            return False

        logging.warning("Title already exists for user",
                        extra={
                            "title": title,
                            "username": username
                        })
        return True

    def clear_areas(self) -> None:
        """Deletes all objects in database that don't have
        an 'Objects' keyword"""
        try:
            stmt = delete(DrawingTableRecord).filter(
                not_(
                    func.lower(
                        DrawingTableRecord.keywords).contains("objects")))
            self._session.execute(stmt)
            self._session.commit()

        except SQLAlchemyError as err:
            logging.error("Error while deleting areas from drawings table",
                          extra={"err": err})

    def list_keywords(self, language: str = "en") -> list[str]:
        """Get a list of unique keywords in the drawings table

        Args:
            language (str, optional): Language of keywords. Defaults to "en".

        Returns:
            list[str]: List of keywords
        """
        # "language" paramter will be used later, use it for a debug log
        # for now (will not pollute logs + no linting errors )
        logging.debug("Selected language: %s", language)
        try:
            stmt = select(DrawingTableRecord.keywords).distinct()
            return list(
                set(val for v in self._session.scalars(stmt).all()
                    for val in v.split(",")))
        except SQLAlchemyError as err:
            logging.error("Error while retrieving keywords from database",
                          extra={'err': err})
            return []

    def _get_username_condition(
            self, username: str | None) -> BooleanClauseList | None:
        """Returns the username condition for the query, or None if no
        (or empty) username was provided"""
        username_condition = None
        if username:
            username_condition = and_(
                DrawingTableRecord.scope == ScopeEnum.USER,  # type: ignore
                DrawingTableRecord.username == username)
        return username_condition

    def _get_age_condition(self, age: int | None) -> BooleanClauseList | None:
        """Returns the age condition for the query, or None if no age
        was provided"""
        age_condition = None
        if age is not None:
            now: datetime = datetime.now(tz=timezone.utc) + timedelta(seconds=1)
            start: datetime = now - timedelta(seconds=age)
            age_condition = and_(
                DrawingTableRecord.lastUpdatedTime <= now,  # type: ignore
                DrawingTableRecord.lastUpdatedTime > start)  # type: ignore
        return age_condition

    def _do_keywords_query(self, statement: Select,
                           keywords: list[str]) -> Select:
        """Do keywords query on given statement"""
        for keyword in keywords:
            statement = statement.filter(
                func.lower(DrawingTableRecord.keywords).regexp_match(
                    f'(^|,){keyword}(,|$)'))

        return statement

    def _do_search_query(self, statement: Select, search: list[str]) -> Select:
        """Do search query on given statement"""
        return statement.filter(
            and_(
                func.lower(DrawingTableRecord.objectName).contains(v)
                for v in search))

    def count_rows(self) -> int:
        """Counts all objects in the drawings database table

        Returns:
            int: number of objects in the drawings database table

        """
        # pylint: disable=not-callable
        query = self._session.scalars(
            select(func.count()).select_from(DrawingTableRecord)).all()
        return int(query.pop())
