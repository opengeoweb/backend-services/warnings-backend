# Local publisher

This is a micro-service that listens to publish requests of the warnings backend and writes the contents ot the local file system.

## Table of Contents

[[_TOC_]]

## Configuration

The publisher service requires three parameters to be set:
- `WARNING_PUBLISHER_URL`: URL to which the Warnings BE will publish
- `LOCAL_PUBLISH_DIR`: Local output directory
- `SINGLE_AREA_PER_WARNING`: A warning can contain multiple areas. By setting this key to `"TRUE"`, a separate warning will be saved per area (one area per warning). If this key is set to `"FALSE"`, a warning will be saved with multiple areas. Defaults to `"FALSE"`.

## Start with uvicorn

1. Set your environment variables:

```
poetry shell

export WARNING_PUBLISHER_URL=http://0.0.0.0:8090
export LOCAL_PUBLISH_DIR="./localpublishdir"
export SINGLE_AREA_PER_WARNING="FALSE"
```

2. Create output directory if it does not already exist:

```
cd publisher_local
mkdir -p $LOCAL_PUBLISH_DIR
```

3. Start the publisher service. Make sure to run this command from the `publisher_local` directory:

```
cd publisher_local
bash start_publisher_local.sh
```

## Start with Docker

You can start the local publisher with docker in two different ways: as a standalone container, or with docker compose.

### Start as standalone service
1. Start the publisher in a single container. First set your environment variables:

```
export WARNING_PUBLISHER_URL=http://0.0.0.0:8090
export LOCAL_PUBLISH_DIR="/localpublishdir"
export SINGLE_AREA_PER_WARNING="FALSE"
```

2. Create the directory if it does not already exist. Also make sure that the container can write output to the directory:

```
cd publisher_local
mkdir -p $LOCAL_PUBLISH_DIR
chmod 777 $LOCAL_PUBLISH_DIR
```

3. Then build and run the container. Make sure to run the commands from the `publisher_local` directory:

```
cd publisher_local
docker build -t warnings_publisher_local .
docker run -it -p 8090:8090 \
    -v ./localpublishdir:/localpublishdir \
    --name warning_publisher_local \
    -e LOCAL_PUBLISH_DIR="${LOCAL_PUBLISH_DIR}" \
    -e SINGLE_AREA_PER_WARNING="FALSE" \
    warnings_publisher_local
```

4. Then you can start the warnings backend with `bin/start-dev.sh`.

### Start with docker compose
1. Make sure to add the following parameters to your `.env` file:

```
LOCAL_PUBLISH_DIR="/publisher_local/localpublishdir"
SINGLE_AREA_PER_WARNING="FALSE"
```

2. Create the publish directory if it does not already exist. Make sure that the container can write output to the directory:

```
mkdir -p publisher_local/$LOCAL_PUBLISH_DIR
chmod 777 publisher_loca/$LOCAL_PUBLISH_DIR
```

3. Start the warnings backend (running the commands from the root directory), including publisher with:

```
docker-compose -f docker-compose-publisher.yml up -d --build
```

or

```
docker compose -f docker-compose-publisher.yml up -d --build
```

depending on your docker compose version.

4. Navigate to https://localhost:4443/ and accept the certificate

5. Now you can configure your local FE config.json with the URL above and start using the warnings-backend

## Endpoints

The local publisher has three endpoints:

### "/" (root)

By going to the root endpoint, you should see a welcome message like `"Welcome to the warnings local publisher"` with a 200 status code.

### "/healthcheck"

This endpoints checks the health of the service. It should return `{"status": "OK", "service": "Local publisher"}` with a 200 status code.

### "/publish"

When configured, the [warnings-backend publisher](../app/utils/warnings_publisher.py) posts to this endpoint for publishing a new warning or updating a published warning.

The expected payload is as follows:

```json
{
    "warning": {
        ....
        "areas": [{
            ...
            "published_warning_uuid": "..." # optional
        }]
    }
}
```

The published files should show up in the `publisher_local/localpublishdir" that was created in the startup-instructions.
