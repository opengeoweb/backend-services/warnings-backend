# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Local publisher micro-service"""
import json
import os
from copy import deepcopy
from uuid import uuid1

from fastapi import FastAPI
from fastapi.exceptions import HTTPException
from fastapi.responses import PlainTextResponse
from pydantic_settings import BaseSettings


class PublisherSettings(BaseSettings):
    """Settings for local publisher"""
    local_publish_dir: str = "localpublishdir"
    single_area_per_warning: str = "FALSE"


publisher_settings = PublisherSettings()

app = FastAPI(docs_url="/api")


@app.get("/")
async def get_root():
    "Return a welcome message"
    return PlainTextResponse("Welcome to the warnings local publisher")


@app.get("/healthcheck")
async def get_healthcheck():
    "Return OK"
    return {"status": "OK", "service": "Local publisher"}


def _write_file(data, filename):
    with open(filename, "w", encoding="utf-8") as file:
        json.dump(data, file, indent=4, ensure_ascii=False)


@app.post("/publish")
@app.post("/publish/")
async def save_file(data: dict):
    """Save warnings on local system"""

    print(f"Received data: {data}")

    # Check if correct fields are in data
    if not data.get("warning"):
        msg = "Payload is missing 'warning' field"
        print(msg)
        raise HTTPException(status_code=400, detail=msg)
    if not data.get("warning").get("areas"):
        msg = "Payload is missing 'areas' field"
        print(msg)
        raise HTTPException(status_code=400, detail=msg)
    if not isinstance(data.get("warning").get("areas"), list):
        msg = "'areas' field should contain a list"
        print(msg)
        raise HTTPException(status_code=400, detail=msg)

    # Save single warning
    if publisher_settings.single_area_per_warning == "FALSE":
        # Check if warning already exists
        published_warning_id = data.get("warning").get("areas")[0].get(
            "published_warning_uuid", uuid1())
        filename = f"{publisher_settings.local_publish_dir}/warning_{published_warning_id}.json"
        if os.path.exists(filename):
            print("Update published warning")
            # overwrite to JSON file
            _write_file(data["warning"], filename)

        else:
            print("Publish new warning")
            # set published_warning_id to areas
            for i, _ in enumerate(data.get("warning").get("areas")):
                data.get("warning").get("areas")[i][
                    "published_warning_uuid"] = str(published_warning_id)
            # write to JSON file
            _write_file(data["warning"], filename)

    # Save a separate warning per area
    else:

        for i, _area in enumerate(data.get("warning").get("areas")):
            published_warning_id = _area.get("published_warning_uuid", uuid1())
            filename = f"./{publisher_settings.local_publish_dir}/warning"+\
                "_{published_warning_id}.json"

            _data = deepcopy(data["warning"])
            _data["areas"] = _area
            if os.path.exists(filename):
                print("Update published warning")
                _write_file(_data, filename)
            else:
                print("Publish new warning")
                # set the published_warning_uuid
                _data["areas"]["published_warning_uuid"] = str(
                    published_warning_id)
                data["warning"]["areas"][i]["published_warning_uuid"] = str(
                    published_warning_id)

                _write_file(_data, filename)

    return data["warning"]
