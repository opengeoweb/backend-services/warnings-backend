#!/usr/bin/env python

# Copyright 2023 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module for application-wide logging configuration"""

import json
import logging
import logging.config
import os

import typer
from alembic.command import stamp
from alembic.config import Config
from geojson_rewind import rewind
from pydantic import ValidationError
from sqlalchemy import inspect
from sqlalchemy.orm import Session

from app.crud.drawings import CRUD as drawings
from app.crud.errors import CreateError
from app.db import engine
from app.models import DrawingTableRecord

# Use INFO if level not valid
log_level = logging.getLevelName(os.getenv('LOG_LEVEL', 'INFO'))

logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'json': {
            'class': 'app.logger.JsonFormatter'
        }
    },
    'handlers': {
        'stream': {
            'class': 'logging.StreamHandler',
            'formatter': 'json',
            'stream': 'ext://sys.stderr',
        }
    },
    'root': {
        'handlers': ['stream'],
        'level': log_level,
    },
    'loggers': {
        'sqlalchemy': {
            'level': "ERROR",
        },
        'alembic': {
            'level': log_level,
        },
        'alembic.runtime.migration': {
            'level': log_level,
        },
    },
})

app = typer.Typer()


@app.command()
def enable_alembic() -> None:
    """Inspect the state of the database and make sure Alembic is enabled"""
    alembic_cfg = Config('alembic.ini')
    inspector = inspect(engine)
    if not inspector.has_table('alembic_version'):
        if inspector.has_table('drawings'):
            # pre-alembic schema seems to exist, so stamp current database with initial version
            stamp(alembic_cfg, revision='69bfc03a0982')


@app.command()
def clear_areas() -> None:
    """Clear all area objects from the drawings table"""
    logging.info("Deleting predefined areas")
    with Session(engine) as session:
        drawings(session).clear_areas()


@app.command()
def load_predefined_areas(area_dir: str = './staticareas') -> None:
    """Load all JSON files in the "area_dir" directory to the drawings table"""

    for subdir, _, files in os.walk(area_dir):
        for file in files:
            try:

                # load JSON file
                with open(f"{subdir}/{file}", mode="rb") as _file:
                    data: dict = json.load(_file)

                # add username
                data["username"] = "area"

                # go over feature list to counter-clockwise winding of coordinates
                for i, _feature in enumerate(
                        data.get("geoJSON", {}).get("features", [])):
                    if geometry := _feature.get("geometry", None):
                        ccw_geometry = rewind(geometry)
                        data["geoJSON"]["features"][i][
                            "geometry"] = ccw_geometry
                    else:
                        logging.warning(
                            "No geometry: %s - counter clockwise direction not guaranteed",
                            file)

                # area to database
                db_item = DrawingTableRecord(**data)
                with Session(engine) as session:
                    drawings(session).create(db_item, "area")

            except (AttributeError, CreateError, ValidationError) as err:
                logging.warning("Failed adding area to drawings table",
                                extra={
                                    "area": f"{subdir}/{file}",
                                    "err": err
                                })


if __name__ == "__main__":
    app()
