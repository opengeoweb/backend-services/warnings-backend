# GeoWeb warnings backend

This repository contains the code to run, develop and deploy the warnings-backend for the [GeoWeb project](https://gitlab.com/opengeoweb). The warnings-backend provides the infrastructure to save drawing objects and transform these drawing objects to weather warnings, with a warning being a drawing object with metadata. The metadata provides information about the warning such as weather phenomenon, severity, start- and end-time and more.

Users are able to save, update and delete drawing objects in a database. By adding metadata to the drawing object, users can also save, update and delete weather warnings. The warnings have a lifecycle attached to them: this means that they are saved in the database with a status, for example: `"DRAFT"` or `"PUBLISHED"`.

It is also possible to configure a publisher system in the warning-backend. If a warning is marked as `"PUBLISHED"`, the contents of the warning are not only updated in the database, they are also sent to the configured publisher system. For more information, see [here](#publishing-a-warning).

Below, you can find a schematic overview of the different components of the backend:

```mermaid
flowchart TD

id1{{Frontend application}}<-->|Requests from and reponses to FE|id2[NGINX]
id2:::beclass<-->|Validated response to FE \n\n Authenticated and authorized request \n to the main Fastapi BE container|id3[Warnings FastAPI BE]
id3:::beclass<-->|Retrieve and update \n drawings and warnings data|id4[(Warnings Database)]:::beclass
id3-.->|OPTIONAL: \n Warnings for publishing|id5[Warnings Publisher]

style id1 stroke-width:3px

classDef beclass fill:#bbe3a4,stroke:#597549,stroke-width:3px

style id5 stroke-dasharray: 5 5,fill:#bbe3a4,stroke:#597549,stroke-width:3px



```



## Table of Contents

[[_TOC_]]

# API documentation

The API provides a number of endpoints. A [healthcheck](#get-healthcheck) and [version](#get-version) provide basic information about the status of the backend. Further, the `/drawings` and `/warnings` endpoints provide access to drawing and warning functionalities. You can go to `/api` of any BE deployment for more specific information on the data structures and field types (for example, you can [run the backend using Uvicorn](#development-local-install-with-uvicorn)).

## Configuration

The Warnings API has some custom configuration settings. These can be set in the [configuration file](./configuration_files/warningsConfig.json) located in the [configuration_files](./configuration_files/) directory. More information about configuring the backend can be found here.

## Generic calls

### GET /

Get Root. Returns a welcome message.

Returns:

- 200 status (Success).

### GET /version

Get Version. Returns the application's version.

Returns:

- 200 status (Success).


### GET /healthcheck

Get Healthcheck to verify backend is running.

Returns:

- 200 status (Success).


### GET /health_check

Get Healthcheck to verify NGINX is running.

Returns:

- 200 status (Success).


## Drawings calls

### GET /drawings

List drawings. Requires the user to be authenticated. Returns a drawing list with all areas, global warnings and the user's own drawings.

The endpoint has three ways to search or filter the list:
- `age`: int [seconds], only drawings with a `lastUpdatedTime` younger than the specified age will be returned (except for areas, all areas will always be returned)
- `keywords`: comma-separated list of keywords: `value1,value2,value%20with%20space` (for more info on keywords see [GET /area-keywords](#get-area-keywords) )
- `search`: comma separated list of search terms: `value1,value2`

Example request: `/drawings?age=3600&keywords=objects&search=location`


Example response:

```json
[{
  "objectName": "GlobalDrawing01",
  "scope": "global",
  "lastUpdatedTime": "2023-09-04T08:00:00Z",
  "keywords": "Objects",
  "id": "1",
  "geoJSON": {
    "type": "FeatureCollection",
    "features": [{
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              5.974479827075811,
              51.20028600251058
            ],
            [
              5.980963583031695,
              51.172307239230804
            ],
            [
              6.0348286325125,
              51.19919218003275
            ],
            [
              5.974479827075811,
              51.20028600251058
            ]
          ]
        ],
        "type": "Polygon"
      }
    }]
  }
}]
```

Returns:

- 200 status (Success).
- 422 status (Validation Error).


### GET /drawings/{drawing_id}

Returns the drawing with the given `drawing_id` if allowed for the current user.

Example response:

```json
{
  "objectName": "GlobalDrawing02",
  "scope": "global",
  "lastUpdatedTime": "2023-09-05T08:00:00Z",
  "keywords": "Objects",
  "id": "2",
  "geoJSON": {
    "type": "FeatureCollection",
    "features": [{
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              5.974479827075811,
              51.20028600251058
            ],
            [
              5.980963583031695,
              51.172307239230804
            ],
            [
              6.0348286325125,
              51.19919218003275
            ],
            [
              5.974479827075811,
              51.20028600251058
            ]
          ]
        ],
        "type": "Polygon"
      }
    }]
  }
}
```

Returns:

- 200 status (Success).
- 422 status (Validation Error).

### POST /drawings/

Saves a drawing with a new `drawing_id` generated by the BE and returned in the `Location` header.

(Note that the "keywords" field is automatically set to "Objects")

Example request payload:

```json
{
  "objectName": "Drawing001",
  "scope": "user",
  "geoJSON": {
    "type": "FeatureCollection",
    "features": [{
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              5.974479827075811,
              51.20028600251058
            ],
            [
              5.980963583031695,
              51.172307239230804
            ],
            [
              6.0348286325125,
              51.19919218003275
            ],
            [
              5.974479827075811,
              51.20028600251058
            ]
          ]
        ],
        "type": "Polygon"
      }
    }]
  }
}
```

Example `Location` header in response:

`Location: http://0.0.0.0:8080/drawings/795ed082-84c5-4db4-b429-268abdd28db6`


### POST /drawings/{drawing_id}?delete={true|false}

Update or delete a drawing specified with `drawing_id`. It will be deleted if `delete` equals `true` (it is set to `false` by default if not specified). Object name is automatically generated in the BE.

Example response:

```json
{
  "objectName": "Drawing001",
  "scope": "user",
  "geoJSON": {
    "type": "FeatureCollection",
    "features": [{
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              5.974479827075811,
              51.20028600251058
            ],
            [
              5.980963583031695,
              51.172307239230804
            ],
            [
              6.0348286325125,
              51.19919218003275
            ],
            [
              5.974479827075811,
              51.20028600251058
            ]
          ]
        ],
        "type": "Polygon"
      }
    }]
  }
}
```

Returns:

- 201 status (Created) if successful.
- 400 status (Bad Request) if validation or database error occurs.
- 401 status (Unauthorized) if not authenticated.

### GET /area-keywords
Returns a list of unique keywords in the drawings table.

Example return:
```
["Objects", "KNMI Specific", "FIR", "North Sea Districts"]
```

## Warnings calls

### GET /warnings

Returns a list of warnings available in the database. The list is ordered based on the status of the warning. Within each section, warnings are ordered based on the last updated time of each warning. The list is structured in the following order of sections:

1. `TODO` - These are warnings that require the attention of forecasters (the exact definition is TBD). Currently, these are warnings that that have a status other than _"DRAFT"_ or _"PUBLISHED"_ and whose expiration time lies in the future (_"validUntil"_ >= time of request). If a warning falls under these requirements, the original status of the warning is overwritten with status _"TODO"_;
2. `DRAFT` -  This section consists of warnings with a status equal to _"DRAFT"_ or _"DRAFT_PUBLISHED"_;
3. `PUBLISHED` - Warnings in this section have a status _"PUBLISHED"_ and an expiration time that lies in the future (_"validUntil"_ >= time of request). These are thus warnings that can currently be active, or become active in the future;
4. `EXPIRED` - This section consists of warnings with any status other than _"DRAFT"_, _"DRAFT_PUBLISHED"_ or _"IGNORED"_ and an expiration time somewhere in the last 24 hours (time of request - 24 hours > _"validUntil"_ > time of request). If a warning falls under these requirements, the original status of the warning is overwritten with status _"EXPIRED"_.

Example response:
```json
[
  {
    "id": "b6daea16-46db-497a-865e-9e7320ad8d12",
    "lastUpdatedTime": "2023-12-06T06:56:39Z",
    "status": "TODO",
    "editor": null,
    "warningDetail": {
      "id": "b6daea16-46db-497a-865e-9e7320ad8d12",
      "phenomenon": "wind",
      "areas": [{
        "objectName": "Object 01",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.85,
                      53.5
                    ],
                    [
                      5.8906901106,
                      53.6306901106
                    ],
                    [
                      5.8806901106,
                      53.5306901106
                    ],
                    [
                      5.85,
                      53.5
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#fae21e",
                "stroke": "#fae21e",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "warning_level": "1",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-19T00:00:00Z",
      "validUntil": "2023-12-20T00:00:00Z",
      "level": "moderate",
      "domain": "public",
      "warningProposalSource": "PASCAL-ECMWF-EPS",
      "authority": "KNMI",
      "type": 1
    }
  },
  {
    "id": "bcbb6f28-235c-495f-b1a3-976c49079ca0",
    "lastUpdatedTime": "2023-12-06T06:56:39Z",
    "status": "TODO",
    "editor": "username",
    "warningDetail": {
      "id": "bcbb6f28-235c-495f-b1a3-976c49079ca0",
      "phenomenon": "wind",
      "areas": [{
        "objectName": "Object 02",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      6.6,
                      53.6
                    ],
                    [
                      6.7366225551,
                      53.5183112776
                    ],
                    [
                      6.6366225551,
                      53.6183112776
                    ],
                    [
                      6.6,
                      53.6
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#fae21e",
                "stroke": "#fae21e",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "warning_level": "1",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-19T00:00:00Z",
      "validUntil": "2023-12-20T00:00:00Z",
      "level": "moderate",
      "domain": "public",
      "warningProposalSource": "PASCAL-ECMWF-EPS",
      "authority": "KNMI",
      "type": 1
    }
  },
  {
    "id": "6053fdab-a258-4bba-9d7a-5467714cc808",
    "lastUpdatedTime": "2023-12-06T06:56:38Z",
    "status": "TODO",
    "editor": "user-name-2",
    "warningDetail": {
      "id": "6053fdab-a258-4bba-9d7a-5467714cc808",
      "phenomenon": "wind",
      "areas": [{
        "objectName": "Object 03",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      4.9,
                      53.2
                    ],
                    [
                      5.0,
                      53.3
                    ],
                    [
                      4.8,
                      53.175
                    ],
                    [
                      4.9,
                      53.2
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#fae21e",
                "stroke": "#fae21e",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "warning_level": "1",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-18T00:00:00Z",
      "validUntil": "2023-12-19T00:00:00Z",
      "level": "moderate",
      "domain": "public",
      "warningProposalSource": "PASCAL-ECMWF-EPS",
      "authority": "KNMI",
      "type": 1
    }
  },
  {
    "id": "5eb1c7d3-f052-453d-8e39-e51e8b855754",
    "lastUpdatedTime": "2023-12-06T06:56:38Z",
    "status": "TODO",
    "editor": null,
    "warningDetail": {
      "id": "5eb1c7d3-f052-453d-8e39-e51e8b855754",
      "phenomenon": "wind",
      "areas": [{
        "objectName": "FIR Amsterdam",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      4.16,
                      52.1
                    ],
                    [
                      4.1571427251,
                      52.1023810624
                    ],
                    [
                      4.2,
                      52.06
                    ],
                    [
                      4.16,
                      52.1
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#fae21e",
                "stroke": "#fae21e",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "warning_level": "1",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-11T00:00:00Z",
      "validUntil": "2023-12-12T00:00:00Z",
      "level": "moderate",
      "domain": "public",
      "warningProposalSource": "PASCAL-ECMWF-EPS",
      "authority": "KNMI",
      "type": 1
    }
  },
  {
    "id": "7688a48e-db0b-43b3-a467-e00353f3eb34",
    "lastUpdatedTime": "2023-12-06T06:56:38Z",
    "status": "TODO",
    "editor": null,
    "warningDetail": {
      "id": "7688a48e-db0b-43b3-a467-e00353f3eb34",
      "phenomenon": "wind",
      "areas": [{
        "objectName": "Province",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      3.6,
                      51.7
                    ],
                    [
                      3.6,
                      51.7106311861
                    ],
                    [
                      3.7422966449,
                      51.8205669592
                    ],
                    [
                      3.6467472607,
                      51.7467472607
                    ],
                    [
                      3.6,
                      51.7
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#fae21e",
                "stroke": "#fae21e",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "warning_level": "1",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-11T00:00:00Z",
      "validUntil": "2023-12-12T00:00:00Z",
      "level": "moderate",
      "domain": "public",
      "warningProposalSource": "PASCAL-ECMWF-EPS",
      "authority": "KNMI",
      "type": 1
    }
  },
  {
    "id": "550c2b5e-937d-11ee-a1bf-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:48:20Z",
    "status": "DRAFT",
    "editor": "username",
    "warningDetail": {
      "phenomenon": "fog",
      "areas": [{
        "objectName": "Object 05",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-05T14:48:09Z",
      "validUntil": "2023-12-05T18:48:09Z",
      "level": "moderate",
      "probability": 30
    }
  },
  {
    "id": "078e6676-937d-11ee-b619-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:46:10Z",
    "status": "DRAFT",
    "editor": null,
    "warningDetail": {
      "phenomenon": "wind",
      "areas": [{
        "objectName": "Object 06",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-05T14:46:01Z",
      "validUntil": "2023-12-05T20:46:01Z",
      "level": "severe",
      "probability": 30
    }
  },
  {
    "id": "30fe2e4c-937d-11ee-b3d1-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:47:19Z",
    "status": "PUBLISHED",
    "editor": null,
    "warningDetail": {
      "phenomenon": "snowIce",
      "areas": [{
        "objectName": "User object",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-05T14:47:03Z",
      "validUntil": "2023-12-18T16:47:03Z",
      "level": "severe",
      "probability": 70
    }
  },
  {
    "id": "20f54a8a-937d-11ee-b619-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:46:52Z",
    "status": "PUBLISHED",
    "editor": null,
    "warningDetail": {
      "phenomenon": "lowTemp",
      "areas": [{
        "objectName": "Predefined Object",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-05T14:46:39Z",
      "validUntil": "2023-12-07T20:48:39Z",
      "level": "moderate",
      "probability": 80
    }
  },
  {
    "id": "14f7903a-937d-11ee-b619-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:46:32Z",
    "status": "PUBLISHED",
    "editor": null,
    "warningDetail": {
      "phenomenon": "funnelCloud",
      "areas": [{
        "objectName": "Predefined Object",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }]
      "validFrom": "2023-12-06T14:46:19Z",
      "validUntil": "2023-12-06T20:46:19Z",
      "level": "severe",
      "probability": 30
    }
  },
  {
    "id": "4b94edb8-937d-11ee-b619-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:48:04Z",
    "status": "EXPIRED",
    "editor": null,
    "warningDetail": {
      "phenomenon": "snowIce",
      "areas": [{
        "objectName": "Object 11",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-05T14:47:50Z",
      "validUntil": "2023-12-05T20:47:50Z",
      "level": "moderate",
      "probability": 40
    }
  },
  {
    "id": "3f756602-937d-11ee-a1bf-0ab3cf8a98c1",
    "lastUpdatedTime": "2023-12-05T14:47:43Z",
    "status": "EXPIRED",
    "editor": null,
    "warningDetail": {
      "phenomenon": "thunderstorm",
      "areas": [{
        "objectName": "User object 5",
        "geoJSON": {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ],
                    [
                      4.280148780801422,
                      52.46455621916433
                    ],
                    [
                      6.878229305858475,
                      52.98133263853793
                    ],
                    [
                      5.0950448864466225,
                      53.492000941975654
                    ]
                  ]
                ]
              },
              "properties": {
                "fill": "#ff7800",
                "stroke": "#ff7800",
                "fill-opacity": 0.2,
                "stroke-width": 2,
                "selectionType": "poly",
                "stroke-opacity": 1
              }
            }
          ]
        }
      }],
      "validFrom": "2023-12-05T14:47:34Z",
      "validUntil": "2023-12-05T20:47:34Z",
      "level": "extreme",
      "probability": 40
    }
  }
]
```

### HEAD /warnings/{warning_id}

Checks for the existence of a warning with the given `warning_id` if allowed for the current user.

### GET /warnings/{warning_id}

Returns the warning with the given `warning_id` if allowed for the current user.

Example response:

```json
{
  "status": "TEST",
  "editor": "username",
  "warningDetail": {
    "id": "dummy_id",
    "phenomenon": "fog",
    "validFrom": "2023-09-20T11:31:14Z",
    "validUntil": "2023-09-20T11:31:15Z",
    "level": "moderate",
    "probability": 30,
    "descriptionOriginal": "",
    "descriptionTranslation": "",
    "domain": "aviation",
    "warningProposalSource": "",
    "authority": "knmi",
    "type": 0,
    "areas": [{
      "objectName": "Object",
      "geoJSON": {
        "type": "FeatureCollection",
        "features": [
          {
            "type": "Feature",
            "properties": {},
            "geometry": {
              "coordinates": [
                [
                  [
                    4.67002710921858,
                    52.361780843580334
                  ],
                  [
                    4.772400671798067,
                    52.27675783859135
                  ],
                  [
                    4.843617063158121,
                    52.40321277509318
                  ],
                  [
                    4.67002710921858,
                    52.361780843580334
                  ]
                ]
              ],
              "type": "Polygon"
            }
          }
        ]
      }
    }]
  }
}
```

Returns:

- 200 status (Success) with the warning details in the response body if successful.
- 400 status (Bad Request) with an error message if a validation error occurs during retrieval.
- 401 status (Unauthorized) if not authenticated.
- 404 status (Not Found) if the warning with the given `warning_id` is not found.

### POST /warnings

Saves a warning with a new ID. The location header in the response contains the URL for retrieving the saved warning. If no `id` is provided, it will be generated by the BE. Please note that `authority` and `type` are optional fields not intended to be set at the FE level.

Additionally, the fields `probability`, `descriptionOriginal`, `descriptionTranslation`, `domain` and `warningProposalSource` are still being defined. They are not (yet) required and are loosely validated.

Example payload:

```json
{
  "status": "TEST",
  "warningDetail": {
    "id": "test1",
    "phenomenon": "fog",
    "validFrom": "2023-09-20T11:31:14Z",
    "validUntil": "2023-09-20T11:31:15Z",
    "level": 1,
    "probability": "extreme",
    "descriptionOriginal": "",
    "descriptionTranslation": "",
    "domain": "maritime",
    "warningProposalSource": "",
    "authority": "knmi",
    "type": 0,
    "areas": [{
      "objectName": "Object",
      "geoJSON": {
        "type": "FeatureCollection",
        "features": [
          {
            "type": "Feature",
            "properties": {},
            "geometry": {
              "coordinates": [
                [
                  [
                    4.67002710921858,
                    52.361780843580334
                  ],
                  [
                    4.772400671798067,
                    52.27675783859135
                  ],
                  [
                    4.843617063158121,
                    52.40321277509318
                  ],
                  [
                    4.67002710921858,
                    52.361780843580334
                  ]
                ]
              ],
              "type": "Polygon"
            }
          }
        ]
      }
    }]
  }
}
```

Returns:

- 200 status (Created) if successful.
- 400 status (Bad Request) if validation or database error occurs.
- 401 status (Unauthorized) if not authenticated.


### POST /warnings/{warning_id}?delete={true|false}?editor={true|false}

This endpoint can be used to (1) update an existing warning, (2) delete an existing warning or (3) (un)assign an editor for a warning. The existing warning is retrieved from the database based on the provided `warning_id`.

#### Updating a warning
For updating the status of a warning, the following possibilities currently exist:

![Warnings life cycle](./img/WarningsLifecycle.png)

Note that a user should first assign theirselves as editor (see below) before updating or publishing a warning. After publishing, the editor is automatically unasssigned and set to `null`.

##### Special case: updating a warning with status "EXPIRED"
As shown in the graph above, a warning is automatically marked as "expired" once the endtime of the warning has passed.

It is also possible to update an "active" warning (status = `"PUBLISHED"` and starttime > now > endtime) with a status "EXPIRED". Based on the value of `"expiration_delay"` in the [backend configuration](./configuration_files/warningsConfig.json), the endtime of the warning is set to `"now" + "expiration_delay"`. The unit of the `"expiration_delay"` is in minutes.

The warning will keep the status of `"PUBLISHED"` and is marked as expired once the endtime has passed.

This kind of update is useful if a warning is no longer needed and can be ended sooner than originally expected.


#### Deleting a warning
To delete a warning, no payload has to be sent along with the request. The ID of the warning specified in the request has to match an existing warning with a `DRAFT` status. Warnings with other statuses cannot be deleted. Unless specified, the delete boolean is always "false".

Note that a user should first assign theirselves as editor (see below) before deleting a warning.

#### Assigning and unassigning an editor

To assign an editor, no payload has to be sent along with the request. The ID of the warning specified in the request has to match an existing warning.
1. A user can assign theirselves as editor by making a request with `editor=true`. In the database, the username of the user that made the request is set as editor for the warning. The username is derived from the authentication header.
2. A user can unassign theirselves as editor by making a request with `editor=false`. In the database, the editor is set to `null`;
3. A user can take over a warning from another user and assign theirselves as editor, also with `editor=true`. Before updating the editor in the database, first a check is done whether the warning in the database already has another editor assigned.
    a. If the warning in the database already has another editor (so the warning editor is being taken over), an extra parameter should be added to the request: `force=true`. If this is not done, this parameter defaults to `force=false` and a 409 response is returned.
    b. If a 409 response is received, the same request can be repeated, this time adding `force=true` to the request.
    c. If the warning in the database has no editor assigned, it is not required to explicitly set `force=true` for the request.

| Editor in database | Editor in request | `?force=` | Response |
| ------------------ | ----------------- | --------- | -------- |
| None               | "user-1"          | `false`   | 200      |
| "user-1"           | "user-2"          | `false`   | 409      |
| "user-1"           | "user-2"          | `true`    | 200      |


If you create a draft from scrath, the user creating the warning is automatically set as editor. If you publish a warning, the editor is automatically unassigned and set to `null`.

_Please note that `authority` and `type` are optional fields not intended to be set at the FE level. Additionally, the field `warningProposalSource` is still being defined. These fields are not (yet) required and are loosely validated._

Example of request to delete a warning:

```
POST /warnings/test_id?delete=true
```

Returns:

- 200 status (Updated) if successful.
- 400 status (Bad Request) if validation or database error occurs.
- 401 status (Unauthorized) if not authenticated.

Example of request to assign an editor:
```
POST /warnings/test_id?editor=true&force=true
```

Returns:
- 200 status (Updated) if successful.
- 400 status (Bad Request) if validation or database error occurs.
- 401 status (Unauthorized) if not authenticated.
- 409 status (Conflict) if the warning already has another editor and `force=true` has not been set

Example of request to unassign an editor:
```
POST /warnings/test_id?editor=false
```

Returns:
- 200 status (Updated) if successful.
- 400 status (Bad Request) if validation or database error occurs.
- 401 status (Unauthorized) if not authenticated.


Example payload with `POST /warnings/test_id`:
```json
{
  "status": "DRAFT",
  "warningDetail": {
    "id": "test_id",
    "phenomenon": "fog",
    "validFrom": "2023-09-20T11:31:14Z",
    "validUntil": "2023-09-20T11:31:15Z",
    "level": 1,
    "probability": "extreme",
    "descriptionOriginal": "",
    "descriptionTranslation": "",
    "domain": "maritime",
    "warningProposalSource": "",
    "authority": "knmi",
    "type": 0,
    "areas": [{
      "objectName": "Object",
      "geoJSON": {
        "type": "FeatureCollection",
        "features": [
          {
            "type": "Feature",
            "properties": {},
            "geometry": {
              "coordinates": [
                [
                  [
                    4.67002710921858,
                    52.361780843580334
                  ],
                  [
                    4.772400671798067,
                    52.27675783859135
                  ],
                  [
                    4.843617063158121,
                    52.40321277509318
                  ],
                  [
                    4.67002710921858,
                    52.361780843580334
                  ]
                ]
              ],
              "type": "Polygon"
            }
          }
        ]
      }
    }]
  }
}
```

Returns:

- 200 status (Updated) if successful.
- 400 status (Bad Request) if validation or database error occurs.
- 401 status (Unauthorized) if not authenticated.

# Publishing a warning

Depending on your configuration, two things can happen when you publish a warning:

1. If no `WARNING_PUBLISHER_URL` is set, the warning is marked as "PUBLISHED" in the warnings database;
2. If a `WARNING_PUBLISHER_URL`is specified, the warning to be published is posted to the specified URL and marked as published in the warnings database.

## Configuring a warning publisher
By specifying a `WARNING_PUBLISHER_URL`, the contents of a warning, including some metadata are sent to the specified URL. If NO warning publisher is configured, the warning will be marked as published in the database and no further action will be taken.

If a publisher is configured, and a new warning is published, a call is made to `{WARNING_PUBLISHER_URL}/publish`. The expected response of the publisher is a UUID. This UUID is saved in the `warningDetail` under `published_warning_uuid`. This uuid will be used for updating a warning (to be implemented). The warning is also marked as published in the database.

## Configuring a local development publisher
In the [publisher_local](./publisher_local/) directory, code to run a local development publisher is located. In the [README](./publisher_local/README.md) is described how to run the publisher with uvicorn, as a standalone docker or with docker-compose.

# Coding standards

## Formatting

This project follows the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md). The [YAPF](https://github.com/google/yapf) tool can be used to enforce this. If you installed all dependencies using `pip install -r requirements.txt -r requirements-dev.txt`, you should be able to run YAPF by executing `yapf -ir app/`. This will format all Python files in [app](app), using the configuration specified in [.style.yapf](.style.yapf).

## Organizing imports

This project uses [isort](https://pycqa.github.io/isort/) for organizing import statements. This is a tool to sort imports alphabetically, and automatically separates them into sections and by type. To sort imports for the project run `isort app/`.

## IDE support

If you are using Visual Studio Code, consider updating [.vscode/settings.json](.vscode/settings.json) with the contents below. This will format code and organize imports on save.

```
{
    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },
    "python.formatting.provider": "yapf",
    "[python]": {
        "editor.formatOnSave": true
    }
}
```

# Local running for testing and development
The warnings-backend can be run locally in three ways for different purposes:

1. For development - [Development: Local install with Uvicorn](#development-local-install-with-uvicorn)
2. For testing - [Testing: Docker compose](#testing-docker-compose)
3. For testing in a local FE branch - [Testing BE in local FE branch with Cognito](#testing-be-in-local-fe-branch-with-gitlab-or-cognito)

## Development: Local install with Uvicorn

The application can be started after a local installation with Uvicorn, using the `--reload` flag.
This is useful for development as the warnings-backend service is automatically refreshed when code changes are detected.
In order to make it work a Postgres database is needed on the local installation.
Below are the instructions to start the code with Uvicorn and a local Postgres database. _All commands should be run from the root folder._

### Install Python and Dependencies (Recommended way)

#### 1. Install Pyenv with Pyenv installer

Check the [prerequisites](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) from the pyenv wiki. for Debian Linux they are:

```bash
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Run the Pyenv installer script:

```bash
curl https://pyenv.run | bash
```

Setup your shell environment with these [instructions](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv)

#### 2. Build and install Python

With Pyenv installed, check the available Python versions with

```bash
pyenv install -l
```

Install the latest Python 3.11 version from the list and set it as global Python version for your system.

```bash
pyenv install 3.11.8
pyenv global 3.11.8
```

#### 3. Install Pipx

Pipx is a helper utility for installing system wide Python packages, adding them to your path automatically.

Install pipx with these [instructions](https://pipx.pypa.io/latest/installation/). Linux users can install it with pip:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Optional
sudo pipx ensurepath --global
```

#### 4. Install Poetry

Poetry is used for managing project dependencies and metadata. Install Poetry with

```bash
pipx install poetry
```

and add poetry export plugin to it

```bash
pipx inject poetry poetry-plugin-export
```

#### 5. Activate virtualenv and install dependencies

Activate the virtualenv with poetry

```bash
poetry shell
```

And install project dependencies

```bash
poetry install
```

### Setup Postgres database

```
### Setup postgres database for user geoweb and password geoweb, databasename warningsbackenddb ###
sudo -u postgres createuser --superuser geoweb
sudo -u postgres psql postgres -c "ALTER USER geoweb PASSWORD 'geoweb';"
sudo -u postgres psql postgres -c "CREATE DATABASE warningsbackenddb;"
echo "\q" | psql "dbname=warningsbackenddb user=geoweb password=geoweb host=localhost"
if [ ${?} -eq 0 ];then
    echo "Postgres database setup correctly"
    echo "Use the following setting for the warnings backennd:"
    echo "export WARNINGS_BACKEND_DB=\"dbname=warningsbackenddb user=geoweb password=geoweb host=localhost\""
else
    echo "Postgres database does not work, please check"
fi
```

Or, alternatively, run a Dockerized Postgres database (while running Uvicorn locally):

```
docker run --rm \
    --name warnings-backend-db \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=warningsbackenddb \
    -p 5432:5432 \
    postgres:13.4
```

### Running the application:

Set the correct environment variables. If you have a warning publisher service running, you can set the URL here by exporting the `WARNING_PUBLISHER_URL` (also see [Configuring a warning publisher](#configuring-a-warning-publisher)).

```
export WARNINGS_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/warningsbackenddb"
export VERSION=1.0.0
# OPTIONAL: if a warning publisher exists:
# export WARNING_PUBLISHER_URL=http://0.0.0.0:8090
```

And start the application:
```
bin/admin.sh
bin/start-dev.sh
```

Visit http://127.0.0.1:8080/ or http://127.0.0.1:8080/api

The following endpoints should now be available:

- `curl -kL http://127.0.0.1:8080/`
- `curl -kL http://127.0.0.1:8080/version`
- `curl -kL http://127.0.0.1:8080/healthcheck`

For some calls you need to be authenticated. You can do this by adding a "Geoweb-Username" header with any string value:

- `curl -X 'GET' -kL http://127.0.0.1:8080/drawings -H 'Content-Type: application/json' -H "Geoweb-Username: test-user"`
- `curl -X 'GET' -kL http://127.0.0.1:8080/drawings/{drawing_id} -H 'Content-Type: application/json' -H "Geoweb-Username: test-user"`
- `curl -X 'GET' -kL http://127.0.0.1:8080/warnings -H 'Content-Type: application/json' -H "Geoweb-Username: test-user"`
- `curl -X 'GET' -kL http://127.0.0.1:8080/warnings/{warning_id} -H 'Content-Type: application/json' -H "Geoweb-Username: test-user"`
- `curl -X 'POST' -kL http://127.0.0.1:8080/drawings -H 'Content-Type: application/json' -H "Geoweb-Username: test-user" -d '{"objectName": "Area 51", "scope": "user", "geoJSON": {}}'`
- `curl -X 'POST' -kL http://127.0.0.1:8080/drawings -H 'Content-Type: application/json' -H "Geoweb-Username: test-user" -d @app/tests/testdata/input_drawing.json`
- `curl -X 'POST' -kL http://127.0.0.1:8080/drawings/{drawing_id} -H 'Content-Type: application/json'-H "Geoweb-Username: test-user" -d '{"objectName": "Area 51", "scope": "user", "geoJSON": {"i": 1}, "lastUpdatedTime": "2023-09-04T08:00:00Z", "id": "1"}'`
- `curl -X 'POST' -kL http://127.0.0.1:8080/drawings/{drawing_id}/ -H 'Content-Type: application/json' -H "Geoweb-Username: test-user" -d @app/tests/testdata/update_drawing.json`
- To delete: `curl -X 'POST' -kL http://127.0.0.1:8080/drawings/{drawing_id}?delete=true -H 'Content-Type: application/json' -H "Geoweb-Username: test-user"`

### Accessing the database

You can browse the tables in the database using:

```
psql "${WARNINGS_BACKEND_DB}"
select * from drawings ;
```

to query the drawings table. You can to de the same for the warings table with:

```
psql "${WARNINGS_BACKEND_DB}"
select * from warnings ;
```

If you want to reset/clean the database, you can drop the `drawings` table using:

```
psql "${WARNINGS_BACKEND_DB}" -c 'DROP TABLE IF EXISTS drawings;'
```

Similarly, you can drop the `warnings` table using:

```
psql "${WARNINGS_BACKEND_DB}" -c 'DROP TABLE IF EXISTS warnings;'
```

And cleanup the Alembic table as well:
```
psql "${WARNINGS_BACKEND_DB}" -c 'DROP TABLE IF EXISTS alembic_version;'
```

When the application is restarted, it will be initialized with fresh data.

### Managing Python dependencies

With Poetry installed from previous step, you can add or update Python dependencies with poetry add command.

```bash
# adding dependency
poetry add fastapi

# updating dependency
poetry update fastapi
```

You can add development dependencies with --group keyword.

```bash
# adding development dependency
poetry add --group dev isort
```

All poetry operations respect version constraints set in pyproject.toml file.

##### Fully updating dependencies

You can see a list of outdated dependencies by running

```bash
poetry show --outdated
```

To update all dependencies, still respecting the version constraints on pyproject.toml, run

```bash
poetry update
```

### Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs, if there are new merged changes on the main branch, even if there are no merge conflicts present.                                                                                                                                                  |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)


### Using pre-commit

You can also use the pre-commit configuration. This is used to inspect the code that's about to be committed, to see if you've forgotten something, to make sure tests run, or to examine whatever you need to inspect in the code. To use pre-commit make sure the `pip install` command has succesfully ran with the `requirements-dev.txt` argument.
Then execute the following command in the terminal in the root folder:
 ```
 pre-commit install
 ```
 This will install the pre-commit functions and now pre-commit will run automatically on each `git commit`.
 Only changed files are taken into consideration.

 Optionally you can check all the files with the pre-commit checks (NOTE: this command can make changes to files):

```
pre-commit run --all-files
```

If you wish to skip the pre-commit test but do have pre-commit setup, you can use the `--no-verify` flag in git to skip the pre-commit checks. For example:
```
git commit -m "Message" --no-verify
```

## Testing: Running unit tests

After setting up the virtual environment for the warnings-backend (see [this section](#5-activate-virtualenv-and-install-dependencies)) you should be able to run the unit tests with:

```
pytest app
```

You can specify to only run a specific test file with:

```
pytest app/tests/crud/test_drawings.py
```

Or even run a specific test only:

```
pytest app/tests/crud/test_drawings.py::test_create
```

Note that pre-commit will run the unit tests with every commit as well.

## Testing: Docker compose
The application can be started in a Docker container.
This will create a static version of the warnings-backend which is not automatically refreshed after saving changes in the code.

The [docker-compose.yml](docker-compose.yml) file defines the Nginx, Postgres, warnings-backend and the warnings-backend-admin containers. It wires everthing together using a network. The nginx acts as a reverse proxy and is the gatekeeper in front of the application. The nginx acts as a reverse proxy and is the gatekeeper in front of the application. The postgres database stores the warnings. The warnings-backend-admin runs the database migrations and the warnings-backend is the python application which makes the warnings backend. Some settings need to be defined, like on which port and what url the application should be run.


### Starting up
To start the application with Docker Compose, set up the environment, build the Docker image and start the infrastructure.
_All commands should be run from the root folder._


Remember to set ENABLE_SSL to FALSE if you are deploying behind a load balancer. Otherwise, SSL is required
for a local deployment.

Also, you can optionally configure a publisher, by setting the `WARNING_PUBLISHER_URL` key (also see [here](#configuring-a-warning-publisher)).

1. First, the `.env` file should be created. If GitLab.com is used as an identity provider:

```
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo
GEOWEB_USERNAME_CLAIM="email"
# GEOWEB_REQUIRE_READ_PERMISSION="groups=opengeoweb/internal"
# GEOWEB_REQUIRE_WRITE_PERMISSION="groups=opengeoweb/internal"
# WARNING_PUBLISHER_URL=http://0.0.0.0:8090
VERSION=1.0.0
LOG_LEVEL="INFO"
EOF
```


In case KNMI's Cognito is used as an identity provider:
```
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO=https://knmi-geoweb-tst.auth.eu-west-1.amazoncognito.com/oauth2/userInfo
GEOWEB_USERNAME_CLAIM='username'
# GEOWEB_REQUIRE_READ_PERMISSION="cognito:groups=WarningsRead"
# GEOWEB_REQUIRE_WRITE_PERMISSION='username=some_user'
# JWKS_URI=<add_jwks_uri_here>
# AUD_CLAIM="client_id"
# AUD_CLAIM_VALUE=<add_audience_here>
# ISS_CLAIM="iss"
# ISS_CLAIM_VALUE=<add_issuer_here>
# WARNING_PUBLISHER_URL=http://0.0.0.0:8090
VERSION=1.0.0
LOG_LEVEL="INFO"
EOF
```

2.  After correctly setting `.env`, the container can be build with Docker Compose.


If you work with a `Docker Compose` version 1.x (check `docker-compose -v` or `docker-compose version`), use:

```
docker-compose up -d --build
```


If you have `Compose V2` installed (check `docker compose -v` or `docker compose version`), you should be able to run:
```
docker compose up -d --build
```
according to the [docs](https://docs.docker.com/compose/).


3. After succesfully composing the container, logging can be viewed via:

`docker logs -f warnings-backend`

Note: Logging level can be adjusted by using the `LOG_LEVEL` environment variable. See the [logging](#logging) section for more details.

The following endpoints should now be available:

- `curl -kL https://0.0.0.0:4443/`
- `curl -kL https://0.0.0.0:4443/version`
- `curl -kL https://0.0.0.0:4443/healthcheck` (for the BE itself)
- `curl -kL https://0.0.0.0:4443/health_check` (for NGINX)

For some calls you need to be authenticated. You can retrieve a bearer token by going to the GeoWeb TST application. Open the network panel and retriever your bearer token from the Authentication header. You can save the header with `export token="Bearer ...`. Then you should be able to do the following calls:

- `curl -X 'GET' -kL https://0.0.0.0:4443/drawings -H 'Content-Type: application/json' -H "Authentication: $token"`
- `curl -X 'GET' -kL https://0.0.0.0:4443/drawings/{drawing_id} -H 'Content-Type: application/json' -H "Authentication: $token"`
- `curl -X 'GET' -kL https://0.0.0.0:4443/warnings -H 'Content-Type: application/json' -H "Authentication: $token"`
- `curl -X 'GET' -kL https://0.0.0.0:4443/warnings/{warning_id} -H 'Content-Type: application/json' -H "Authentication: $token"`
- `curl -X 'POST' -kL https://0.0.0.0:4443/drawings -H 'Content-Type: application/json' -H "Authentication: $token" -d '{"objectName": "Area 51", "scope": "user", "geoJSON": {}}'`
- `curl -X 'POST' -kL https://0.0.0.0:4443/drawings -H 'Content-Type: application/json' -H "Authentication: $token" -d @app/tests/testdata/input_drawing.json`
- `curl -X 'POST' -kL https://0.0.0.0:4443/drawings/{drawing_id} -H 'Content-Type: application/json'-H "Authentication: $token" -d '{"objectName": "Area 51", "scope": "user", "geoJSON": {"i": 1}, "lastUpdatedTime": "2023-09-04T08:00:00Z", "id": "1"}'`
- `curl -X 'POST' -kL https://0.0.0.0:4443/drawings/{drawing_id}/ -H 'Content-Type: application/json' -H "Authentication: $token" -d @app/tests/testdata/update_drawing.json`
- To delete: `curl -X 'POST' -kL https://0.0.0.0:4443/drawings/{drawing_id}?delete=true -H 'Content-Type: application/json' -H "Authentication: $token"`

For Mac users, replace `https://0.0.0.0:4443/` with `https://localhost:4443`:

- `curl -kL https:/localhost:4443/`
- `curl -kL https://localhost:4443/version`
- `curl -kL https://localhost:4443/healthcheck` (for the BE itself)
- `curl -kL https://localhost:4443/health_check` (for NGINX)
- `curl -X 'GET' -kL https://localhost:4443/drawings -H 'Content-Type: application/json' -H "Authentication: $token"`
- `curl -X 'GET' -kL https://localhost:4443/drawings/{drawing_id} -H 'Content-Type: application/json' -H "Authentication: $token"`
- `curl -X 'POST' -kL https://localhost:4443/drawings -H 'Content-Type: application/json' -H "Authentication: $token" -d '{"objectName": "Area 51", "scope": "user", "geoJSON": {}}`
- - `curl -X 'POST' -kL https://localhost:4443/drawings -H 'Content-Type: application/json' -H "Authentication: $token" -d @app/tests/testdata/input_drawing.json`
- `curl -X 'POST' -kL https://localhost:4443/drawings/{drawing_id} -H 'Content-Type: application/json' -H "Authentication: $token" -d '{"objectName": "Area 51", "scope": "user", "geoJSON": {"i": 1}, "lastUpdatedTime": "2023-09-04T08:00:00Z", "id": "1"}'`
- `curl -X 'POST' -kL https://localhost:4443/drawings/{drawing_id}/ -H 'Content-Type: application/json' -H "Authentication: $token" -d @app/tests/testdata/update_drawing.json`
- To delete: `curl -X 'POST' -kL https://localhost:4443/drawings/{drawing_id}?delete=true -H 'Content-Type: application/json' -H "Authentication: $token"`


### Clean-up

To reset everything, including the database do:

```
docker-compose down --volumes
```
or
```
docker compose down --volumes
```
depending on your `Docker Compose` version.

## Testing BE in local FE branch with GitLab or Cognito
To test backend changes in a local [opengeoweb frontend](https://gitlab.com/opengeoweb/opengeoweb) branch, the warnings-backend can be setup in a Docker and connected to a local frontend branch with the following steps:

* Make sure that you are _not_ connected to a VPN
* Checkout the branch you want to test locally
* Check that docker is installed and running
* Setup Docker with `OAUTH2_USERINFO` in the `.env` file. See [this section](#testing-docker-compose).
  * to use KNMI's Cognito set
`OAUTH2_USERINFO=https://knmi-geoweb-tst.auth.eu-west-1.amazoncognito.com/oauth2/userInfo`
  * or to use GitLab.com set `OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo`
* In the web browser that you will use to test the app:
  * Go to address: https://localhost:4443/ or https://0.0.0.0:4443/
  * Accept the certificate (this is not possible on connect). Keep this browser open.
* Copy `"GW_WARNING_BACKEND_URL": "https://localhost:4443/"` or  `"GW_WARNING_BACKEND_URL": "https://0.0.0.0:4443/"` into your local config.json in the FE
* Connect to your VPN
* Run your frontend app using the same identity provider set in the warnings-backend
* Open the app in the same browser as where you accepted the certificate

## Logging

For this backend, the environment variable `LOG_LEVEL` can be configured to adjust the verbosity of logging.

It is possible to set it to any standard Python logging level: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`, or `NOTSET`. `INFO` is the default value.

## Authentication mechanism

The authentication is handled in the Nginx reverse proxy. Nginx checks if an authorization header is set and if it contains a valid OAuth2 bearer token. If authorization header is set, Nginx will communicate with the identity provider to get the user information using the provided access token. If an user can be identified, nginx will set a header named `Geoweb-Username` with the identity and proceed with forwarding the request to the warnings-backend container. The warnings-backend will read this header, it uses this to know who signed in. If the OAuth2 bearer token is not given in the authorization header and `ALLOW_ANONYMOUS_ACCESS` is enabled, Nginx will continue forwarding the request to the warnings-backend container without the user identity information. The authentication settings for the API endpoints are defined in the [nginx.conf](./nginx/nginx.conf) file. For authentication details, see the [NGINX reverse proxy documentation](https://gitlab.com/opengeoweb/backend-services/auth-backend).
