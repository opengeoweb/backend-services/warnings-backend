# Geoweb Warnings Backend Configuration

This directory contains the configuration files for the warnings-backend.

By default, the application will use the file found at `configuration_files/warningsConfig.json`, but the path can be adjusted by setting `WARNINGS_CONFIG_FILE`-environment variable.

More explanation about the keys can be found below.

## warningsConfig.json


| Field | Type | Description |
| ----- | ---- | ----------- |
| `"expiration_delay"` | integer| If a warning is marked as "Expired", the endtime is set to current time + `"expiration_delay"` in minutes |
