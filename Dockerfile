# Base image (common for build and deploy stage)
FROM python:3.12-slim-bookworm AS base

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    POETRY_VIRTUALENVS_CREATE=1

# Upgrade pip
RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir poetry==1.8.3

# Create non-root worker
RUN useradd --create-home worker

EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["sh", "bin/start.sh"]


# Build stage
FROM base AS build

# Install dependencies
ADD pyproject.toml ./
ADD poetry.lock ./
RUN poetry install --with dev --no-root

ENV PATH="./.venv/bin:$PATH"
# Copy code
ADD . .

# Run quality checks
RUN isort --diff --check-only app
RUN yapf --diff --recursive app
RUN pylint --extension-pkg-whitelist=pydantic app
RUN mypy app

# Switch to worker
USER worker


# Deploy stage
FROM base

# Install (non-dev) dependencies
COPY --from=build /app/pyproject.toml .
COPY --from=build /app/poetry.lock .
RUN poetry install --without dev --no-root

ENV PATH="./.venv/bin:$PATH"

# Copy application
COPY --from=build /app/app ./app
COPY --from=build /app/bin ./bin
COPY --from=build /app/cli.py /app/
COPY --from=build /app/alembic.ini /app/
COPY --from=build /app/migrations ./migrations
COPY --from=build /app/staticareas ./staticareas
COPY --from=build /app/configuration_files ./configuration_files

# Switch to worker
USER worker
