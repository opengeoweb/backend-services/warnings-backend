"""Add keywords field to drawings table

Revision ID: 0abeb6bc6f47
Revises: 76bc885d7ddc
Create Date: 2024-07-15 11:10:39.589660

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '0abeb6bc6f47'
down_revision = '76bc885d7ddc'
branch_labels = None
depends_on = None


def upgrade() -> None:
    """Add keywords column to drawings table, default is 'Objects' """
    op.add_column('drawings',
                  sa.Column('keywords', sa.String(length=50), nullable=True))
    # assume: if entries in database DON'T have keyword set, they are
    # objects and should get "Objects" parameter assigned
    op.execute("UPDATE drawings SET keywords='Objects' WHERE keywords IS NULL")


def downgrade() -> None:
    """Remove keywords column from drawings table"""
    op.drop_column('drawings', 'keywords')
