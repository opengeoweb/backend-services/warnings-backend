"""add warnings table

Revision ID: 2c44dcfc0c9e
Revises: 69bfc03a0982
Create Date: 2023-09-11 13:56:46.295362

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB

# revision identifiers, used by Alembic.
revision = '2c44dcfc0c9e'
down_revision = '69bfc03a0982'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table('warnings',
                    sa.Column('id', sa.String(length=50), nullable=False),
                    sa.Column('lastUpdatedTime', sa.TIMESTAMP, nullable=False),
                    sa.Column('status', sa.String(length=50), nullable=False),
                    sa.Column('warningjson', JSONB, nullable=False),
                    sa.PrimaryKeyConstraint('id'))


def downgrade() -> None:
    op.drop_table('warnings')
