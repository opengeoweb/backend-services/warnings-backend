"""add domain and source to drawings

Revision ID: 4323ef6ccf03
Revises: 2c44dcfc0c9e
Create Date: 2023-11-21 13:48:49.326515

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '4323ef6ccf03'
down_revision = '2c44dcfc0c9e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    '''Add domain and source column, rename warningjson column'''
    op.add_column('drawings', sa.Column('domain', sa.String(length=50), nullable=True))
    op.add_column('drawings', sa.Column('warningProposalSource', sa.String(length=50), nullable=True))

    op.alter_column('warnings', 'warningjson', new_column_name='warningDetail')


def downgrade() -> None:
    '''Remove domain and source column, undo renaming of warningjson column'''
    op.drop_column('drawings', 'domain')
    op.drop_column('drawings', 'warningProposalSource')

    op.alter_column('warnings', 'warningDetail', new_column_name='warningjson')
