"""create drawings table

Revision ID: 69bfc03a0982
Revises:
Create Date: 2023-08-09 10:16:49.923504

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import JSONB

# revision identifiers, used by Alembic.
revision = '69bfc03a0982'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    '''Create drawings table'''
    op.create_table(
        'drawings', sa.Column('id', sa.String(length=50), nullable=False),
        sa.Column('objectName', sa.String(length=50), nullable=False),
        sa.Column('username', sa.String(length=50), nullable=False),
        sa.Column('lastUpdatedTime', sa.TIMESTAMP, nullable=False),
        sa.Column('scope', sa.String(length=50), nullable=False),
        sa.Column('geoJSON', JSONB, nullable=False),
        sa.PrimaryKeyConstraint('id'))


def downgrade() -> None:
    '''Drop drawings table'''
    op.drop_table('drawings')
