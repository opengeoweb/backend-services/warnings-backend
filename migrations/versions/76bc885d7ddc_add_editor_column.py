"""add editor column

Revision ID: 76bc885d7ddc
Revises: 4323ef6ccf03
Create Date: 2023-12-22 12:29:12.864399

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '76bc885d7ddc'
down_revision = '4323ef6ccf03'
branch_labels = None
depends_on = None


def upgrade() -> None:
    """Add editor column to warnings table, default is None"""
    op.add_column('warnings',
                  sa.Column('editor', sa.String(length=50), nullable=True))


def downgrade() -> None:
    """Remove editor column from warnings table"""
    op.drop_column('warnings', 'editor')
