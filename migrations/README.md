## Database migration

The warnings backend hosts a PostgreSQL database with default and customised warnings. When changes are made to the warnings database schema (e.g. adding or removing columns), the existing database should be updated without the loss of information already in it. Although a best practice is not to edit the table schema (and think beforehand about the schema), making alterations can be inevitable.

In this case, the database migration tool [Alembic](https://alembic.sqlalchemy.org/en/latest/) is used. All files related to database migration can be found in the folder [migrations](migrations). Below, the steps to create a new database migration are described:

### Steps to create a new database migration
1. Run ```alembic revision -m "Revision description"``` in the root folder to generate a new revision file
2. Open the generated revision file.
    * Check that the variable `down_revision` is correctly set, it should be identical to the previous revision ID
3. Complete the function `upgrade()`. Here you describe the database changes with respect to the previous revision.
4. You can complete the `downgrade()` function as well to revert the changes made by the `upgrade()` function if down-revision compatibility is desired.
5. Done! During startup of the backend, the command ```alembic upgrade head``` is used, ensuring that the latest database version is used. Additionally, you can play around with ```alembic upgrade/downgrade``` to verify that the correct changes are made during the migration.


### Some more background information

Alembic can be used for database migrations and can also help you to easily switch between different revisions. Some useful commands:
  * ```alembic current``` show the revision currently in use. If the current revision is equal to the latest revision, `(head)` is printed after the revision ID.
  * ```alembic history (--verbose)``` shows the migration history. The flag ```--verbose``` can be used to display extra information.
  * ```alembic upgrade head``` will migrate to the latest created database revision.
  * ```alembic upgrade [revision ID]``` will migrate to the specified revision ID.
  * ```alembic upgrade +n``` will upgrade to `n` new versions from the current version.
  * ```alembic downgrade base``` will downgrade to the beginning first database revision.
  * ```alembic downgrade -n``` will downgrade to `n` versions from the current.
  * ```alembic revision -m "Revision description"``` This will generate a new revision file in [migrations/versions](migrations/versions/).

In the folder [migrations/versions](migrations/versions/), revision files can be found that describe the database migration:
  * Each migration has a unique Revision ID that is identified with the variable `revision`. The previous revision is identified by the variable `down_revision`. With these variables, all revision files can be linked in order to easily switch between migrations.
  * As stated above, by running ```alembic revision -m "Revision description"``` a new revision file is generated. This revision is the most recent one, so it can be identified as `head`.
  * In each revision file, two functions can be found: `upgrade` and `downgrade`. The `upgrade` function describes how the database changes with respect to the previous revision. The `downgrade` function reverts the changes made with `upgrade` to restore the database to the previous revision.
  * During startup of the backend, the command ```alembic upgrade head``` is used, ensuring that the latest database revision is used.

  For more information, see the [Alembic documentation](https://alembic.sqlalchemy.org/en/latest/).
