#!/bin/bash

python cli.py enable-alembic
alembic upgrade head
python cli.py clear-areas
python cli.py load-predefined-areas
