#!/bin/bash

uvicorn --host 0.0.0.0 \
    --port=${WARNINGS_PORT_HTTP:-8080} \
    --workers=4 \
    --no-access-log \
    --no-use-colors \
    "app.main:app"
